package net.crytec.phoenix.api.version.v13_0;

import java.lang.reflect.Field;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_13_R2.event.CraftEventFactory;
import org.bukkit.craftbukkit.v1_13_R2.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import net.crytec.phoenix.api.anvil.AnvilImplementation;
import net.minecraft.server.v1_13_R2.BlockPosition;
import net.minecraft.server.v1_13_R2.Blocks;
import net.minecraft.server.v1_13_R2.ChatComponentText;
import net.minecraft.server.v1_13_R2.ChatMessage;
import net.minecraft.server.v1_13_R2.Container;
import net.minecraft.server.v1_13_R2.ContainerAnvil;
import net.minecraft.server.v1_13_R2.EntityHuman;
import net.minecraft.server.v1_13_R2.EntityPlayer;
import net.minecraft.server.v1_13_R2.ItemStack;
import net.minecraft.server.v1_13_R2.PacketPlayOutCloseWindow;
import net.minecraft.server.v1_13_R2.PacketPlayOutOpenWindow;

public class AnvilWrapper implements AnvilImplementation {

	public int getNextContainerId(Player player) {
		return toNMS(player).nextContainerCounter();
	}

	public void handleInventoryCloseEvent(Player player) {
		CraftEventFactory.handleInventoryCloseEvent(toNMS(player));
	}

	public void sendPacketOpenWindow(Player player, int containerId) {
		toNMS(player).playerConnection.sendPacket(new PacketPlayOutOpenWindow(containerId, "minecraft:anvil", new ChatMessage(Blocks.ANVIL.a() + ".name")));
	}

	public void sendPacketCloseWindow(Player player, int containerId) {
		toNMS(player).playerConnection.sendPacket(new PacketPlayOutCloseWindow(containerId));
	}

	public void setActiveContainerDefault(Player player) {
		toNMS(player).activeContainer = toNMS(player).defaultContainer;
	}

	public void setActiveContainer(Player player, Object container) {
		toNMS(player).activeContainer = (Container) container;
	}

	public void setActiveContainerId(Object container, int containerId) {
		Field id = null;
		try {
			
			id = Container.class.getField("windowId");
			id.setAccessible(true);
			id.setInt(container, containerId);
			
		} catch (SecurityException | NoSuchFieldException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	public void addActiveContainerSlotListener(Object container, Player player) {
		((Container) container).addSlotListener(toNMS(player));
	}

	public Inventory toBukkitInventory(Object container) {
		return ((Container) container).getBukkitView().getTopInventory();
	}

	public Object newContainerAnvil(Player player) {
		return new AnvilContainer(toNMS(player));
	}

	/**
	 * Turns a {@link Player} into an NMS one
	 *
	 * @param player
	 *            The player to be converted
	 * @return the NMS EntityPlayer
	 */
	private EntityPlayer toNMS(Player player) {
		return ((CraftPlayer) player).getHandle();
	}

	/**
	 * Modifications to ContainerAnvil that makes it so you don't have to have
	 * xp to use this anvil
	 */
    private class AnvilContainer extends ContainerAnvil {

        AnvilContainer(EntityHuman entityhuman) {
            super(entityhuman.inventory, entityhuman.world, new BlockPosition(0, 0, 0), entityhuman);
            this.checkReachable = false;
            
			output = CraftItemStack.asNMSCopy(new ItemBuilderWrapper(Material.COAL).build());
			this.getSlot(1).set(CraftItemStack.asNMSCopy(new ItemBuilderWrapper(Material.COAL).setModelData(2002).build()));
        }
        
        private final ItemStack output;

        @Override
        public void d() {
            super.d();
			if (this.renameText != null && !this.renameText.isEmpty()) {
				this.output.a(new ChatComponentText(ChatColor.translateAlternateColorCodes('&', this.renameText)));
				this.getSlot(2).set(this.output);
			} else {
				return;
			}
			CraftEventFactory.callPrepareAnvilEvent(this.getBukkitView(), this.output);
			this.b();
            this.levelCost = 0;
        }

    }
}
