package net.crytec.phoenix.api.version.v13_0.hologram;

import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_13_R2.CraftWorld;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import com.google.common.collect.Sets;

import net.crytec.phoenix.api.holograms.PhoenixHologram;
import net.crytec.phoenix.api.holograms.PhoenixHologramLine;
import net.crytec.phoenix.api.holograms.PhoenixHologramManager;
import net.minecraft.server.v1_13_R2.DamageSource;
import net.minecraft.server.v1_13_R2.EntitySlime;
import net.minecraft.server.v1_13_R2.EntityTypes;
import net.minecraft.server.v1_13_R2.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_13_R2.PacketPlayOutSpawnEntityLiving;

public class Hologram extends PhoenixHologram {
	
	public Hologram(Location baseLocation, Predicate<Player> playerFilter, PhoenixHologramManager manager) {
		super(baseLocation, playerFilter, manager);
		clickableEntitys = Sets.newHashSet();
	}
	
	private final Set<ClickableEntity> clickableEntitys;
	
	@Override
	public void appendTextLine(String text) {
		this.appendLine(new HologramTextLine(this.getBaseLocation().clone().subtract(0, 0.25 * this.getSize(), 0), text, this));
		if(this.clickable && clickableEntitys.size() < this.getSize() / 4) {
			this.fillClickableEntities();
		}
	}

	@Override
	public void appendItemLine(ItemStack item) {
		this.appendLine(new HologramItemLine(this.getBaseLocation().clone().subtract(0, 0.25 * this.getSize(), 0), item, this));
		if(this.clickable && clickableEntitys.size() < this.getSize() / 4) {
			this.fillClickableEntities();
		}
	}
	
	private void fillClickableEntities() {
		Set<Player> viewers = this.getViewers();
		for(Player player : viewers) {
			this.hideClickableEntities(player);
		}
		while(clickableEntitys.size() < this.getSize() / 4) {
			this.clickableEntitys.add(new ClickableEntity(this.getBaseLocation().clone().subtract(0, clickableEntitys.size() * -1.5, 0)));
		}
		for(Player player : viewers) {
			this.showClickableEntities(player);
		}
		this.registerClickableEntities();
	}
	
	@Override
	public void setClickable() {
		if(this.clickable) return;
		this.clickable = true;
		this.fillClickableEntities();
	}

	@Override
	protected void showClickableEntities(Player player) {
		for(ClickableEntity clickable : clickableEntitys) {
			clickable.sendSpawnPacket(player);
		}
	}

	@Override
	protected void hideClickableEntities(Player player) {
		for(ClickableEntity clickable : clickableEntitys) {
			clickable.sendDespawnPacket(player);
		}
	}
	
	private final class ClickableEntity extends EntitySlime {

		public ClickableEntity(Location location) {
			super(EntityTypes.SLIME , ((CraftWorld) location.getWorld()).getHandle());
			this.setPosition(location.getX(), location.getY(), location.getZ());
			this.setInvisible(true);
			this.setSize(2, true);
			this.setInvulnerable(true);
		}
		
		@Override
		public boolean damageEntity(DamageSource damagesource, float f) {
			return false;
		}
		
		@Override
		protected boolean damageEntity0(DamageSource damagesource, float f) {
			return false;
		}
		
		
		public void sendSpawnPacket(Player player) {
			((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutSpawnEntityLiving(this));
		}
		
		public void sendDespawnPacket(Player player) {
			((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutEntityDestroy(this.getId()));
		}
	}

	@Override
	protected Set<Integer> getClickableEntityIds() {
		return this.clickableEntitys.stream().map(e -> e.getId()).collect(Collectors.toSet());
	}

	@Override
	protected void moveHologram(Vector direction) {
		for(Player viewer : this.getViewers()) {
			for(PhoenixHologramLine<?> line : this.lines) {
				line.sendMove(viewer, direction);
			}
		}
	}
}
