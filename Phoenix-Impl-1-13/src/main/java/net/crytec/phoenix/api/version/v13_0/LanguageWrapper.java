package net.crytec.phoenix.api.version.v13_0;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionType;

import com.google.common.collect.Maps;

import net.crytec.phoenix.api.io.language.EnumLang;
import net.crytec.phoenix.api.io.language.Language;
import net.crytec.phoenix.api.io.language.PotionName;
import net.crytec.phoenix.api.utils.MaterialSet;
import net.crytec.phoenix.api.utils.RomanNumsUtil;

public class LanguageWrapper implements Language {
	
	private final MaterialSet potions;
	
	private Map<String, String> locale;
	private EnumLang language = EnumLang.EN_US;
	private final File languageFolder;
	private final JavaPlugin plugin;
	private static final String download = "http://ctcore.avarioncraft.de/languages/1.13/";
	
	public LanguageWrapper(JavaPlugin plugin) {
		
		this.plugin = plugin;
		
		potions = new MaterialSet(new NamespacedKey(plugin, "lang-potion"),
				Material.LINGERING_POTION,
				Material.SPLASH_POTION,
				Material.TIPPED_ARROW,
				Material.POTION
				);
		locale = Maps.newHashMap();
		
		File customizedLangDir = new File(plugin.getDataFolder(), "lang" + File.separator + "1.13");
		if (!customizedLangDir.exists())
			customizedLangDir.mkdirs();
		
		this.languageFolder = customizedLangDir;
		
	}

	@Override
	public String getEnchantment(Enchantment enchantment) {
		return getEnchantment(enchantment, 0);
	}

	@Override
	public String getEnchantment(Enchantment enchantment, int level) {
		String path = new StringBuilder().append("enchantment.").append(enchantment.getKey().getNamespace()).append('.').append(enchantment.getKey().getKey()).toString();
		
		return new StringBuilder().append(locale.getOrDefault(path, path)).append(" ").append(RomanNumsUtil.toRoman(level)).toString();
	}

	@Override
	public String getEntityName(Entity entity) {
		return entity.getCustomName() != null ? entity.getCustomName() : getEntityName(entity.getType());
	}

	@Override
	public String getEntityName(EntityType type) {
		return StringUtils.capitalize(type.toString().replace("_", "").toLowerCase());
	}

	@Override
	public String getItemName(ItemStack item) {
		if (item.hasItemMeta() && item.getItemMeta().hasDisplayName()) {
			return item.getItemMeta().getDisplayName();
		}
		
		Material type = item.getType();
		
		String path = "unknown";
		
        if (item.getType().isBlock()) {
        	path = new StringBuilder().append("block.").append(type.getKey().getNamespace()).append('.').append(type.getKey().getKey()).toString();
        } else if (potions.isTagged(item)) {
        	
        	StringBuilder pathBuilder = new StringBuilder()
        			.append("item.")
        			.append(type.getKey().getNamespace())
        			.append('.')
        			.append(type.getKey().getKey())
        			.append('.')
        			.append("effect")
        			.append(".")
        			;
        	
            PotionMeta meta = (PotionMeta) item.getItemMeta();
            PotionType potiontype = meta.getBasePotionData().getType();
            
            pathBuilder.append(PotionName.get(potiontype).getLocalePath());
        	
            path = pathBuilder.toString();
        	
        } else if (item.getType().isItem()) {
        	path = new StringBuilder().append("item.").append(type.getKey().getNamespace()).append('.').append(type.getKey().getKey()).toString();
        }
        return locale.getOrDefault(path, path);	
	}

	private void initialize() {
		File file = new File(this.languageFolder, this.language.getLocale() + ".json");

		if (!file.exists()) {
			try {
				URL url = new URL(download + language.getLocale() + ".json");

				HttpURLConnection.setFollowRedirects(false);
				HttpURLConnection con = (HttpURLConnection) url.openConnection();
				con.setRequestMethod("HEAD");
				int response = con.getResponseCode();

				if (response == 200) {
					int contentLength = con.getContentLength();
					plugin.getLogger().info("Downloading language file " + language.getLocale() + " (" + contentLength + " bytes) ");
					FileUtils.copyURLToFile(url, file, 500, 500);
				} else {
					plugin.getLogger().severe("Failed to download the language file! Plugin may not function correctly!");
					return;
				}
			} catch (IOException ex) {
				plugin.getLogger().severe("Failed to download the language file! Plugin may not function correctly!");
				plugin.getLogger().severe(ex.getMessage());
			}
		}

		try (FileInputStream fs = new FileInputStream(file)) {
			EnumLang.readFile(language, new BufferedReader(new InputStreamReader(fs, Charset.forName("UTF-8"))));
			plugin.getLogger().info("LanguageAPI loaded: " + language.getLocale());
		} catch (Exception e) {
			plugin.getLogger().severe("Failed to initialize the language file!");
			plugin.getLogger().severe(e.getMessage());
		}
	}

	@Override
	public void setLanguage(EnumLang language) {
		this.language = language;
		this.initialize();
		this.locale.clear();
		this.locale.putAll(this.language.getMap());
	}

	@Override
	public EnumLang getLanguage() {
		return this.language;
	}

	@Override
	public String translate(String path) {
		return this.locale.getOrDefault(path, "<invalid translation>");
	}

}
