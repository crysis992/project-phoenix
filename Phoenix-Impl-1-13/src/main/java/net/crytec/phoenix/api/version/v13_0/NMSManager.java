package net.crytec.phoenix.api.version.v13_0;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_13_R2.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.Lists;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.crytec.phoenix.api.NPCs.PhoenixNPCManager;
import net.crytec.phoenix.api.anvil.AnvilImplementation;
import net.crytec.phoenix.api.holograms.PhoenixHologramManager;
import net.crytec.phoenix.api.holograms.infobars.InfoBarManager;
import net.crytec.phoenix.api.io.language.EnumLang;
import net.crytec.phoenix.api.io.language.Language;
import net.crytec.phoenix.api.item.ItemFactory;
import net.crytec.phoenix.api.miniblocks.MiniBlockManager;
import net.crytec.phoenix.api.recipes.RecipeManager;
import net.crytec.phoenix.api.version.ImplementationHandler;
import net.crytec.phoenix.api.version.ServerVersion.BukkitVersion;
import net.crytec.phoenix.api.version.v13_0.hologram.HologramManager;
import net.crytec.phoenix.api.version.v13_0.hologram.infobar.InfoBar;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import net.minecraft.server.v1_13_R2.MojangsonParser;
import net.minecraft.server.v1_13_R2.NBTTagCompound;

public class NMSManager implements ImplementationHandler {

	private JavaPlugin plugin;
	
	private AnvilWrapper anvilWrapper;
	private LanguageWrapper languageWrapper;
	private InfoBarManager infoBarManager;
	
	private PhoenixHologramManager hologramManager;
	
	private InfoBarManager getInfoBarManagerInstance() {
		return infoBarManager;
	}
	
	public void startup(JavaPlugin plugin) {
		this.plugin = plugin;
		
		this.anvilWrapper = new AnvilWrapper();
		// Setup language files
		this.languageWrapper = new LanguageWrapper(this.plugin);
		this.languageWrapper.setLanguage(EnumLang.EN_US);
		
		
		if (Bukkit.getPluginManager().getPlugin("ProtocolLib") != null) {
			this.hologramManager = new HologramManager(this.plugin);
			ChunkTracker.init(this.plugin);
			EntityTracker.init(this.plugin);
			
			this.infoBarManager = new InfoBarManager(this.plugin, (entity) -> new InfoBar(entity, this.getInfoBarManagerInstance()));
			
		} else {
			this.plugin.getLogger().warning("ProtocolLib not found - unable to initialize holograms");
		}
		plugin.getLogger().info(this.getVersion().getVersionImplementation());
	}

	public AnvilImplementation getAnvilWrapper() {
		return this.anvilWrapper;
	}

	@Override
	public ItemFactory getItemFactory(Material material) {
		return new ItemBuilderWrapper(material);
	}

	@Override
	public ItemFactory getItemFactory(ItemStack item) {
		return new ItemBuilderWrapper(item);
	}

	@Override
	public String serializeItemStack(ItemStack item) {
		if (item == null) return null;
		
		net.minecraft.server.v1_13_R2.ItemStack nms = CraftItemStack.asNMSCopy(item);
		NBTTagCompound tag = new NBTTagCompound();
		nms.save(tag);
		return tag.toString();
	}

	@Override
	public ItemStack deserializeItemStack(String string) {
		if (string == null || string.equals("empty")) {
			return null;
		}
		try {
			NBTTagCompound comp = MojangsonParser.parse(string);
			net.minecraft.server.v1_13_R2.ItemStack cis = net.minecraft.server.v1_13_R2.ItemStack.a(comp);
			return CraftItemStack.asBukkitCopy(cis);
		} catch (CommandSyntaxException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	@Override
	public Language getLanguageHelper() {
		return this.languageWrapper;
	}

	@Override
	public void sendActionbar(Player player, String message) {
		player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(message));
		
	}

	@Override
	public PhoenixHologramManager getHologramManager() {
		return this.hologramManager;
	}
	
	@Override
	public BukkitVersion getVersion() {
		return BukkitVersion.v1_13_R2;
	}

	@Override
	public PhoenixNPCManager getNPCManager() {
		throw new UnsupportedOperationException("PhoenixNPCManager is currently not implemented for 1.13.x");
	}

	@Override
	public RecipeManager getRecipeManager() {
		throw new UnsupportedOperationException("RecipeManager is currently not implemented for 1.13.x");
	}

	@Override
	public MiniBlockManager getMiniBlockManager() {
		throw new UnsupportedOperationException("MiniBlockManager is currently not implemented for 1.13.x");
	}

	@Override
	public InfoBarManager getInfoBarManager() {
		return this.infoBarManager;
	}

	@Override
	public List<ItemStack> breakBlock(Player player, Block block, ItemStack tool) {
		Bukkit.getLogger().severe("The method breakBlock() in PhoenixAPI is not supported on 1.13");
		return Lists.newArrayList();
	}
}