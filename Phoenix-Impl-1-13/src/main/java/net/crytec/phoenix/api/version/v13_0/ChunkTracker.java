package net.crytec.phoenix.api.version.v13_0;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;

import net.crytec.phoenix.api.events.player.PlayerReceiveChunkEvent;
import net.crytec.phoenix.api.events.player.PlayerUnloadsChunkEvent;

public class ChunkTracker {
	
	public static void init(JavaPlugin host) {
		
		ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(host, PacketType.Play.Server.MAP_CHUNK) {
			
			@Override
			public void onPacketSending(PacketEvent event) {
				if(event.getPacketType() == PacketType.Play.Server.MAP_CHUNK) {
					if (event.getPacket().getMeta("phoenix-ignore").isPresent()) return;
					
					Bukkit.getScheduler().runTask(host, () ->{
						PacketContainer packet = event.getPacket();
						List<Integer> coords = packet.getIntegers().getValues();
						Player player = event.getPlayer();
						long chunkKey = (long) coords.get(0) & 0xffffffffL | ((long) coords.get(1) & 0xffffffffL) << 32;
						
						PlayerReceiveChunkEvent e = new PlayerReceiveChunkEvent(player, chunkKey);
						Bukkit.getPluginManager().callEvent(e);
					});
					
				}
			}
		});
		
		
		ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(host, PacketType.Play.Server.UNLOAD_CHUNK) {
			
			@Override
			public void onPacketSending(PacketEvent event) {
				if(event.getPacketType() == PacketType.Play.Server.UNLOAD_CHUNK) {
					if (event.getPacket().getMeta("phoenix-ignore").isPresent()) return;
					
					Bukkit.getScheduler().runTask(host, () ->{
						PacketContainer packet = event.getPacket();
						List<Integer> coords = packet.getIntegers().getValues();
						Player player = event.getPlayer();
						long chunkKey = (long) coords.get(0) & 0xffffffffL | ((long) coords.get(1) & 0xffffffffL) << 32;
						
						PlayerUnloadsChunkEvent e = new PlayerUnloadsChunkEvent(player, chunkKey);
						Bukkit.getPluginManager().callEvent(e);
					});
				}
			}
		});
	}
}