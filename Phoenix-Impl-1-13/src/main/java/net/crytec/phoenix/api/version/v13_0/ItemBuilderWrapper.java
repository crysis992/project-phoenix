package net.crytec.phoenix.api.version.v13_0;

import java.util.List;
import java.util.UUID;

import org.apache.commons.lang.Validate;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.attribute.AttributeModifier.Operation;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import com.google.common.collect.Lists;

import net.crytec.phoenix.api.item.ItemFactory;
import net.crytec.phoenix.api.nbt.NBTItem;

public class ItemBuilderWrapper implements ItemFactory {
	
	private ItemStack item;
	private ItemMeta meta;
	
	public ItemBuilderWrapper(Material material) {
		this.item = new ItemStack(material);
		this.meta = item.getItemMeta();
	}
	
	public ItemBuilderWrapper(ItemStack item) {
		this.item = item;
		this.meta = item.getItemMeta();
	}

	@Override
	public ItemStack build() {
		this.item.setItemMeta(this.meta);
		return this.item;
	}

	@Override
	public ItemFactory amount(int amount) {
		this.item.setAmount(amount);
		return this;
	}

	@Override
	public ItemFactory name(String name) {
		this.meta.setDisplayName(name);
		return this;
	}

	@Override
	public ItemFactory lore(String string) {
		List<String> lore = this.meta.hasLore() ? this.meta.getLore() : Lists.newArrayList();
		lore.add(string);
		this.meta.setLore(lore);
		return this;
	}

	@Override
	public ItemFactory lore(String string, int index) {
		List<String> lore = this.meta.hasLore() ? this.meta.getLore() : Lists.newArrayList();
		lore.set(index, string);
		this.meta.setLore(lore);
		return this;
	}

	@Override
	public ItemFactory lore(List<String> lore) {
		List<String> newLore = this.meta.hasLore() ? this.meta.getLore() : Lists.newArrayList();
		newLore.addAll(lore);
		this.meta.setLore(newLore);
		return this;
	}

	@Override
	public ItemFactory setUnbreakable(boolean unbreakable) {
		this.meta.setUnbreakable(unbreakable);
		return this;
	}

	@Override
	public ItemFactory setDurability(int durability) {
		((Damageable) meta).setDamage(durability);
		return this;
	}

	@Override
	public ItemFactory enchantment(Enchantment enchantment, int level) {
		if (level <= 0) {
			meta.removeEnchant(enchantment);
		} else {
			meta.addEnchant(enchantment, level, true);
		}
		return this;
	}

	@Override
	public ItemFactory enchantment(Enchantment enchantment) {
		meta.addEnchant(enchantment, 1, true);
		return this;
	}

	@Override
	public ItemFactory type(Material material) {
		this.item.setType(material);
		return this;
	}

	@Override
	public ItemFactory clearLore() {
		this.meta.setLore(Lists.newArrayList());
		return this;
	}

	@Override
	public ItemFactory clearEnchantment() {
		for (final Enchantment e : item.getEnchantments().keySet()) {
			item.removeEnchantment(e);
		}
		return this;
	}

	@Override
	public ItemFactory setSkullOwner(OfflinePlayer player) {
		Validate.isTrue(item.getType() == Material.PLAYER_HEAD, "skullOwner() only applicable for skulls!");

		SkullMeta meta = (SkullMeta) this.meta;
		meta.setOwningPlayer(player);
		item.setItemMeta(meta);
		return this;
	}

	@Override
	public ItemFactory setItemFlag(ItemFlag flag) {
		meta.addItemFlags(flag);
		return this;
	}

	@Override
	public ItemFactory addNBTString(String key, String value) {
		NBTItem nbt = new NBTItem(this.item);
		nbt.setString(key, value);
		
		this.item.setItemMeta(this.meta);
		
		this.item = nbt.getItem();
		this.meta = item.getItemMeta();
		
		return this;
	}

	@Override
	public ItemFactory addNBTDouble(String key, double value) {
		NBTItem nbt = new NBTItem(this.item);
		nbt.setDouble(key, value);
		
		this.item.setItemMeta(this.meta);
		
		this.item = nbt.getItem();
		this.meta = item.getItemMeta();
		return this;
	}

	@Override
	public ItemFactory addNBTBoolean(String key, boolean value) {
		NBTItem nbt = new NBTItem(this.item);
		nbt.setBoolean(key, value);
		
		this.item.setItemMeta(this.meta);
		
		this.item = nbt.getItem();
		this.meta = item.getItemMeta();
		return this;
	}

	@Override
	public ItemFactory addNBTInt(String key, int value) {
		NBTItem nbt = new NBTItem(this.item);
		nbt.setInteger(key, value);
		
		this.item.setItemMeta(this.meta);
		
		this.item = nbt.getItem();
		this.meta = item.getItemMeta();
		return this;
	}

	@Override
	public ItemFactory addNBTLong(String key, long value) {
		NBTItem nbt = new NBTItem(this.item);
		nbt.setLong(key, value);
		
		this.item.setItemMeta(this.meta);
		
		this.item = nbt.getItem();
		this.meta = item.getItemMeta();
		return this;
	}

	@Override
	public ItemFactory setAttribute(Attribute attribute, double amount, EquipmentSlot slot) {
		AttributeModifier modifier = new AttributeModifier(UUID.randomUUID(), "itembuilder", amount, Operation.ADD_NUMBER, slot);
		meta.addAttributeModifier(attribute, modifier);
		return this;
	}

	@Override
	public ItemFactory removeAttribute(Attribute attribute) {
		meta.removeAttributeModifier(attribute);
		return this;
	}

	@Override
	public ItemFactory setModelData(int data) {
		return this;
	}

}
