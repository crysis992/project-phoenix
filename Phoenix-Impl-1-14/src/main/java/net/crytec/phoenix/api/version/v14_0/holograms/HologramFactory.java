package net.crytec.phoenix.api.version.v14_0.holograms;

import java.util.function.Predicate;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import net.crytec.phoenix.api.holograms.PhoenixHologram;
import net.crytec.phoenix.api.holograms.PhoenixHologramFactory;
import net.crytec.phoenix.api.holograms.PhoenixHologramManager;

public class HologramFactory implements PhoenixHologramFactory{

	@Override
	public PhoenixHologram supplyHologram(Location location, Predicate<Player> viewFilter, PhoenixHologramManager manager) {
		return new Hologram(location, viewFilter, manager);
	}
	
}
