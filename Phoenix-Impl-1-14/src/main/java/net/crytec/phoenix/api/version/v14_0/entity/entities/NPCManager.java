package net.crytec.phoenix.api.version.v14_0.entity.entities;

import net.crytec.phoenix.api.NPCs.PhoenixNPCManager;
import net.crytec.phoenix.api.NPCs.customEntitys.CustomEntityManager;

public class NPCManager extends PhoenixNPCManager{

	public NPCManager(CustomEntityManager entityManager) {
		super(entityManager, (location) -> new SimpleNPC(location));
	}

}