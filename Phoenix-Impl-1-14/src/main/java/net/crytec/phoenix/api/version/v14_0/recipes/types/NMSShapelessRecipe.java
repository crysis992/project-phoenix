package net.crytec.phoenix.api.version.v14_0.recipes.types;

import java.util.List;

import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.RecipeChoice;
import org.bukkit.inventory.ShapelessRecipe;

import com.google.common.collect.Lists;

import net.crytec.phoenix.api.recipes.types.CustomShapelessRecipe;
import net.crytec.phoenix.api.recipes.types.RecipeType;

public class NMSShapelessRecipe implements CustomShapelessRecipe {
	
	public NMSShapelessRecipe(NamespacedKey key) {
		this.key = key;
		this.choices = Lists.newArrayList();
		this.group = "";
	}
	
	private String group;
	private ItemStack result;
	private final NamespacedKey key;
	private final List<RecipeChoice> choices;
	
	@Override
	public void addChoice(RecipeChoice choice) {
		this.choices.add(choice);
	}
	
	@Override
	public void setResult(ItemStack result) {
		this.result = result;
	}
	
	@Override
	public void setGroup(String group) {
		this.group = group;
	}
	
	@Override
	public NamespacedKey getKey() {
		return this.key;
	}

	@Override
	public RecipeType getRecipeType() {
		return RecipeType.SHAPELESS;
	}

	@Override
	public ItemStack getResult() {
		return this.result;
	}

	@Override
	public boolean isNBTShape() {
		return false;
	}

	@Override
	public Recipe getRecipe() {
		ShapelessRecipe recipe = new ShapelessRecipe(this.key, this.result);
		for(RecipeChoice choice : this.choices) {
			recipe.addIngredient(choice);
		}
		recipe.setGroup(this.group);
		return recipe;
	}

	@Override
	public List<RecipeChoice> getChoices() {
		return this.choices;
	}

	@Override
	public String getGroup() {
		return this.group;
	}

}
