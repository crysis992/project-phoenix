package net.crytec.phoenix.api.version.v14_0.recipes.types;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.RecipeChoice;
import org.bukkit.inventory.ShapedRecipe;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import net.crytec.phoenix.api.recipes.types.CustomShapedRecipe;
import net.crytec.phoenix.api.recipes.types.RecipeType;

public class NMSShapedRecipe implements CustomShapedRecipe{
	
	private static final char[] SLOTS = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i' };
	
	@SuppressWarnings("deprecation")
	private static final RecipeChoice getChoice(ItemStack item) {
		if(!item.hasItemMeta()) {
			return new RecipeChoice.MaterialChoice(item.getType());
		}else {
			return new RecipeChoice.ExactChoice(item);
		}
	}
	
	public NMSShapedRecipe(NamespacedKey key) {
		this.key = key;
		this.choiceMap = Maps.newHashMap();
	}
	
	private String group = "";
	private ItemStack result;
	private final Map<Character, RecipeChoice> choiceMap;
	private final NamespacedKey key;
	private ItemStack[] matrix;
	
	@Override
	public void setMatrix(ItemStack[] matrix) {
		this.matrix = matrix;
	}
	
	@Override
	public Map<Character, RecipeChoice> getChoiceMap() {
		return this.choiceMap;
	}

	@Override
	public NamespacedKey getKey() {
		return this.key;
	}

	@Override
	public RecipeType getRecipeType() {
		return RecipeType.SHAPED;
	}

	@Override
	public ItemStack getResult() {
		return this.result;
	}

	@Override
	public void setResult(ItemStack result) {
		this.result = result;
	}

	@Override
	public Recipe getRecipe() {
		ShapedRecipe recipe = new ShapedRecipe(this.key, this.result);
		recipe.setGroup(this.group);
		
		HashMap<Integer, ItemStack> temporary = Maps.newHashMap();
		HashMap<Character, ItemStack> shapedItems = Maps.newHashMap();
		
		for (int i = 0; i < 9; i++) {
			temporary.put(i, matrix[i]);
		}
		
		StringBuilder shape = new StringBuilder();
		
		for (int i = 0; i < 9; i++) {
			if (temporary.get(i) == null || temporary.get(i).getType() == Material.AIR) {
				shape.append('0');
			} else {
				shape.append(SLOTS[i]);
				shapedItems.put(SLOTS[i], temporary.get(i));
			}
		}
		
		String finalShape = shape.toString();
		
		if (finalShape.equals("000000000")) {
			return null;
		}
		
		String first = finalShape.substring(0, 3);
		String second = finalShape.substring(3, 6);
		String third = finalShape.substring(6, 9);
		
		String[] shapeArray = new String[] {first,second,third};
		
		recipe.shape(this.modifyShape(shapeArray));
		
		for(Character c : shapedItems.keySet()) {
			recipe.setIngredient(c, getChoice(shapedItems.get(c)));
		}
		
		return recipe;
	}

	@Override
	public void setGroup(String group) {
		this.group = group;
	}

	@Override
	public String getGroup() {
		return this.group;
	}
	
	private String[] modifyShape(String[] shape) {
		
		// Corner
		if(shape[0].charAt(0) != '0' && shape[2].charAt(2) != '0') {
			return this.crop(shape);
		}
		if(shape[0].charAt(2) != '0' && shape[2].charAt(0) != '0') {
			return this.crop(shape);
		}
		
		
		//Top and Bottom
		if(shape[0].charAt(1) != '0' && shape[2].charAt(1) != '0') {
			if(this.columnEmpty(shape, 0)) this.removeColumn(shape, 0);
			if(this.columnEmpty(shape, 2)) this.removeColumn(shape, 2);
			return this.crop(shape);
		}
		if(shape[0].charAt(0) != '0' && shape[2].charAt(0) != '0') {
			if(this.columnEmpty(shape, 1)) this.removeColumn(shape, 1);
			if(this.columnEmpty(shape, 2)) this.removeColumn(shape, 2);
			return this.crop(shape);
		}
		if(shape[0].charAt(2) != '0' && shape[2].charAt(2) != '0') {
			if(this.columnEmpty(shape, 0)) this.removeColumn(shape, 0);
			if(this.columnEmpty(shape, 1)) this.removeColumn(shape, 1);
			return this.crop(shape);
		}
		
		//Left and Right
		if(shape[0].charAt(0) != '0' && shape[0].charAt(2) != '0') {
			if(this.rowEmpty(shape, 1)) this.removeRow(shape, 1);
			if(this.rowEmpty(shape, 2)) this.removeRow(shape, 2);
			return this.crop(shape);
		}
		if(shape[1].charAt(0) != '0' && shape[1].charAt(2) != '0') {
			if(this.rowEmpty(shape, 0)) this.removeRow(shape, 0);
			if(this.rowEmpty(shape, 2)) this.removeRow(shape, 2);
			return this.crop(shape);
		}
		if(shape[2].charAt(0) != '0' && shape[2].charAt(2) != '0') {
			if(this.rowEmpty(shape, 1)) this.removeRow(shape, 1);
			if(this.rowEmpty(shape, 0)) this.removeRow(shape, 0);
			return this.crop(shape);
		}
		
		//Crop
		for(int row = 0; row < 3; row++) {
			if(this.rowEmpty(shape, row)) {
				this.removeRow(shape, row);
			}
		}
		for(int column = 0; column < 3; column++) {
			if(this.columnEmpty(shape, column)) {
				this.removeColumn(shape, column);
			}
		}
		
		return this.crop(shape);
	}
	
	private String[] crop(String[] shape) {
		
		for(int i = 0; i < 3; i++) {
			shape[i] = shape[i].replace("#", "");
		}
		
		shape = Arrays.stream(shape).filter(entry -> !entry.isEmpty()).toArray(String[]::new);
		
		return shape;
	}
	
	private void removeRow(String[] shape, int rowIndex) {
		shape[rowIndex] = "###";
	}
	
	private void removeColumn(String[] shape, int columnIndex) {
		for(int i = 0; i < 3; i++) {
			char[] ca = shape[i].toCharArray();
			ca[columnIndex] = '#';
			shape[i] = String.valueOf(ca);
		}
	}
	
	private boolean rowEmpty(String[] shape, int rowIndex) {
		if(shape[rowIndex].equals("000")) return true;
		if(shape[rowIndex].equals("0  ")) return true;
		if(shape[rowIndex].equals(" 0 ")) return true;
		if(shape[rowIndex].equals("  0")) return true;
		if(shape[rowIndex].equals("0 0")) return true;
		if(shape[rowIndex].equals("00 ")) return true;
		if(shape[rowIndex].equals(" 00")) return true;
		if(shape[rowIndex].equals("   ")) return true;
		return false;
	}
	
	private boolean columnEmpty(String[] shape, int columnIndex) {
		List<Character> chars = Lists.newArrayList('0', ' ', '#');
		
		for(int i = 0; i < 3; i++) {
			if(!chars.contains(shape[i].charAt(columnIndex))){
				return false;
			}
		}
		
		return true;
	}
	
	@Override
	public ItemStack[] getMatrix() {
		return this.matrix;
	}

}
