package net.crytec.phoenix.api.version.v14_0.miniblocks;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_14_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_14_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import net.crytec.phoenix.api.miniblocks.MiniBlock;
import net.minecraft.server.v1_14_R1.EntityArmorStand;
import net.minecraft.server.v1_14_R1.EnumItemSlot;
import net.minecraft.server.v1_14_R1.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_14_R1.PacketPlayOutEntityEquipment;
import net.minecraft.server.v1_14_R1.PacketPlayOutEntityHeadRotation;
import net.minecraft.server.v1_14_R1.PacketPlayOutEntityMetadata;
import net.minecraft.server.v1_14_R1.PacketPlayOutSpawnEntity;
import net.minecraft.server.v1_14_R1.PlayerConnection;
import net.minecraft.server.v1_14_R1.Vector3f;

public class NMSMiniBlock implements MiniBlock {
	
	protected NMSMiniBlock(ItemStack item, Location location) {
		this.location = location;
		this.mount = new MiniMount(location, item);
		this.spawnPacket = this.mount.getSpawnPacket();
		this.equpPacket = this.mount.getEquipPacket();
		this.despawnPacket = this.mount.getDespawnPacket();
		this.metaPacket = this.mount.getMetaPacket();
		this.headPacket = this.mount.getRotationPacket();
	}
	
	private final Location location;
	private final MiniMount mount;
	private final PacketPlayOutSpawnEntity spawnPacket;
	private final PacketPlayOutEntityEquipment equpPacket;
	private final PacketPlayOutEntityMetadata metaPacket;
	private final PacketPlayOutEntityDestroy despawnPacket;
	private final PacketPlayOutEntityHeadRotation headPacket;
	
	@Override
	public Location getLocation() {
		return this.location;
	}

	@Override
	public void showTo(Player player) {
		PlayerConnection conn = ((CraftPlayer) player).getHandle().playerConnection;
		conn.sendPacket(this.spawnPacket);
		conn.sendPacket(this.equpPacket);
		conn.sendPacket(this.metaPacket);
		conn.sendPacket(this.headPacket);
	}

	@Override
	public void hideFrom(Player player) {
		PlayerConnection conn = ((CraftPlayer) player).getHandle().playerConnection;
		conn.sendPacket(this.despawnPacket);
	}
	
	private final class MiniMount extends EntityArmorStand{

		public MiniMount(Location location, ItemStack item) {
			super(((CraftWorld)location.getWorld()).getHandle(), location.getX(), location.getY(), location.getZ());
			super.setInvulnerable(true);
			super.setInvisible(true);
			super.setMarker(true);
			this.item = CraftItemStack.asNMSCopy(item);
			super.setEquipment(EnumItemSlot.HEAD, this.item);
			super.setHeadPose(new Vector3f(0F, 1F, 0F));
		}
		
		private final net.minecraft.server.v1_14_R1.ItemStack item;
		
		public PacketPlayOutSpawnEntity getSpawnPacket() {
			return new PacketPlayOutSpawnEntity(this);
		}
		
		public PacketPlayOutEntityDestroy getDespawnPacket() {
			
			return new PacketPlayOutEntityDestroy(this.getId());
		}
		
		public PacketPlayOutEntityEquipment getEquipPacket() {
			return new PacketPlayOutEntityEquipment(this.getId(), EnumItemSlot.HEAD, this.item);
		}
		
		public PacketPlayOutEntityMetadata getMetaPacket() {
			return new PacketPlayOutEntityMetadata(this.getId(), this.getDataWatcher(), true);
		}
		
		public PacketPlayOutEntityHeadRotation getRotationPacket() {
			return new PacketPlayOutEntityHeadRotation(this, (byte)0);
		}
		
	}
	
}
