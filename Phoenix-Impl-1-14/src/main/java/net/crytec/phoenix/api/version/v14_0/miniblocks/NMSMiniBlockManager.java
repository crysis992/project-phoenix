package net.crytec.phoenix.api.version.v14_0.miniblocks;

import org.bukkit.plugin.java.JavaPlugin;

import net.crytec.phoenix.api.miniblocks.MiniBlockManager;

public class NMSMiniBlockManager extends MiniBlockManager{

	public NMSMiniBlockManager(JavaPlugin host) {
		super(host, (item, loc) -> new NMSMiniBlock(item, loc));
	}

}
