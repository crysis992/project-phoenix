package net.crytec.phoenix.api.version.v14_0;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;

import net.crytec.phoenix.api.events.player.PlayerReceiveEntityEvent;
import net.crytec.phoenix.api.events.player.PlayerUnloadsEntityEvent;

public class EntityTracker {
	
	public static void init(JavaPlugin host) {
		ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(host, PacketType.Play.Server.SPAWN_ENTITY_LIVING) {
			
			@Override
			public void onPacketSending(PacketEvent event) {
				if(event.getPacketType() == PacketType.Play.Server.SPAWN_ENTITY_LIVING) {
					if (event.getPacket().getMeta("phoenix-ignore").isPresent()) return;

					PacketContainer packet = event.getPacket();

					Player viewer = event.getPlayer();
					int entityID = packet.getIntegers().read(0);
					
					PlayerReceiveEntityEvent recieveEvent = new PlayerReceiveEntityEvent(viewer, entityID);
					Bukkit.getScheduler().runTask(host, () -> Bukkit.getPluginManager().callEvent(recieveEvent));
				}
			}
		});
		
		
		ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(host, PacketType.Play.Server.ENTITY_DESTROY) {
			
			@Override
			public void onPacketSending(PacketEvent event) {
				if(event.getPacketType() == PacketType.Play.Server.ENTITY_DESTROY) {
					if (event.getPacket().getMeta("phoenix-ignore").isPresent()) return;

					PacketContainer packet = event.getPacket();

					int[] entityIDs = packet.getIntegerArrays().getValues().get(0);
					Player viewer = event.getPlayer();
					
					PlayerUnloadsEntityEvent unloadEvent = new PlayerUnloadsEntityEvent(viewer, entityIDs);
					Bukkit.getScheduler().runTask(host, () -> Bukkit.getPluginManager().callEvent(unloadEvent));
				}
			}
		});
	}
}