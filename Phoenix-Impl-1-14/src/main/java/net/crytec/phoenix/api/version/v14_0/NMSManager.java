package net.crytec.phoenix.api.version.v14_0;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.Validate;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_14_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_14_R1.block.CraftBlock;
import org.bukkit.craftbukkit.v1_14_R1.block.CraftBlockState;
import org.bukkit.craftbukkit.v1_14_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.crytec.phoenix.api.NPCs.customEntitys.CustomEntityManager;
import net.crytec.phoenix.api.anvil.AnvilImplementation;
import net.crytec.phoenix.api.holograms.PhoenixHologramManager;
import net.crytec.phoenix.api.holograms.infobars.InfoBarManager;
import net.crytec.phoenix.api.io.language.Language;
import net.crytec.phoenix.api.item.ItemFactory;
import net.crytec.phoenix.api.miniblocks.MiniBlockManager;
import net.crytec.phoenix.api.recipes.RecipeManager;
import net.crytec.phoenix.api.version.ImplementationHandler;
import net.crytec.phoenix.api.version.ServerVersion.BukkitVersion;
import net.crytec.phoenix.api.version.v14_0.entity.entities.NPCManager;
import net.crytec.phoenix.api.version.v14_0.holograms.HologramManager;
import net.crytec.phoenix.api.version.v14_0.holograms.infobar.InfoBar;
import net.crytec.phoenix.api.version.v14_0.miniblocks.NMSMiniBlockManager;
import net.crytec.phoenix.api.version.v14_0.recipes.NMSRecipeManager;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import net.minecraft.server.v1_14_R1.BlockPosition;
import net.minecraft.server.v1_14_R1.IBlockData;
import net.minecraft.server.v1_14_R1.LootContextParameters;
import net.minecraft.server.v1_14_R1.LootTableInfo;
import net.minecraft.server.v1_14_R1.MojangsonParser;
import net.minecraft.server.v1_14_R1.NBTTagCompound;
import net.minecraft.server.v1_14_R1.TileEntity;
import net.minecraft.server.v1_14_R1.WorldServer;

public class NMSManager implements ImplementationHandler {
	
	private JavaPlugin plugin;
	
	private AnvilModule anvilModule;
	private LanguageModule language;
	private HologramManager hologramManager;
	private NPCManager npcManager;
	private NMSRecipeManager recipeManager;
	private NMSMiniBlockManager miniBlockManager;
	private InfoBarManager infoBarManager;
	
	private InfoBarManager getInfoBarManagerInstance() {
		return infoBarManager;
	}
	
	public void startup(JavaPlugin plugin) {
		this.plugin = plugin;
		
		// Setup NMS / Version Specific Modules
		
		this.language = new LanguageModule(plugin);
		this.anvilModule = new AnvilModule();
		this.npcManager = new NPCManager(new CustomEntityManager(plugin));
		this.recipeManager = new NMSRecipeManager();
		
//		this.miniBlockManager = new NMSMiniBlockManager(plugin);
		
				
		if (Bukkit.getPluginManager().getPlugin("ProtocolLib") != null) {
			
			if (!Bukkit.getPluginManager().getPlugin("ProtocolLib").getDescription().getVersion().contains("4.5")) {
				Bukkit.getLogger().severe("=================== -ERROR- ========================");
				Bukkit.getLogger().severe(" ");
				Bukkit.getLogger().severe("You are running an incompatible version of ProtocolLib. Please update to their latest development build (4.5 or higher) ");
				Bukkit.getLogger().severe(" ");
				Bukkit.getLogger().severe("=================== " + this.plugin.getDescription().getName() + "[" + this.plugin.getDescription().getVersion() +  "] ========================");
			} else {
				ChunkTracker.init(plugin);
				EntityTracker.init(plugin);
				this.hologramManager = new HologramManager(this.plugin);
				this.infoBarManager = new InfoBarManager(this.plugin, (entity) -> new InfoBar(entity, this.getInfoBarManagerInstance()));
			}
		} else {
			this.plugin.getLogger().warning("Unable to find ProtocolLib! The plugin may not function correctly. Certain features require ProtocolLib.");
		}
		
		plugin.getLogger().info(this.getVersion().getVersionImplementation());
	}
	
	public NPCManager getNPCManager() {
		return this.npcManager;
	}

	public AnvilImplementation getAnvilWrapper() {
		return this.anvilModule;
	}

	@Override
	public ItemFactory getItemFactory(Material material) {
		return new ItemBuilderWrapper(material);
	}

	@Override
	public ItemFactory getItemFactory(ItemStack item) {
		return new ItemBuilderWrapper(item);
	}

	@Override
	public String serializeItemStack(ItemStack item) {
		if (item == null) return null;
		
		net.minecraft.server.v1_14_R1.ItemStack nms = CraftItemStack.asNMSCopy(item);
		NBTTagCompound tag = new NBTTagCompound();
		nms.save(tag);
		return tag.toString();
	}

	@Override
	public ItemStack deserializeItemStack(String string) {
		if (string == null || string.equals("empty")) {
			return null;
		}
		try {
			NBTTagCompound comp = MojangsonParser.parse(string);
			net.minecraft.server.v1_14_R1.ItemStack cis = net.minecraft.server.v1_14_R1.ItemStack.a(comp);
			return CraftItemStack.asBukkitCopy(cis);
		} catch (CommandSyntaxException ex) {
			ex.printStackTrace();
		}
		return null;
	}

	@Override
	public Language getLanguageHelper() {
		return this.language;
	}

	@Override
	public void sendActionbar(Player player, String message) {
		player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(message));
		
	}

	@Override
	public PhoenixHologramManager getHologramManager() {
		return this.hologramManager;
	}
	
	@Override
	public RecipeManager getRecipeManager() {
		return this.recipeManager;
	}

	@Override
	public BukkitVersion getVersion() {
		return BukkitVersion.v1_14_R1;
	}

	@Override
	public MiniBlockManager getMiniBlockManager() {
		Validate.notNull(this.miniBlockManager, "MiniBlockManager not initialized");
		return this.miniBlockManager;
	}

	@Override
	public InfoBarManager getInfoBarManager() {
		return this.infoBarManager;
	}

	@Override
	public List<ItemStack> breakBlock(Player player, Block block, ItemStack tool) {
		WorldServer ws = ((CraftWorld) block.getWorld()).getHandle();
		IBlockData data = ((CraftBlockState) block.getState()).getHandle();
		BlockPosition pos = ((CraftBlock) block).getPosition();
		TileEntity blockEnt = ws.getTileEntity(pos);

		LootTableInfo.Builder builder = new LootTableInfo.Builder(ws).set(LootContextParameters.POSITION, pos).set(LootContextParameters.BLOCK_STATE, data)
				.setOptional(LootContextParameters.BLOCK_ENTITY, blockEnt).setOptional(LootContextParameters.TOOL, CraftItemStack.asNMSCopy(tool));

		return data.a(builder).stream().map(original -> CraftItemStack.asBukkitCopy(original)).collect(Collectors.toList());
	}
}