package net.crytec.phoenix.api.version.v14_0.entity.entities;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_14_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftVillager;
import org.bukkit.entity.Villager.Profession;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;

import net.crytec.phoenix.api.NPCs.customEntitys.CustomEntity;
import net.crytec.phoenix.api.NPCs.customEntitys.CustomPersistantTypeContainer;
import net.crytec.phoenix.api.NPCs.customEntitys.CustomVillagerNPC;
import net.crytec.phoenix.api.utils.UtilLoc;
import net.minecraft.server.v1_14_R1.ChatMessage;
import net.minecraft.server.v1_14_R1.DamageSource;
import net.minecraft.server.v1_14_R1.EntityHuman;
import net.minecraft.server.v1_14_R1.EntityItem;
import net.minecraft.server.v1_14_R1.EntityTypes;
import net.minecraft.server.v1_14_R1.EntityVillager;
import net.minecraft.server.v1_14_R1.EnumHand;
import net.minecraft.server.v1_14_R1.MerchantRecipe;
import net.minecraft.server.v1_14_R1.MinecraftServer;

public class SimpleNPC extends EntityVillager implements CustomEntity, CustomVillagerNPC {
	
	@SuppressWarnings("deprecation")
	public SimpleNPC(Location location) {
		super(EntityTypes.VILLAGER, ((CraftWorld) location.getWorld()).getHandle());
		this.setPosition(location.getX(), location.getY(), location.getZ());
		MinecraftServer.getServer().processQueue.add(() -> ((CraftWorld)location.getWorld()).getHandle().addEntitySerialized(this, SpawnReason.CUSTOM));
		this.setInvulnerable(true);
		this.dataContainer = new CustomPersistantTypeContainer();
	}
	
	private final CustomPersistantTypeContainer dataContainer;
	
	@Override
	protected void initPathfinder() {
		return;
	}
	
	public void despawn() {
		
	}
	
	@Override
	public boolean damageEntity(DamageSource damagesource, float f) {
		return false;
	}
	
	@Override
	protected boolean damageEntity0(DamageSource damagesource, float f) {
		return false;
	}

	@Override
	public void tick() {

	}
	
	@Override
	protected void mobTick() {
		// Keine Ahnung was das hier macht
	}
	
	public boolean a(EntityHuman entityhuman, EnumHand enumhand) { // mobInteract
		
		return false;
		
	}
	
	@Override
	protected void ed() { // stopTrading
		return;
	}
	
	@Override
	public boolean ei() { //canRestock
		return false;
	}
	
	@Override
	public void ej() { // restock
		return;
	}
	
	@Override
	public boolean ek() { // shouldRestock
		return false;
	}
	
	@Override
	protected void b(MerchantRecipe merchantrecipe) { // setOffers
		return;
	}
	
	
	@Override
	protected void tickPotionEffects() {
		return; // Do not tick any potion effects
	}
	
	@Override
	public int f(EntityHuman entityhuman) { // getPlayerReputation
		return 1;
	}
	
	@Override
	public void eo() { // eatAndDigestFood
		return;
	}
	
	@Override
	protected void a(EntityItem entityitem) { // pickUpItem
		return;
	}
	

	@Override
	public boolean isInWater() {
		return false;
	}

	@Override
	protected void burnFromLava() {
		return;
	}

//	@Override
//	public void b(NBTTagCompound nbttagcompound) {
//		
//	}

	@Override
	public void onDeath() {
	}
	
	@Override
	public void setVillagerProfession(Profession profession) {
		((CraftVillager)this.getBukkitEntity()).setProfession(profession);
		this.dataContainer.add("_Profession", profession.toString());
	}
	
	@Override
	public void setVillagerExperience(int value) {
		((CraftVillager)this.getBukkitEntity()).setVillagerExperience(value);
		this.dataContainer.add("_ProfessionExp", value);
	}
	
	@Override
	public CustomPersistantTypeContainer getDataContainer() {
		return this.dataContainer;
	}

	@Override
	public void onLoad() {
		if(this.dataContainer.containsString("_Profession")) {
			System.out.println("Setting profession");
			this.setVillagerProfession(Profession.valueOf(this.dataContainer.getString("_Profession")));
		}
		if(this.dataContainer.containsInteger("_ProfessionExp")) {
			System.out.println("Setting Exp");
			this.setExperience(this.dataContainer.getInt("_ProfessionExp"));
		}
		
		if (this.dataContainer.containsString("_displayname")) {
			this.setCustomName(new ChatMessage(this.dataContainer.getString("_displayname")));
		}
		
		if (this.dataContainer.containsString("_location")) {
			Location loc = UtilLoc.parseString(this.dataContainer.getString("_location"));
			this.setYawPitch(loc.getYaw(), loc.getPitch());
			this.getBukkitEntity().teleport(loc);
			this.getBukkitEntity().setRotation(loc.getYaw(), loc.getPitch());
		}
	}

	@Override
	public void onSave() {
		this.dataContainer.add("_location", UtilLoc.toString(this.getBukkitEntity().getLocation()));
		this.dataContainer.add("_displayname", this.getCustomName().getText());
	}
	
}