package net.crytec.phoenix.api.version.v14_0.recipes;

import java.util.HashMap;
import java.util.Iterator;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.craftbukkit.v1_14_R1.CraftServer;
import org.bukkit.craftbukkit.v1_14_R1.inventory.CraftFurnaceRecipe;
import org.bukkit.craftbukkit.v1_14_R1.inventory.CraftShapedRecipe;
import org.bukkit.craftbukkit.v1_14_R1.inventory.CraftShapelessRecipe;
import org.bukkit.craftbukkit.v1_14_R1.inventory.CraftStonecuttingRecipe;
import org.bukkit.craftbukkit.v1_14_R1.util.CraftNamespacedKey;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.inventory.StonecuttingRecipe;

import com.google.common.collect.Maps;

import net.crytec.phoenix.api.recipes.RecipeManager;
import net.crytec.phoenix.api.recipes.types.CustomFurnaceRecipe;
import net.crytec.phoenix.api.recipes.types.CustomShapedRecipe;
import net.crytec.phoenix.api.recipes.types.CustomShapelessRecipe;
import net.crytec.phoenix.api.recipes.types.PhoenixRecipe;
import net.crytec.phoenix.api.recipes.types.VanillaRecipe;
import net.crytec.phoenix.api.version.v14_0.recipes.types.NMSFurnaceRecipe;
import net.crytec.phoenix.api.version.v14_0.recipes.types.NMSShapedRecipe;
import net.crytec.phoenix.api.version.v14_0.recipes.types.NMSShapelessRecipe;
import net.crytec.phoenix.api.version.v14_0.recipes.types.NMSVanillaRecipe;
import net.minecraft.server.v1_14_R1.CraftingManager;
import net.minecraft.server.v1_14_R1.MinecraftServer;
import net.minecraft.server.v1_14_R1.Recipes;

public class NMSRecipeManager implements RecipeManager {

	private final MinecraftServer server;
	private final CraftingManager craftingManager;
	
	private final HashMap<NamespacedKey, PhoenixRecipe> registeredRecipes;
	
	public NMSRecipeManager() {
		this.server = ((CraftServer) Bukkit.getServer()).getHandle().getServer();
		this.craftingManager = this.server.getCraftingManager();

		this.registeredRecipes = Maps.newHashMap();

		// Load and store Vanilla Recipes
		try {
			Iterator<Recipe> iter = Bukkit.recipeIterator();

			while (iter.hasNext()) {

				Recipe recipe = iter.next();

				if (recipe instanceof ShapedRecipe || recipe instanceof ShapelessRecipe || recipe instanceof FurnaceRecipe || recipe instanceof StonecuttingRecipe) {
					NMSVanillaRecipe vRecipe = new NMSVanillaRecipe(recipe);
					this.registeredRecipes.put(vRecipe.getKey(), vRecipe);
				}
			}
		} catch (Exception ex) {
			Bukkit.getLogger().severe("Failed to register recipe during startup [" + ex.getMessage() + "] ");
		}
	}
	
	
	@Override
	public void registerRecipe(PhoenixRecipe recipe) {
		if (recipe.getResult() == null || recipe.getResult().getType() == Material.AIR ) {
			Bukkit.getLogger().severe("Failed to register recipe from class" + Thread.currentThread().getStackTrace()[2].getClassName() + ". The recipe must have non-AIR result.");
			return;
		}
		
		Bukkit.addRecipe(recipe.getRecipe());
		
		if (recipe instanceof VanillaRecipe) {
			if (recipe instanceof ShapedRecipe) ((CraftShapedRecipe) recipe).addToCraftingManager();
			else if (recipe instanceof ShapedRecipe) ((CraftShapedRecipe) recipe).addToCraftingManager();
			else if (recipe instanceof ShapelessRecipe) ((CraftShapelessRecipe) recipe).addToCraftingManager();
			else if (recipe instanceof FurnaceRecipe) ((CraftFurnaceRecipe) recipe).addToCraftingManager();
			else if (recipe instanceof StonecuttingRecipe) ((CraftStonecuttingRecipe) recipe).addToCraftingManager();
			
			NMSVanillaRecipe rec = (NMSVanillaRecipe) recipe;
			rec.setDisabled(false);
			return;
		}
		
		if (!recipe.isNBTShape()) {
			Recipes<?> recipes = this.craftingManager.a(CraftNamespacedKey.toMinecraft(recipe.getKey())).get().g();
			this.craftingManager.recipes.get(recipes).getAndMoveToLast(this.craftingManager.recipes.get(recipes).firstKey());
		}
	}
	
	private void unregisterRecipe(NamespacedKey recipeKey) {
		
		Recipes<?> NMS_something = this.craftingManager.a(CraftNamespacedKey.toMinecraft(recipeKey)).get().g();
		this.craftingManager.recipes.get(NMS_something).remove(CraftNamespacedKey.toMinecraft(recipeKey));
		this.registeredRecipes.remove(recipeKey);
		
	}
	
	public void unregisterRecipe(PhoenixRecipe recipe) {
		
			Recipes<?> NMS_something = this.craftingManager.a(CraftNamespacedKey.toMinecraft(recipe.getKey())).get().g();
			this.craftingManager.recipes.get(NMS_something).remove(CraftNamespacedKey.toMinecraft(recipe.getKey()));
			
			if (recipe instanceof VanillaRecipe) {
				NMSVanillaRecipe rec = (NMSVanillaRecipe) recipe;
				rec.setDisabled(true);
				Bukkit.getLogger().info("Disabled Recipe " + recipe.getKey().toString());
				return;
			} else  {
				
				this.registeredRecipes.remove(recipe.getKey());
			}
			
	}

	@Override
	public CustomShapedRecipe createShapedRecipe(NamespacedKey key) {
		return new NMSShapedRecipe(key);
	}

	@Override
	public CustomShapelessRecipe createShapelessRecipe(NamespacedKey key) {
		return new NMSShapelessRecipe(key);
	}


	@Override
	public CustomFurnaceRecipe createFurnaceRecipe(NamespacedKey key) {
		return new NMSFurnaceRecipe(key);
	}


	@Override
	public void updateRecipe(NamespacedKey oldRecipeKey, PhoenixRecipe newRecipe) {
		
		this.unregisterRecipe(oldRecipeKey);
		this.registerRecipe(newRecipe);
		
	}
	
}