package net.crytec.phoenix.api.version.v14_0.recipes.types;

import org.bukkit.NamespacedKey;
import org.bukkit.craftbukkit.v1_14_R1.util.CraftNamespacedKey;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.inventory.StonecuttingRecipe;

import net.crytec.phoenix.api.recipes.types.RecipeType;
import net.crytec.phoenix.api.recipes.types.VanillaRecipe;
import net.minecraft.server.v1_14_R1.MinecraftKey;

public class NMSVanillaRecipe implements VanillaRecipe {

	
	private Recipe recipe;
	private boolean disabled = false;
	private final MinecraftKey NMSKey;
	
	public NMSVanillaRecipe(Recipe recipe) {
		this.recipe = recipe;
		this.NMSKey = CraftNamespacedKey.toMinecraft(this.getKey());
		
	}
	
	@Override
	public NamespacedKey getKey() {
		if (recipe instanceof ShapedRecipe) return ((ShapedRecipe) recipe).getKey();
		else if (recipe instanceof ShapelessRecipe) return ((ShapelessRecipe) recipe).getKey();
		else if (recipe instanceof StonecuttingRecipe) return ((StonecuttingRecipe) recipe).getKey();
		else if (recipe instanceof FurnaceRecipe) return ((FurnaceRecipe) recipe).getKey();
		else throw new UnsupportedOperationException("Recipe is not a valid type");
	}

	@Override
	public RecipeType getRecipeType() {
		return RecipeType.VANILLA;
	}

	@Override
	public ItemStack getResult() {
		return this.recipe.getResult();
	}

	@Override
	public boolean isNBTShape() {
		return false;
	}

	@Override
	public Recipe getRecipe() {
		return this.recipe;
	}

	@Override
	public String getId() {
		return this.recipe.getResult().getType().toString();
	}

	@Override
	public boolean isDisabled() {
		return this.disabled;
	}

	@Override
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public MinecraftKey getNMSKey() {
		return NMSKey;
	}

	@Override
	public void setGroup(String group) {
		throw new UnsupportedOperationException("Not supported on vanilla recipes");
	}

	@Override
	public String getGroup() {
		throw new UnsupportedOperationException("Not supported on vanilla recipes");
	}

	@Override
	public void setResult(ItemStack result) {
		throw new UnsupportedOperationException("Not supported on vanilla recipes");
	}
}
