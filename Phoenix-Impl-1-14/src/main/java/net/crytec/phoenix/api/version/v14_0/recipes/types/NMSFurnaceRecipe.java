package net.crytec.phoenix.api.version.v14_0.recipes.types;

import org.bukkit.NamespacedKey;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.RecipeChoice;

import net.crytec.phoenix.api.recipes.types.CustomFurnaceRecipe;
import net.crytec.phoenix.api.recipes.types.RecipeType;

public class NMSFurnaceRecipe implements CustomFurnaceRecipe{
	
	public NMSFurnaceRecipe(NamespacedKey key) {
		this.key = key;
	}
	
	private final NamespacedKey key;
	private ItemStack result;
	private ItemStack input;
	private String group = "";
	private boolean exact = false;
	private float exp = 0;
	private int cookingTime = 200;
	
	@Override
	public NamespacedKey getKey() {
		return this.key;
	}

	@Override
	public RecipeType getRecipeType() {
		return RecipeType.FURNACE;
	}

	@Override
	public ItemStack getResult() {
		return this.result;
	}

	@Override
	public void setResult(ItemStack result) {
		this.result = result;
	}

	@Override
	public boolean isNBTShape() {
		return this.exact;
	}

	@SuppressWarnings("deprecation")
	@Override
	public Recipe getRecipe() {
		RecipeChoice choice = this.exact ? new RecipeChoice.ExactChoice(this.input) : new RecipeChoice.MaterialChoice(this.input.getType());
		FurnaceRecipe recipe = new FurnaceRecipe(this.key, this.result, choice, this.exp, this.cookingTime);
		recipe.setGroup(this.group);
		return recipe;
	}

	@Override
	public void setGroup(String group) {
		this.group = group;
	}

	@Override
	public String getGroup() {
		return this.group;
	}

	@Override
	public void setInput(ItemStack input, boolean exact) {
		this.input = input;
		this.exact = exact;
	}

	@Override
	public void setCookingTime(int time) {
		this.cookingTime = time;
	}

	@Override
	public void setExp(float exp) {
		this.exp = exp;
	}
	
}
