package net.crytec.phoenix.api.redis;

import com.google.gson.JsonObject;

public abstract class RedisPacket {

	protected final String id;
	protected final RedisServerType type;

	public RedisPacket(String id, RedisServerType serverType) {
		this.id = id;
		this.type = serverType;
	}
	
	protected RedisPacket() {
		this.id = null;
		this.type = null;
	}

	public final String getID() {
		return this.id;
	}

	public final RedisServerType getReceiverType() {
		return this.type;
	}

	public abstract void onPacketSent(JsonObject data);

	public abstract void onPacketReceive(JsonObject data);

}
