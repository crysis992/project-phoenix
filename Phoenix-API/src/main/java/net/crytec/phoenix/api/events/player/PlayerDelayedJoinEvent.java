package net.crytec.phoenix.api.events.player;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * This event is called 3 Seconds after a player joins the server. <b> You may
 * use this for delayed database queries.
 * 
 * @author crysis
 *
 */
public class PlayerDelayedJoinEvent extends Event {
	private static final HandlerList handlers = new HandlerList();
	private Player _player;

	public PlayerDelayedJoinEvent(Player player) {
		this._player = player;
	}

	public Player getPlayer() {
		return this._player;
	}

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}
