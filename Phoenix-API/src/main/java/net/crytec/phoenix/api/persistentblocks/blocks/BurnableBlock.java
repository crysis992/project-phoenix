package net.crytec.phoenix.api.persistentblocks.blocks;

import org.bukkit.event.block.BlockBurnEvent;

public interface BurnableBlock extends PersistentBaseBlock {
	
	public abstract void onBurn(BlockBurnEvent event);
	
}
