package net.crytec.phoenix.api.chat.chatpause;

import org.bukkit.entity.Player;

import com.comphenix.protocol.events.PacketContainer;

public interface PhoenixChatQueue {

	public void registerPlayer(Player player);

	public boolean isRegistered(Player player);

	public void unregisterPlayer(Player player);

	public void unregisterPlayer(Player player, boolean sendInterceptedPackets);

	public void addPacket(Player player, PacketContainer packet);

}
