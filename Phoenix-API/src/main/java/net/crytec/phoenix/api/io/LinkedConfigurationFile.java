package net.crytec.phoenix.api.io;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class LinkedConfigurationFile {

	public LinkedConfigurationFile(String key, File containingFolder, boolean clearOnLoad) {
		this.file = new File(containingFolder, key + ".yml");
		this.config = YamlConfiguration.loadConfiguration(this.file);
		if (clearOnLoad) {
			this.clear();
		}
	}

	public LinkedConfigurationFile(String key, File containingFolder) {
		this(key, containingFolder, false);
	}

	private final File file;
	private final FileConfiguration config;

	public void save() {
		try {
			this.config.save(this.file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void write(String path, Object value) {
		this.config.set(path, value);
	}

	public void clear() {
		this.config.getKeys(false).forEach(key -> {
			this.config.set(key, null);
		});
	}
	
	public FileConfiguration getConfig() {
		return this.config;
	}

}
