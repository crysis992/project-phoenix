package net.crytec.phoenix.api.persistentblocks;

import java.util.Set;
import java.util.function.Supplier;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.data.BlockData;

import net.crytec.phoenix.api.persistentblocks.blocks.PersistentBaseBlock;
import net.crytec.phoenix.api.persistentblocks.blocks.PersistentMultiBlock;

public interface PhoenixPersistentBlocks {
	
	
	public boolean containsPersistentBlocks(Chunk chunk);
	
	public boolean containsPersistentBlocks(World world);
	
	public PersistentBaseBlock createBlock(String blockKey, Location location, BlockData blockData);
	
	public PersistentMultiBlock createMultiBlock(String blockKey, Location location, boolean airOnly);
	
	public Set<PersistentBaseBlock> getPersistentBlocksInChunk(Chunk chunk);
	
	public void removePersistentBlock(PersistentBaseBlock pBlock);
	
	public boolean isPersistent(Location location);
	
	public PersistentBaseBlock getPersistentBlockAt(Location location);
	
	public void registerPersistentBlock(String key, Supplier<? extends PersistentBaseBlock> supplier);

}
