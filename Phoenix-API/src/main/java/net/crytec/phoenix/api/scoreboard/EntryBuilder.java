package net.crytec.phoenix.api.scoreboard;

import java.util.LinkedList;
import java.util.List;

public class EntryBuilder {

	private LinkedList<Entry> entries = new LinkedList<>();

	/**
	 * Append a blank line.
	 *
	 * @return this
	 */
	public EntryBuilder blank() {
		return next("");
	}

	/**
	 * Append a new line with specified text.
	 *
	 * @param string
	 *            text
	 * @return this
	 */
	public EntryBuilder next(String string) {
		entries.add(new Entry(string, entries.size()));
		return this;
	}

	/**
	 * Returns a map of entries.
	 *
	 * @return map
	 */
	public List<Entry> build() {
		for (Entry entry : entries) {
			entry.setPosition(entries.size() - entry.getPosition());
		}
		return entries;
	}
}