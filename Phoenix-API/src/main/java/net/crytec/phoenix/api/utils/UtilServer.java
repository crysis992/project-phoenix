package net.crytec.phoenix.api.utils;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Entity;

import com.google.common.base.Preconditions;

/**
 * This class contains extra methods which are related to the whole server.
 */
public class UtilServer {

	/**
	 * Broadcast a message to a specific Rank
	 * 
	 * @param message
	 * @param permission
	 *            - The given permission
	 */
	public static void broadcast(String message, String permission) {
		broadcast(null, message, permission);
	}

	/**
	 * Broadcast a message to players with a specific rank
	 * 
	 * @param prefix
	 *            The prefix
	 * @param message
	 *            The Message
	 * @param permission
	 */
	public static void broadcast(String prefix, String message, String permission) {
		Bukkit.getOnlinePlayers().stream().filter(p -> p.hasPermission(permission)).forEach(cur -> cur.sendMessage(F.main((prefix == null) ? "Info " : prefix, message)));
	}

	/**
	 * Gets all entities over the server.
	 * 
	 * @return a list of entities
	 */
	public static List<Entity> getAllEntities() {
		List<Entity> e = new ArrayList<>();
		for (World w : Bukkit.getWorlds())
			e.addAll(w.getEntities());
		return e;
	}

	/**
	 * Gets all entities which are same species over the server.
	 * 
	 * @param entityClass
	 *            the class represents a type of entity
	 * @param <E>
	 *            the entity type
	 * @return list of entities
	 */
	public static <E extends Entity> List<Entity> getAllEntitiesByClass(Class<E> entityClass) {
		Preconditions.checkNotNull(entityClass);
		List<Entity> e = new ArrayList<>();
		for (World w : Bukkit.getWorlds())
			e.addAll(w.getEntitiesByClass(entityClass));
		return e;
	}
}
