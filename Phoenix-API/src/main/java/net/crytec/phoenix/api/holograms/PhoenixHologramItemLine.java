package net.crytec.phoenix.api.holograms;

import java.util.Set;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Consumer;

import com.google.common.collect.Sets;

public abstract class PhoenixHologramItemLine implements PhoenixHologramLine<ItemStack>{
	
	public PhoenixHologramItemLine(Location location, ItemStack itemStack, PhoenixHologram hologram) {
		this.location = location;
		this.itemStack = itemStack;
		this.hologram = hologram;
		this.clickActions = Sets.newHashSet();
	}
		
	private final Set<Consumer<Player>> clickActions;
	private final PhoenixHologram hologram;
	
	@Override
	public PhoenixHologram getHostingHologram() {
		return this.hologram;
	}
	
	@Override
	public void registerClickAction(Consumer<Player> action) {
		this.clickActions.add(action);
	}
		
	@Override
	public void onClick(Player player) {
		for(Consumer<Player> action : this.clickActions) {
			action.accept(player);
		}
	}
	
	private final Location location;
	private ItemStack itemStack;
	
	@Override
	public Location getLocation() {
		return this.location;
	}
	
	@Override
	public ItemStack getCurrentValue() {
		return this.itemStack;
	}
	
	@Override
	public HologramLineType getType() {
		return HologramLineType.ITEM_LINE;
	}
}
