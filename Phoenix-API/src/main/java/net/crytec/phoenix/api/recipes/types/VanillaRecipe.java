package net.crytec.phoenix.api.recipes.types;

import org.bukkit.inventory.Recipe;

public interface VanillaRecipe extends PhoenixRecipe {
	
	public Recipe getRecipe();
	public String getId();
	
	public boolean isDisabled();
	
	public void setDisabled(boolean disabled);

}
