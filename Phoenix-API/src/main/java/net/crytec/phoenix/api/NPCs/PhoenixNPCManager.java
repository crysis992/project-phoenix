package net.crytec.phoenix.api.NPCs;

import java.util.function.Function;

import javax.annotation.Nullable;

import org.bukkit.Location;
import org.bukkit.entity.Entity;

import net.crytec.phoenix.api.NPCs.customEntitys.CustomEntity;
import net.crytec.phoenix.api.NPCs.customEntitys.CustomEntityManager;

public abstract class PhoenixNPCManager {

	private static final String NPC_KEY = "CUSTOM_NPC";

	public PhoenixNPCManager(CustomEntityManager entityManager, Function<Location, CustomEntity> npcSupplier) {
		entityManager.getRegistry().register(NPC_KEY, npcSupplier);
		this.entityManager = entityManager;
	}

	private final CustomEntityManager entityManager;

	@Nullable
	public CustomEntity getNPC(Entity entity) {
		return this.isNPC(entity) ? this.entityManager.getCustomEntity(entity).get() : null;
	}

	public boolean isNPC(Entity entity) {
		if (!this.entityManager.isCustomEntity(entity))
			return false;
		return this.entityManager.getCustomEntity(entity).get().getKey().equals(NPC_KEY);
	}

	public CustomEntity createNPC(Location location) {
		return this.entityManager.createEntity(NPC_KEY, location);
	}

}
