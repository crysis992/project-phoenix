package net.crytec.phoenix.api.player;

import java.util.function.Consumer;

import org.bukkit.entity.Player;

public interface PlayerInput {
	
	public void getPlayerChatInput(Player player, Consumer<String> result);

}
