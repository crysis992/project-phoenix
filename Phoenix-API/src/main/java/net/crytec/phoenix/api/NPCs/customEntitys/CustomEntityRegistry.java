package net.crytec.phoenix.api.NPCs.customEntitys;

import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import org.bukkit.Location;

import com.google.common.collect.Maps;

public class CustomEntityRegistry {
	
	protected CustomEntityRegistry() {
		this.entityMap = Maps.newHashMap();
	}
	
	private final Map<String, Function<Location, CustomEntity>> entityMap;
	
	protected Optional<Function<Location, CustomEntity>> getEntityOf(String key){
		return Optional.ofNullable(this.entityMap.get(key));
	}
	
	public void register(String key, Function<Location, CustomEntity> entity) {
		this.entityMap.put(key, entity);
	}
	
	public boolean isRegistered(String key) {
		return this.entityMap.containsKey(key);
	}
	
}
