package net.crytec.phoenix.api.io.data;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.IllegalPluginAccessException;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.Maps;

import net.crytec.phoenix.api.io.events.PlayerDataLoadEvent;
import net.crytec.phoenix.api.io.events.PlayerDataUnloadEvent;

public class PluginStorage {

	private final JavaPlugin plugin;
	private final File dataPath;

	private HashMap<UUID, HashMap<String, PlayerData>> playerData = Maps.newHashMap();
	private HashMap<String, Class<? extends PlayerData>> storageAdapters = Maps.newHashMap();

	public PluginStorage(JavaPlugin plugin) {
		this.plugin = plugin;
		this.dataPath = new File(plugin.getDataFolder(), "playerstorage");
		if (!dataPath.exists()) {
			dataPath.mkdirs();
		}
	}

	public void addStorageAdapter(Class<? extends PlayerData> clazz) {
		this.storageAdapters.put(clazz.getName(), clazz);
	}

	public void loadPlayer(UUID uuid) {

		File cfg = new File(this.dataPath, uuid.toString() + ".yml");
		if (!cfg.exists()) {
			try {
				cfg.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		YamlConfiguration config = YamlConfiguration.loadConfiguration(cfg);
		playerData.putIfAbsent(uuid, Maps.newHashMap());
		
		for (String key : storageAdapters.keySet()) {
			try {
				Class<? extends PlayerData> clazz = storageAdapters.get(key);

				if (clazz.getConstructors().length != 1 || clazz.getConstructors()[0].getParameterCount() >= 1) {
					throw new IllegalPluginAccessException("The use of an constructor with parameters is forbidden at class " + clazz.getName());
				}

				PlayerData data = clazz.newInstance();
				data.initialize(this.plugin, uuid, cfg, config);

				playerData.get(uuid).put(key, data);
				data.loadData(config);
				data.initialize();
				
				PlayerDataLoadEvent event = new PlayerDataLoadEvent(uuid, this.plugin, clazz);
				Bukkit.getPluginManager().callEvent(event);
				
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}

	public void unloadPlayer(UUID uuid) {
		File cfg = new File(this.dataPath, uuid.toString() + ".yml");
		YamlConfiguration config = YamlConfiguration.loadConfiguration(cfg);

		if (!playerData.containsKey(uuid)) return;
		
		for (PlayerData data : playerData.get(uuid).values()) {
			data.saveData(config);
		}

		try {
			config.save(cfg);
			PlayerDataUnloadEvent event = new PlayerDataUnloadEvent(uuid, this.plugin);
			Bukkit.getPluginManager().callEvent(event);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public PlayerData getPlayerData(Player player, String key) {
		return this.getPlayerData(player.getUniqueId(), key);
	}

	@SuppressWarnings("unchecked")
	public <T extends PlayerData> T getPlayerData(UUID uuid, String key) {
		return (T) playerData.get(uuid).get(key);
	}

}
