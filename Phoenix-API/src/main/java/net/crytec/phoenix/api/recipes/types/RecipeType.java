package net.crytec.phoenix.api.recipes.types;

public enum RecipeType {
	
	FURNACE,
	SHAPED,
	SHAPELESS,
	STONECUTTER,
	VANILLA,
	SPECIAL
	
}
