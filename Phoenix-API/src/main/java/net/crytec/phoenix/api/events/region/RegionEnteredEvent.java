package net.crytec.phoenix.api.events.region;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;

import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import net.crytec.phoenix.api.worldguard.MovementWay;

public class RegionEnteredEvent extends RegionEvent {
	/**
	 * creates a new RegionEnteredEvent
	 * 
	 * @param region   the region the player entered
	 * @param player   the player who triggered the event
	 * @param movement the type of movement how the player entered the region
	 * @param parent
	 */
	public RegionEnteredEvent(ProtectedRegion region, Player player, MovementWay movement, Event parent) {
		super(region, player, movement, parent);
	}
}
