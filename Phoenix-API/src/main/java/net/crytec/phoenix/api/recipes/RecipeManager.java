package net.crytec.phoenix.api.recipes;

import org.bukkit.NamespacedKey;

import net.crytec.phoenix.api.recipes.types.CustomFurnaceRecipe;
import net.crytec.phoenix.api.recipes.types.CustomShapedRecipe;
import net.crytec.phoenix.api.recipes.types.CustomShapelessRecipe;
import net.crytec.phoenix.api.recipes.types.PhoenixRecipe;

public interface RecipeManager {
	
	public void registerRecipe(PhoenixRecipe recipe);
	public void unregisterRecipe(PhoenixRecipe recipe);
	public CustomShapedRecipe createShapedRecipe(NamespacedKey key);
	public CustomShapelessRecipe createShapelessRecipe(NamespacedKey key);
	public CustomFurnaceRecipe createFurnaceRecipe(NamespacedKey key);
	public void updateRecipe(NamespacedKey oldRecipeKey, PhoenixRecipe newRecipe);
	
}