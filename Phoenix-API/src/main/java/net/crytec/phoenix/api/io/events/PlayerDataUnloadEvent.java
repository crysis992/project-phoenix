package net.crytec.phoenix.api.io.events;

import java.util.UUID;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

public class PlayerDataUnloadEvent extends Event {

	public PlayerDataUnloadEvent(UUID player, JavaPlugin plugin) {
		this.player = player;
		this.plugin = plugin;
	}

	private final JavaPlugin plugin;
	private final UUID player;

	private static final HandlerList handlers = new HandlerList();

	public static HandlerList getHandlerList() {
		return handlers;
	}

	public HandlerList getHandlers() {
		return handlers;
	}

	public JavaPlugin getPlugin() {
		return plugin;
	}

	public UUID getPlayer() {
		return player;
	}

}
