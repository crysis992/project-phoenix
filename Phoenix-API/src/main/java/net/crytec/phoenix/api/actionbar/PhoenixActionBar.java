package net.crytec.phoenix.api.actionbar;

import java.util.UUID;

import org.bukkit.entity.Player;

import net.crytec.phoenix.api.actionbar.ActionBarSection.StringArrangement;

public interface PhoenixActionBar {
	
	public ActionBarSection getSection(UUID id);
	
	public void removeSection(ActionBarSection section, Player player, int index);
	
	public void removeSection(UUID id, Player player, int index);
	
	public ActionBarSection createSection(int length, StringArrangement arrangement, int priority);
	
	public void insertSection(Player player, int index, ActionBarSection section);
	
	public void updateFor(Player player);
	
	public ActionBarContainer getActionBarContainer(Player player);
	
	public ActionBarContainer createActionBarContainer(int sectionCount, int preferredLength, String sectionBorder);
	
	public void addPlayer(Player player, ActionBarContainer container);

}
