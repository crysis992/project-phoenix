package net.crytec.phoenix.api.inventory.implementation;

import java.util.Map;
import java.util.Optional;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Maps;

import net.crytec.phoenix.api.inventory.ClickableItem;
import net.crytec.phoenix.api.inventory.SmartInventory;
import net.crytec.phoenix.api.inventory.content.InventoryContents;
import net.crytec.phoenix.api.inventory.content.Pagination;
import net.crytec.phoenix.api.inventory.content.SlotIterator;
import net.crytec.phoenix.api.inventory.content.SlotPos;

public class InventoryContentImpl implements InventoryContents {

	private final Player holder;
	private SmartInventory inv;
	private ClickableItem[][] contents;

	private Pagination pagination = new PaginationImpl();
	private Map<String, SlotIterator> iterators = Maps.newHashMap();
	private Map<String, Object> properties = Maps.newHashMap();
	private Inventory inventory;
	
	public InventoryContentImpl(SmartInventory inv, Player player) {
		this.holder = player;
		this.inv = inv;
		if (inv.getType() == InventoryType.CHEST || inv.getType() == InventoryType.ENDER_CHEST) {
			this.inventory = Bukkit.createInventory(player, inv.getColumns() * inv.getRows(), inv.getTitle());
		} else {
			this.inventory = Bukkit.createInventory(player, inv.getType(), inv.getTitle());
		}
		this.contents = new ClickableItem[inv.getRows()][inv.getColumns()];
	}

	@Override
	public SmartInventory inventory() {
		return inv;
	}

	@Override
	public Pagination pagination() {
		return pagination;
	}

	@Override
	public Optional<SlotIterator> iterator(String id) {
		return Optional.ofNullable(this.iterators.get(id));
	}

	@Override
	public SlotIterator newIterator(String id, SlotIterator.Type type, int startRow, int startColumn) {
		SlotIterator iterator = new SlotIteratorImplementation(this, inv, type, startRow, startColumn);

		this.iterators.put(id, iterator);
		return iterator;
	}

	@Override
	public SlotIterator newIterator(String id, SlotIterator.Type type, SlotPos startPos) {
		return newIterator(id, type, startPos.getRow(), startPos.getColumn());
	}

	@Override
	public SlotIterator newIterator(SlotIterator.Type type, int startRow, int startColumn) {
		return new SlotIteratorImplementation(this, inv, type, startRow, startColumn);
	}

	@Override
	public SlotIterator newIterator(SlotIterator.Type type, SlotPos startPos) {
		return newIterator(type, startPos.getRow(), startPos.getColumn());
	}

	@Override
	public ClickableItem[][] all() {
		return contents;
	}

	@Override
	public Optional<SlotPos> firstEmpty() {
		for (int column = 0; column < contents[0].length; column++) {
			for (int row = 0; row < contents.length; row++) {
				if (!this.get(row, column).isPresent())
					return Optional.of(new SlotPos(row, column));
			}
		}

		return Optional.empty();
	}

	@Override
	public Optional<ClickableItem> get(int row, int column) {
		if (row >= contents.length)
			return Optional.empty();
		if (column >= contents[row].length)
			return Optional.empty();

		return Optional.ofNullable(contents[row][column]);
	}

	@Override
	public Optional<ClickableItem> get(SlotPos slotPos) {
		return get(slotPos.getRow(), slotPos.getColumn());
	}

	@Override
	public InventoryContents set(int row, int column, ClickableItem item) {
		if (row >= contents.length)
			return this;
		if (column >= contents[row].length)
			return this;

		contents[row][column] = item;
		update(row, column, item != null ? item : null);
		return this;
	}

	@Override
	public InventoryContents set(SlotPos slotPos, ClickableItem item) {
		return set(slotPos.getRow(), slotPos.getColumn(), item);
	}

	@Override
	public InventoryContents set(int slot, ClickableItem item) {
		return set(SlotPos.of(slot), item);
	}

	@Override
	public InventoryContents add(ClickableItem item) {
		for (int row = 0; row < contents.length; row++) {
			for (int column = 0; column < contents[0].length; column++) {
				if (contents[row][column] == null) {
					set(row, column, item);
					return this;
				}
			}
		}

		return this;
	}

	@Override
	public InventoryContents fill(ClickableItem item) {
		for (int row = 0; row < contents.length; row++)
			for (int column = 0; column < contents[row].length; column++)
				set(row, column, item);

		return this;
	}

	@Override
	public InventoryContents fillRow(int row, ClickableItem item) {
		if (row >= contents.length)
			return this;

		for (int column = 0; column < contents[row].length; column++)
			set(row, column, item);

		return this;
	}

	@Override
	public InventoryContents fillColumn(int column, ClickableItem item) {
		for (int row = 0; row < contents.length; row++)
			set(row, column, item);

		return this;
	}

	@Override
	public InventoryContents fillBorders(ClickableItem item) {
		fillRect(0, 0, inv.getRows() - 1, inv.getColumns() - 1, item);
		return this;
	}

	@Override
	public InventoryContents fillRect(int fromRow, int fromColumn, int toRow, int toColumn, ClickableItem item) {
		for (int row = fromRow; row <= toRow; row++) {
			for (int column = fromColumn; column <= toColumn; column++) {
				if (row != fromRow && row != toRow && column != fromColumn && column != toColumn)
					continue;

				set(row, column, item);
			}
		}

		return this;
	}

	@Override
	public InventoryContents fillRect(SlotPos fromPos, SlotPos toPos, ClickableItem item) {
		return fillRect(fromPos.getRow(), fromPos.getColumn(), toPos.getRow(), toPos.getColumn(), item);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T property(String name) {
		return (T) properties.get(name);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T property(String name, T def) {
		return properties.containsKey(name) ? (T) properties.get(name) : def;
	}

	@Override
	public Map<String, Object> properties() {
		return properties;
	}

	@Override
	public InventoryContents setProperty(String name, Object value) {
		properties.put(name, value);
		return this;
	}

	private void update(int row, int column, ClickableItem item) {
		if (item == null || item.getItem() != null)
			return;
		this.inventory.setItem(inv.getColumns() * row + column, item.getItem());
	}

	@Override
	public InventoryContents updateMeta(SlotPos pos, ItemMeta meta) {
		int slot = inv.getColumns() * pos.getRow() + pos.getColumn();
		this.inventory.getItem(slot).setItemMeta(meta);
		return this;
	}

	@Override
	public Player getHolder() {
		return this.holder;
	}

	@Override
	public Inventory getInventory() {
		return this.inventory;
	}
}