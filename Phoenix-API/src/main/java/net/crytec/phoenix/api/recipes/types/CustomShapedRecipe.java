package net.crytec.phoenix.api.recipes.types;

import java.util.Map;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.RecipeChoice;

public interface CustomShapedRecipe extends PhoenixRecipe{
	
	public Map<Character, RecipeChoice> getChoiceMap();
	public void setMatrix(ItemStack[] matrix);
	public ItemStack[] getMatrix();
	
	@SuppressWarnings("deprecation")
	public default boolean isNBTShape() {
		return this.getChoiceMap().values().stream().anyMatch(choice -> choice instanceof RecipeChoice.ExactChoice);
	}
	
}
