package net.crytec.phoenix.api.miniplugin;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.apache.commons.lang3.Validate;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.Maps;

import net.crytec.phoenix.api.io.PluginConfig;

public abstract class MiniPlugin implements Listener {
	private final String moduleName;

	private static File _dataFolder;
	private static File _configFolder;
	
	protected JavaPlugin _plugin;
	protected PluginConfig _config;
	
	protected File _cfgFile;
	private final HashMap<String, YamlConfiguration> data = Maps.newHashMap();

	public MiniPlugin(String moduleName, JavaPlugin plugin) {
		this.moduleName = moduleName;
		this._plugin = plugin;

		_dataFolder = new File(plugin.getDataFolder() + File.separator + "data");
		if (!_dataFolder.exists()) {
			_dataFolder.mkdirs();
		}
	}

	public final JavaPlugin getPlugin() {
		return this._plugin;
	}

	public final void enable() {
		try {
			this.onEnable();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public final void disable() {
		HandlerList.unregisterAll(this);
		this.onDisable();
		this.data.keySet().forEach(d -> this.saveDataFile(d));
	}
	
	protected abstract void onEnable();
	
	protected abstract void onDisable();
	
	public final PluginConfig getConfig() {
		return this._config;
	}

	public final void saveConfig() {
		Validate.notNull(_config, getName() + " requires the loadConfig() method! Cannot write to config!");
		_config.saveConfig();
	}

	public final void reloadConfig() {
		Validate.notNull(_config, getName() + " requires the loadConfig() method! Cannot write to config!");
		_config.reloadConfig(true);
	}

	protected void loadConfig() {
		_configFolder = new File(_plugin.getDataFolder() + File.separator + "addons");
		if (!_configFolder.exists()) {
			_configFolder.mkdir();
		}

		this._config = new PluginConfig(_plugin, _configFolder, this.moduleName + ".yml");
	}

	/**
	 * Use it in the enable method to initialize configuration objects
	 * 
	 * @param path
	 * @param value
	 */
	public final void setConfigEntry(String path, Object value) {
		Validate.notNull(_config, getName() + " requires the loadConfig() method! Cannot write to config!");
		if (!_config.isSet(path)) {
			_config.set(path, value);
			this.saveConfig();
		}
	}

	/**
	 * Returns the module name.
	 * 
	 * @return The name of this addon
	 */
	public final String getName() {
		return this.moduleName;
	}
	
	public final YamlConfiguration getDataFile(String dataFile) {

		if (this.data.containsKey(dataFile)) {
			return this.data.get(dataFile);
		} else {

			File file = new File(_dataFolder, dataFile + ".yml");

			if (!file.exists()) {
				try {
					file.createNewFile();
				} catch (IOException ex) {
					ex.printStackTrace();
				}
				YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
				this.data.put(dataFile, config);
				return config;
			} else {
				YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
				this.data.put(dataFile, config);
				return config;
			}
		}
	}
	
	public final void saveDataFile(String dataFile) {
		if (!this.data.containsKey(dataFile))
			return;

		File file = new File(_dataFolder, dataFile + ".yml");
		try {
			this.data.get(dataFile).save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sends a colored message to the console.
	 * 
	 * @param message
	 */
	protected final void log(String message) {
		Bukkit.getConsoleSender().sendMessage("[" + this.moduleName + "] " + message);
	}
}
