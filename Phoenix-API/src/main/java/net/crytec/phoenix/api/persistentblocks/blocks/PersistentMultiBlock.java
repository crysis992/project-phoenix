package net.crytec.phoenix.api.persistentblocks.blocks;

import java.util.Map;

import org.bukkit.Location;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.BlockData;

public interface PersistentMultiBlock extends PersistentBaseBlock {
	
	public Map<Location, BlockData> getBlockGrid();
	
	public void onAssemble(BlockFace direction);

}
