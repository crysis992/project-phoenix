package net.crytec.phoenix.api.io;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.EnumUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import net.crytec.phoenix.api.utils.UtilLoc;

public class PluginConfig extends YamlConfiguration {
	
	private final File file;
	private final JavaPlugin plugin;
		
	public PluginConfig(JavaPlugin plugin, File path, String configname) {
		this.plugin = plugin;
		File configfilepath = new File(path, configname);
		if (!path.exists()) {
			path.mkdirs();
		}
		if (!configfilepath.exists()) {
			try {
				if (plugin.getResource(configname) != null) {
					plugin.saveResource(configname, false);
					FileUtils.moveFile(new File(plugin.getDataFolder(), configname), configfilepath);
					plugin.getLogger().info("Created default configuration - " + configname);
				} else {
					configfilepath.createNewFile();
				}
			} catch (IOException e) {
				plugin.getLogger().severe("Failed to create configuration file for " + configname + "!");
				plugin.getLogger().severe("Path: " + path.getAbsolutePath() + " Filename: " + configname);
				e.printStackTrace();
			}
		}
		this.file = configfilepath;
		try {
			this.load(this.file);
		} catch (IOException | InvalidConfigurationException e) {
			Bukkit.getLogger().severe("Failed to deserialize configuration: " + e.getMessage());
		}
	}
	
	public void reloadConfig(boolean save) {
		if (save) this.saveConfig();
		try {
			this.load(this.file);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Saves the configuration file to the disk.
	 */
	public void saveConfig() {
		try {
			this.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Updates the configuration file with the given path and default value if its not set.<p>
	 * 
	 * Note: Does <b>NOT</b> save the configuration.
	 * @param path
	 * @param def
	 * @return True if the configuration entry was updated/set
	 */
	public boolean update(String path, Object def) {
		if (!this.isSet(path)) {
			this.set(path, def);
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Returns a material based on it's enum name
	 * @param path
	 * @return Returns the material or a BARRIER if it's invalid
	 */
	public Material getMaterial(String path) {
		return this.getMaterial(path, Material.BARRIER);
	}
	
	/**
	 * Returns a material based on it's enum name
	 * @param path
	 * @return Returns the material or the default material
	 */
	public Material getMaterial(String path, Material def) {
		if (this.isSet(path)) {
			boolean valid = EnumUtils.isValidEnum(Material.class, this.getString(path));
			if (!valid) {
				plugin.getLogger().severe("Invalid Material in config " + file.getName() + " at path: " + path + " No Material found for: " + this.getString(path));
				return def;
			}
			return Material.valueOf(this.getString(path));
		} else {
			return def;
		}
	}
	
	/**
	 * Save the given Location to the configuration.
	 * @param path
	 * @param location
	 */
	public void setLocation(String path, Location location) {
		this.set(path, UtilLoc.toString(location));
	}
	
	/**
	 * Load a location from the configuration.
	 * @param path
	 * @return The Location Object
	 */
	public Location getLocation(String path) {
		return UtilLoc.parseString(this.getString(path));
	}
	
	public void setInventoryContent(String path, Inventory inventory) {
		this.set(path, inventory.getContents());
	}
	
	public void setItemStackArray(String path, ItemStack[] array) {
		this.set(path, array);
	}
	
	@SuppressWarnings("unchecked")
	public ItemStack[] getItemStackArray(String path) {
		return ((List<ItemStack>) this.get(path)).toArray(new ItemStack[0]);
	}
	
	public ItemStack[] getInventoryContents(String path, ItemStack[] def) {
		return this.isSet(path) ? getItemStackArray(path) : def;	
	}
	
	/**
	 * Returns a Location from a config, or the default value if it's not saved.
	 * @param path
	 * @param def
	 * @return The Location object or the default value
	 */
	public Location getLocation(String path, Location def) {
		return this.isSet(path) ? UtilLoc.parseString(this.getString(path)) : def;		
	}
	
	public File getFile() {
		return this.file;
	}
}