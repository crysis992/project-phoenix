package net.crytec.phoenix.api.chat.chatpause;

import java.util.Queue;
import java.util.stream.Stream;

import org.apache.commons.collections4.queue.CircularFifoQueue;

import com.comphenix.protocol.events.PacketContainer;

public class PacketJam<E extends PacketContainer> {

	public PacketJam(int MAX_PACKETS) {
		this.packetJam = new CircularFifoQueue<E>(MAX_PACKETS);
		this.MAX_PACKETS = MAX_PACKETS;
	}

	private final int MAX_PACKETS;

	private final Queue<E> packetJam;

	public void append(E packet) {
		if (this.packetJam.size() < MAX_PACKETS)
			this.packetJam.offer(packet);
	}

	public Stream<E> release() {
		return this.packetJam.stream();
	}

	public Stream<E> parallelRelease() {
		return this.packetJam.parallelStream();
	}
}