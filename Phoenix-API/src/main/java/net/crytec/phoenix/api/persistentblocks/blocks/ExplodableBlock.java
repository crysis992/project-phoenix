package net.crytec.phoenix.api.persistentblocks.blocks;

import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

public interface ExplodableBlock extends PersistentBaseBlock  {
	
	public void onExplode(BlockExplodeEvent event);
	public void onExplode(EntityExplodeEvent event);
	
}
