package net.crytec.phoenix.api.NPCs.customEntitys;

import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;

import javax.annotation.Nullable;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class CustomEntityManager implements Listener{
	
	private static final String ENTITY_TAG = "__CUSTOM_ENTITY";
	private static final String DATA_TAG = "__CUSTOM_ENTITY_DATA";
	protected static final String NAME_TAG = "__CUSTOM_ENTITY_NAME";
	
	public CustomEntityManager(JavaPlugin host) {
		this.registry = new CustomEntityRegistry();
		this.loadedEntitys = Maps.newHashMap();
		this.gson = new Gson();
		
		Bukkit.getScheduler().runTaskLater(host, () ->{
			for(World world : Bukkit.getWorlds()) {
				for(Chunk chunk : world.getLoadedChunks()) {
					this.loadCustomEntities(chunk);
				}
			}
		}, 120L);
		
		Bukkit.getPluginManager().registerEvents(this, host);
	}
	
	private final Map<UUID, CustomEntity> loadedEntitys;
	
	private final Gson gson;
	private final CustomEntityRegistry registry;
	
	public CustomEntityRegistry getRegistry() {
		return registry;
	}
	
	public Optional<CustomEntity> getCustomEntity(Entity entity){
		return Optional.ofNullable(this.loadedEntitys.get(entity.getUniqueId()));
	}
	
	@Nullable
	public CustomEntity createEntity(String key, Location location) {
		Optional<Function<Location, CustomEntity>> optEntitySupplier = this.registry.getEntityOf(key);
		if(!optEntitySupplier.isPresent()) return null;
		CustomEntity entity = optEntitySupplier.get().apply(location);
		entity.getBukkitEntity().getScoreboardTags().add(ENTITY_TAG);
		this.loadedEntitys.put(entity.getBukkitEntity().getUniqueId(), entity);
		entity.getBukkitEntity().getScoreboardTags().add(NAME_TAG + "_#_" + key);
		return entity;
	}
	
	public boolean isCustomEntity(Entity entity) {
		return entity.getScoreboardTags().contains(ENTITY_TAG);
	}
	
	private void loadData(Entity entity) {
		CustomEntity cEntity = this.loadedEntitys.get(entity.getUniqueId());
		String dataString = null;
		for(String tag : entity.getScoreboardTags()) {
			if(tag.startsWith(DATA_TAG)) {
				dataString = tag.substring(DATA_TAG.length() + 3);
				break;
			}
		}
		if(dataString == null) return;
		JsonObject dataObject = this.gson.fromJson(dataString, JsonObject.class);
		cEntity.loadData(dataObject);
		cEntity.onLoad();
	}
	
	private void saveData(Entity entity) {
		CustomEntity cEntity = this.loadedEntitys.get(entity.getUniqueId());
		cEntity.onSave();
		
		JsonObject dataObject = new JsonObject();
		cEntity.saveData(dataObject);
		String dataString = "_#_" + gson.toJson(dataObject);
		String oldData = null;
		Set<String> scoreboardData = entity.getScoreboardTags();
		for(String tag : scoreboardData) {
			if(tag.startsWith(DATA_TAG)) {
				oldData = tag;
				break;
			}
		}
		if(oldData != null) {
			scoreboardData.remove(oldData);
		}
		scoreboardData.add(DATA_TAG + dataString);
	}
	
	@EventHandler
	public void onEntityDeath(EntityDeathEvent event) {
		if(this.isCustomEntity(event.getEntity())) {
			UUID entityID = event.getEntity().getUniqueId();
			this.loadedEntitys.get(entityID).onDeath();
			this.loadedEntitys.remove(entityID);
		}
	}
	
	private void loadCustomEntities(Chunk chunk) {
		for(Entity entity : chunk.getEntities()) {
			if(this.isCustomEntity(entity)) {
				tagloop:
				for(String tag : entity.getScoreboardTags()) {
					if(tag.startsWith(NAME_TAG)) {
						String type = tag.substring(NAME_TAG.length() + 3);
						if(this.registry.isRegistered(type)) {
							CustomEntity cEntity = this.createEntity(type, entity.getLocation());
							Entity bEntity = cEntity.getBukkitEntity();
							
							for(String tagg : entity.getScoreboardTags()) {
								bEntity.getScoreboardTags().add(tagg);
							}
							
							this.loadData(bEntity);
							this.loadedEntitys.put(bEntity.getUniqueId(), cEntity);
							entity.remove();
						}
						break tagloop;
					}
				}
//				entity.remove();
			}
		}
	}
	
	private void saveCustomEntities(Chunk chunk) {
		for(Entity entity : chunk.getEntities()) {
			if(this.isCustomEntity(entity)) {
				this.saveData(entity);
				this.loadedEntitys.remove(entity.getUniqueId());
			}
		}
	}
	
	@EventHandler
	public void onChunkLoad(ChunkLoadEvent event) {
		this.loadCustomEntities(event.getChunk());
	}
	
	@EventHandler
	public void onChunkUnload(ChunkUnloadEvent event) {
		this.saveCustomEntities(event.getChunk());
	}
}
