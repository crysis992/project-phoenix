package net.crytec.phoenix.api.miniplugin;

import net.crytec.phoenix.api.io.PluginConfig;

public interface PhoenixAddon {
	
	public void onEnable();
	
	public default void onDisable() {
		
	}
	
	public String getAddonName();
	
	public default boolean hasConfig() {
		return false;
	}
	
	public default boolean hasEvents() {
		return true;
	}
	
	public PluginConfig getConfig();
	
	public void loadConfig();
	public void saveConfig();
	
}