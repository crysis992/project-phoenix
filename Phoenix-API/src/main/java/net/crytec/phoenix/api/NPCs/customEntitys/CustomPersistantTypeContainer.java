package net.crytec.phoenix.api.NPCs.customEntitys;

import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import com.google.common.collect.Lists;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class CustomPersistantTypeContainer {
	
	public CustomPersistantTypeContainer() {
		this.strings = new TypeMap<String>();
		this.ints = new TypeMap<Integer>();
		this.doubles = new TypeMap<Double>();
		this.floats = new TypeMap<Float>();
		this.booleans = new TypeMap<Boolean>();
		this.stringLists = new TypeMap<List<String>>();
	}
	
	protected void saveTo(JsonObject json) {
		JsonObject stringCompound = new JsonObject();
		JsonObject intCompound = new JsonObject();
		JsonObject doubleCompound = new JsonObject();
		JsonObject flaotCompound = new JsonObject();
		JsonObject stringListCompound = new JsonObject();
		JsonObject booleanCompound = new JsonObject();
		
		for(Entry<String, Boolean> entry : this.booleans) {
			booleanCompound.addProperty(entry.getKey(), entry.getValue());
		}
		
		for(Entry<String, String> entry : this.strings) {
			stringCompound.addProperty(entry.getKey(), entry.getValue());
		}
		
		for(Entry<String, Integer> entry : this.ints) {
			intCompound.addProperty(entry.getKey(), entry.getValue());
		}
		
		for(Entry<String, Double> entry : this.doubles) {
			doubleCompound.addProperty(entry.getKey(), entry.getValue());
		}
		
		for(Entry<String, Float> entry : this.floats) {
			flaotCompound.addProperty(entry.getKey(), entry.getValue());
		}
		
		for(Entry<String, List<String>> entry : this.stringLists) {
			JsonObject listObject = new JsonObject();
			List<String> list = entry.getValue();
			for(int i = 0; i < list.size(); i++) {
				listObject.addProperty("" + i, list.get(i));
			}
			stringListCompound.add(entry.getKey(), listObject);
		}
		
		json.add("Booleans", booleanCompound);
		json.add("Strings", stringCompound);
		json.add("Integers", intCompound);
		json.add("Doubles", doubleCompound);
		json.add("Floats", flaotCompound);
		json.add("StringLists", stringListCompound);
	}
	
	protected void laodFrom(JsonObject json) {
		JsonObject stringCompound = json.get("Strings").getAsJsonObject();
		JsonObject intCompound = json.get("Integers").getAsJsonObject();
		JsonObject doubleCompound = json.get("Doubles").getAsJsonObject();
		JsonObject flaotCompound = json.get("Floats").getAsJsonObject();
		JsonObject stringListCompound = json.get("StringLists").getAsJsonObject();
		JsonObject booleanCompound = json.get("Booleans").getAsJsonObject();
		
		for(Entry<String, JsonElement> entry : booleanCompound.entrySet()) {
			this.booleans.put(entry.getKey(), entry.getValue().getAsBoolean());
		}
		
		for(Entry<String, JsonElement> entry : stringCompound.entrySet()) {
			this.strings.put(entry.getKey(), entry.getValue().getAsString());
		}
		
		for(Entry<String, JsonElement> entry : intCompound.entrySet()) {
			this.ints.put(entry.getKey(), entry.getValue().getAsInt());
		}
		
		for(Entry<String, JsonElement> entry : doubleCompound.entrySet()) {
			this.doubles.put(entry.getKey(), entry.getValue().getAsDouble());
		}
		
		for(Entry<String, JsonElement> entry : flaotCompound.entrySet()) {
			this.floats.put(entry.getKey(), entry.getValue().getAsFloat());
		}
		
		for(Entry<String, JsonElement> entry : stringListCompound.entrySet()) {
			Set<Entry<String, JsonElement>> listAsSet = entry.getValue().getAsJsonObject().entrySet();
			List<String> list = Lists.newArrayList(new String[listAsSet.size()]);
			for(Entry<String, JsonElement> listEntry : listAsSet) {
				list.set(Integer.valueOf(listEntry.getKey()), listEntry.getValue().getAsString());
			}
			this.stringLists.put(entry.getKey(), list);
		}
		
	}
	
	private final TypeMap<Boolean> booleans;
	private final TypeMap<String> strings;
	private final TypeMap<Integer> ints;
	private final TypeMap<Double> doubles;
	private final TypeMap<Float> floats;
	private final TypeMap<List<String>> stringLists;
	
	public boolean containsString(String key) {
		return this.strings.containsKey(key);
	}
	
	public boolean containsInteger(String key) {
		return this.ints.containsKey(key);
	}
	
	public boolean containsDouble(String key) {
		return this.doubles.containsKey(key);
	}
	
	public boolean containsFloat(String key) {
		return this.floats.containsKey(key);
	}
	
	public boolean containsStringList(String key) {
		return this.stringLists.containsKey(key);
	}
	
	public boolean containsBoolean(String key) {
		return this.booleans.containsKey(key);
	}
	
	public void add(String key, boolean bool) {
		this.booleans.put(key, bool);
	}
	
	public void add(String key, String value) {
		this.strings.put(key, value);
	}
	
	public void add(String key, Integer value) {
		this.ints.put(key, value);
	}
	
	public void add(String key, Double value) {
		this.doubles.put(key, value);
	}
	
	public void add(String key, Float value) {
		this.floats.put(key, value);
	}
	
	public void add(String key, List<String> value) {
		this.stringLists.put(key, value);
	}
	
	public Boolean getBoolean(String key) {
		return this.booleans.get(key);
	}
	
	public String getString(String key) {
		return this.strings.get(key);
	}
	
	public Integer getInt(String key) {
		return this.ints.get(key);
	}
	
	public Double getDouble(String key) {
		return this.doubles.get(key);
	}
	
	public Float getFloat(String key) {
		return this.floats.get(key);
	}
	
	public List<String> getStringList(String key){
		return this.stringLists.get(key);
	}
	
}
