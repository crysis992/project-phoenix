package net.crytec.phoenix.api.io;

import java.util.HashMap;
import java.util.UUID;

import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.HashBiMap;
import com.google.common.collect.Maps;

import net.crytec.phoenix.api.io.data.PlayerData;
import net.crytec.phoenix.api.io.data.PluginStorage;

public class PlayerDataManager implements Listener {

	private final HashBiMap<UUID, Integer> internalPlayerIDs = HashBiMap.create();
	private final HashMap<Class<? extends JavaPlugin>, PluginStorage> plugins = Maps.newHashMap();

	private static PlayerDataManager instance;
	
	private static boolean initialized = false;
	
	private final JavaPlugin host;

	public PlayerDataManager(JavaPlugin plugin) {
		Validate.isTrue(!initialized, "PlayerDataManager cannot be instantiated!");

		this.host = plugin;
		PlayerDataManager.instance = this;
		Bukkit.getPluginManager().registerEvents(this, plugin);
		initialized = true;
	}

	@EventHandler
	public void cacheDataOnLogin(AsyncPlayerPreLoginEvent event) {
		final UUID uuid = event.getUniqueId();

		try {
			this.internalPlayerIDs.putIfAbsent(uuid, (internalPlayerIDs.size() + 1));
		} catch (IllegalArgumentException exception) {
			Bukkit.getLogger().severe("[PhoenixAPI] Failed to initialize data for UUID " + uuid + "(" + event.getName() + "): " + exception.getMessage());
			Bukkit.getScheduler().runTask(host, () -> attemptLater(uuid));
		}
	}
	
	private void attemptLater(UUID uuid) {
		try {
			this.internalPlayerIDs.putIfAbsent(uuid, (internalPlayerIDs.size() + 1));
		} catch (IllegalArgumentException exception) {
			Bukkit.getLogger().severe("[PhoenixAPI] Failed to initialize data for UUID " + uuid.toString() + " Plugin related data may be broken until a relog!");
		}
	}

	public void registerStorageAdapter(JavaPlugin plugin, Class<? extends PlayerData> dataClass) {
		this.plugins.computeIfAbsent(plugin.getClass(), (c) -> new PluginStorage(plugin)).addStorageAdapter(dataClass);
	}

	public int getInternalId(UUID uuid) {
		return this.internalPlayerIDs.get(uuid);
	}

	public int getInternalId(Player player) {
		return this.internalPlayerIDs.get(player.getUniqueId());
	}

	public UUID getUUIDFromId(int id) {
		return internalPlayerIDs.inverse().getOrDefault(id, null);
	}

	public PluginStorage getPluginStorage(JavaPlugin plugin) {
		return this.plugins.get(plugin.getClass());
	}

	public <T extends PlayerData> T getPlayerData(UUID player, JavaPlugin plugin, Class<? extends PlayerData> dataClass) {
		return this.getPluginStorage(plugin).getPlayerData(player, dataClass.getName());
	}

	public void loadData(UUID uuid) {
		this.plugins.values().forEach(storage -> storage.loadPlayer(uuid));
	}

	public void loadData(JavaPlugin plugin, UUID uuid) {
		this.getPluginStorage(plugin).loadPlayer(uuid);
	}

	public void unloadData(UUID uuid) {
		this.plugins.values().forEach(storage -> storage.unloadPlayer(uuid));
	}

	public void unloadData(JavaPlugin plugin, UUID uuid) {
		this.getPluginStorage(plugin).unloadPlayer(uuid);
	}

	@EventHandler(priority = EventPriority.LOW)
	public void loadDataOnJoin(PlayerJoinEvent event) {
		this.loadData(event.getPlayer().getUniqueId());
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void unloadPlayerData(PlayerQuitEvent event) {
		this.plugins.values().forEach(storage -> storage.unloadPlayer(event.getPlayer().getUniqueId()));
	}
	
	@EventHandler
	public void onPluginDisable(PluginDisableEvent event) {
		if (this.plugins.containsKey(event.getPlugin().getClass())) {
			Bukkit.getOnlinePlayers().forEach(cur -> this.plugins.get(event.getPlugin().getClass()).unloadPlayer(cur.getUniqueId()));
			this.plugins.remove(event.getPlugin().getClass());
		}
	}

	public static PlayerDataManager getInstance() {
		return instance;
	}
}