package net.crytec.phoenix.api.persistentblocks.blocks;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;

public interface InventoryBlock extends PersistentBaseBlock  {
	
	public abstract Inventory getInventory();
	
	public default void saveInventory(ConfigurationSection config) {
		ArrayList<ItemStack> items = Lists.newArrayList(this.getInventory().getStorageContents());
		config.set("Inventory", items);
	}
	
	@SuppressWarnings("unchecked")
	public default void loadInventory(ConfigurationSection config) {
		List<ItemStack> items = (List<ItemStack>) config.getList("Inventory");
		this.getInventory().setContents(items.toArray(new ItemStack[items.size()]));
	}
	
}