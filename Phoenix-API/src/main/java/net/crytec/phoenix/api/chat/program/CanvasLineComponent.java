package net.crytec.phoenix.api.chat.program;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.util.Consumer;

import com.google.common.collect.Lists;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.HoverEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;

public class CanvasLineComponent {

	public CanvasLineComponent(String string, boolean clickable, Consumer<Player> consumer) {
		this.textComponent = new TextComponent(string);
		this.componentID = UUID.randomUUID();
		this.descriptionLines = Lists.newArrayList();
		this.clickable = clickable;
		this.consumer = consumer;
		if (clickable) {
			ChatEditorCore.getInstance().registerCanvasComponent(this);
		}
	}

	public CanvasLineComponent(String string, Consumer<Player> consumer) {
		this(string, true, consumer);
	}

	public CanvasLineComponent(String text) {
		this(text, false, null);
	}

	public void unregister() {
		if (this.isClickable()) {
			ChatEditorCore.getInstance().unregisterCanvasComponent(componentID);
		}
	}

	private final UUID componentID;
	private final boolean clickable;
	private TextComponent textComponent;
	private final ArrayList<String> descriptionLines;
	private final Consumer<Player> consumer;
	private final List<Object> data = Lists.newArrayList();

	private String hover = "";

	public CanvasLineComponent setHover(String text) {
		this.hover = text;
		return this;
	}

	public CanvasLineComponent setData(int index, Object data) {
		this.data.add(index, data);
		return this;
	}

	protected TextComponent getComponent(Player player) {

		ComponentBuilder builder = new ComponentBuilder("");

		for (String line : this.descriptionLines) {
			builder.append(line);
		}

		if (this.clickable) {
			this.textComponent.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/ccline " + this.componentID.toString()));
			if (!hover.isEmpty()) {
				ComponentBuilder hoverBuilder = new ComponentBuilder(this.hover);
				this.textComponent.setHoverEvent(new HoverEvent(Action.SHOW_TEXT, hoverBuilder.create()));
			}
		}

		return this.textComponent;
	}

	protected UUID getComponentID() {
		return componentID;
	}

	protected boolean isClickable() {
		return clickable;
	}

	public ArrayList<String> getDescriptionLines() {
		return descriptionLines;
	}

	protected Consumer<Player> getConsumer() {
		return consumer;
	}

	@SuppressWarnings("unchecked")
	public <T> T getData(int index) {
		return (T) data.get(index);
	}
}