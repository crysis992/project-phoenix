package net.crytec.phoenix.api.miniblocks;

import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.collect.Table;

import net.crytec.phoenix.api.events.player.PlayerReceiveChunkEvent;
import net.crytec.phoenix.api.events.player.PlayerUnloadsChunkEvent;
import net.crytec.phoenix.api.utils.UtilChunk;

public abstract class MiniBlockManager implements Listener {
	
	public MiniBlockManager(JavaPlugin host, BiFunction<ItemStack, Location, MiniBlock> miniBlockFeeder) {
		this.miniBlockTable = HashBasedTable.<String, Long, Set<MiniBlock>>create();
		this.miniBlockFeeder = miniBlockFeeder;
		this.viewerMap = Maps.newHashMap();
		this.showingBlocksMap = Maps.newHashMap();
		Bukkit.getPluginManager().registerEvents(this, host);
	}
	
	private final BiFunction<ItemStack, Location, MiniBlock> miniBlockFeeder;
	private final Table<String, Long, Set<MiniBlock>> miniBlockTable;
	private final Map<Player, Set<MiniBlock>> viewerMap;
	private final Map<MiniBlock, Set<Player>> showingBlocksMap;
	
	public MiniBlock createMiniBlock(ItemStack item, Location location) {
		MiniBlock mini = miniBlockFeeder.apply(item, location);
		String worldName = location.getWorld().getName();
		long chunkKey = UtilChunk.getChunkKey(location);
		
		this.showingBlocksMap.put(mini, Sets.newHashSet());
		if(!this.miniBlockTable.contains(worldName, chunkKey)) {
			this.miniBlockTable.put(worldName, chunkKey, Sets.newHashSet());
		}
		this.miniBlockTable.get(worldName, chunkKey).add(mini);
		
		for(Player player : location.getWorld().getPlayers()) {
			if(UtilChunk.getChunkViewOf(player).contains(chunkKey)) {
				mini.showTo(player);
			}
		}
		
		return mini;
	}
	
	public void removeMiniBlock(MiniBlock mini) {
		for(Player player : this.showingBlocksMap.get(mini)) {
			mini.hideFrom(player);
			this.viewerMap.get(player).remove(mini);
		}
		this.showingBlocksMap.remove(mini);
		Location miniLocation = mini.getLocation();
		String worldName = miniLocation.getWorld().getName();
		long chunkKey = UtilChunk.getChunkKey(miniLocation);
		Set<MiniBlock> blockSet = this.miniBlockTable.get(worldName, chunkKey);
		blockSet.remove(mini);
		if(blockSet.isEmpty()) {
			this.miniBlockTable.remove(worldName, chunkKey);
		}
	}
	
	private void showMiniBlocks(long chunkKey, Player player) {
		String worldName = player.getWorld().getName();
		if(this.miniBlockTable.contains(worldName, chunkKey)) {
			for(MiniBlock mini : this.miniBlockTable.get(worldName, chunkKey)) {
				mini.showTo(player);
				this.showingBlocksMap.get(mini).add(player);
				this.viewerMap.get(player).add(mini);
			}
		}
	}
	
	private void hideMiniBlocks(long chunkKey, Player player) {
		String worldName = player.getWorld().getName();
		if(this.miniBlockTable.contains(worldName, chunkKey)) {
			for(MiniBlock mini : this.miniBlockTable.get(worldName, chunkKey)) {
				mini.hideFrom(player);
				this.showingBlocksMap.get(mini).remove(player);
				this.viewerMap.get(player).remove(mini);
			}
		}
	}
	
	@EventHandler
	public void onChunkShow(PlayerReceiveChunkEvent event) {
		this.showMiniBlocks(event.getChunkKey(), event.getPlayer());
	}
	
	@EventHandler
	public void onChunkHiding(PlayerUnloadsChunkEvent event) {
		this.hideMiniBlocks(event.getChunkKey(), event.getPlayer());
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		this.viewerMap.put(event.getPlayer(), Sets.newHashSet());
	}
	
	@EventHandler
	public void onLeave(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		for(MiniBlock mini : this.viewerMap.get(player)) {
			this.showingBlocksMap.get(mini).remove(player);
		}
		this.viewerMap.remove(player);
	}
	
}
