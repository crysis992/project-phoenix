package net.crytec.phoenix.api.version;

import java.util.List;
import net.crytec.phoenix.api.NPCs.PhoenixNPCManager;
import net.crytec.phoenix.api.anvil.AnvilImplementation;
import net.crytec.phoenix.api.holograms.PhoenixHologramManager;
import net.crytec.phoenix.api.holograms.infobars.InfoBarManager;
import net.crytec.phoenix.api.io.language.Language;
import net.crytec.phoenix.api.item.ItemFactory;
import net.crytec.phoenix.api.miniblocks.MiniBlockManager;
import net.crytec.phoenix.api.recipes.RecipeManager;
import net.crytec.phoenix.api.version.ServerVersion.BukkitVersion;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

public interface ImplementationHandler {
	
	public BukkitVersion getVersion();

	public void sendActionbar(Player player, String message);
	
	/**
	 * Do not call this method!
	 */
	public void startup(JavaPlugin plugin);

	/**
	 * Do not use this class, use the AnvilGUI class
	 * 
	 */
	public AnvilImplementation getAnvilWrapper();

	/**
	 * Gets the language helper to translate varius names to the clients
	 * language
	 * 
	 * @return Returns the Language that is currently in use
	 */
	public Language getLanguageHelper();

	/**
	 * Do not use this class, use the ItemBuilder class
	 * 
	 * @param item
	 * @return
	 */
	public ItemFactory getItemFactory(Material material);

	/**
	 * Do not use this class, use the ItemBuilder class
	 * 
	 * @param item
	 * @return
	 */
	public ItemFactory getItemFactory(ItemStack item);

	/**
	 * Serializes an ItemStack into the Mojang Json Format
	 * 
	 * @param The
	 *            {@link ItemStack} you want to serialize
	 * @return Returns the serialized Json String
	 */
	public String serializeItemStack(ItemStack item);

	/**
	 * Deserializes a Mojang Json String to an ItemStack.
	 * 
	 * @param The
	 *            serializes json string
	 * @return - Returns either the itemstack or null if the string is invalid
	 */
	public ItemStack deserializeItemStack(String string);
	
	
	public PhoenixHologramManager getHologramManager();
	
	public PhoenixNPCManager getNPCManager();
	
	public RecipeManager getRecipeManager();
	
	public MiniBlockManager getMiniBlockManager();
	
	public InfoBarManager getInfoBarManager();
	
	public List<ItemStack> breakBlock(Player player, Block block, ItemStack tool);
	
}
