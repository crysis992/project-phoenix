package net.crytec.phoenix.api.holograms;

import java.util.function.Predicate;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public interface PhoenixHologramFactory {
	
	public PhoenixHologram supplyHologram(Location location, Predicate<Player> viewFilter, PhoenixHologramManager manager);
	
}
