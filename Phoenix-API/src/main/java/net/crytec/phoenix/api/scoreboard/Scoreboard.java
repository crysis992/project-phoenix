package net.crytec.phoenix.api.scoreboard;

import org.bukkit.entity.Player;

/**
 * Represents an advanced scoreboard that can display up to 48 characters in a single entry.
 *
 */
public interface Scoreboard {

    /**
     * Activate the scoreboard.
     */
	public void activate();

    /**
     * Deactivate the scoreboard.
     */
	public void deactivate();

    /**
     * Determine if the scoreboard has been already activated.
     *
     * @return activated
     */
	public boolean isActivated();

    /**
     * Returns the handler for this scoreboard.
     *
     * @return handler
     */
	public ScoreboardHandler getHandler();

    /**
     * Set the handler for this scoreboard.
     *
     * @param handler handler
     */
	public Scoreboard setHandler(ScoreboardHandler handler);
    /**
     * Returns the holder of this scoreboard.
     *
     * @return holder
     */
	public Player getHolder();

}