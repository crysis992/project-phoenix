package net.crytec.phoenix.api.persistentblocks.blocks;

import net.crytec.phoenix.api.holograms.PhoenixHologram;

public interface HolographicBlock extends PersistentBaseBlock  {
	
	public void updateHologram();
	public int updateTime();
	public PhoenixHologram getHologram();
	
}