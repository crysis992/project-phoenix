package net.crytec.phoenix.api.persistentblocks.blocks;

import org.bukkit.event.player.PlayerInteractEvent;

public interface InteractableBlock extends PersistentBaseBlock  {
	
	public abstract void onInteract(PlayerInteractEvent event);
	
}
