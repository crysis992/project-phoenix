package net.crytec.phoenix.api.itemactions;

import java.util.Map;
import java.util.function.BiConsumer;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.Maps;

import net.crytec.phoenix.api.nbt.NBTCompound;
import net.crytec.phoenix.api.nbt.NBTItem;

public class ItemActions implements Listener {
	
	private static ItemActions instance;
	private static final String PLACE_KEY = "_PlaceckActions";
	private static final String INTERACT_KEY = "_ClickActions";
	
	public ItemActions(JavaPlugin plugin) {
		instance = this;
		Bukkit.getPluginManager().registerEvents(this, plugin);
		this.interactActions = Maps.newHashMap();
		this.placeActions = Maps.newHashMap();
	}
	
	private final Map<String, BiConsumer<PlayerInteractEvent, String>> interactActions;
	private final Map<String, BiConsumer<BlockPlaceEvent, String>> placeActions;
	
	public static void registerPlayerInteractAction(String key, BiConsumer<PlayerInteractEvent, String> eventConsumer) {
		instance.interactActions.put(key, eventConsumer);
	}
	
	public static void registerBlockPlaceAction(String key, BiConsumer<BlockPlaceEvent, String> eventConsumer) {
		instance.placeActions.put(key, eventConsumer);
	}
	
	public static ItemStack tagPlaceBlockItem(ItemStack item, String key, String value) {
		NBTItem nbt = new NBTItem(item);
		if(!nbt.hasKey(PLACE_KEY)) {
			nbt.addCompound(PLACE_KEY).setString(key, value);
			return nbt.getItem();
		}
		
		nbt.getCompound(PLACE_KEY).setString(key, value);
		return nbt.getItem();
	}
	
	public static ItemStack tagPlayerInteractItem(ItemStack item, String key, String value) {
		NBTItem nbt = new NBTItem(item);
		if(!nbt.hasKey(INTERACT_KEY)) {
			nbt.addCompound(INTERACT_KEY).setString(key, value);
			return nbt.getItem();
		}
		
		nbt.getCompound(INTERACT_KEY).setString(key, value);
		return nbt.getItem();
	}
	
	@EventHandler
	public void onPlace(BlockPlaceEvent event) {
		ItemStack item = event.getItemInHand();
		if(item == null) return;
		NBTItem nbt = new NBTItem(item);
		if(nbt.hasKey(PLACE_KEY)) {
			NBTCompound compound = nbt.getCompound(PLACE_KEY);
			for(String key : compound.getKeys()) {
				if(this.placeActions.containsKey(key)) {
					this.placeActions.get(key).accept(event, compound.getString(key));
				}
			}
		}
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent event) {
		ItemStack item = event.getItem();
		if(item == null) return;
		NBTItem nbt = new NBTItem(item);
		if(nbt.hasKey(INTERACT_KEY)) {
			NBTCompound compound = nbt.getCompound(INTERACT_KEY);
			for(String key : compound.getKeys()) {
				if(this.interactActions.containsKey(key)) {
					this.interactActions.get(key).accept(event, compound.getString(key));
				}
			}
		}
	}
}