package net.crytec.phoenix.api.io.events;

import java.util.UUID;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

import net.crytec.phoenix.api.io.data.PlayerData;

public class PlayerDataLoadEvent extends Event {

	public PlayerDataLoadEvent(UUID player, JavaPlugin plugin, Class<? extends PlayerData> dataClass) {
		this.dataClass = dataClass;
		this.player = player;
		this.plugin = plugin;
	}

	private final JavaPlugin plugin;
	private final UUID player;
	private final Class<? extends PlayerData> dataClass;

	private static final HandlerList handlers = new HandlerList();

	public static HandlerList getHandlerList() {
		return handlers;
	}

	public HandlerList getHandlers() {
		return handlers;
	}

	public JavaPlugin getPlugin() {
		return plugin;
	}

	public UUID getPlayer() {
		return player;
	}

	public Class<? extends PlayerData> getDataClass() {
		return dataClass;
	}

}
