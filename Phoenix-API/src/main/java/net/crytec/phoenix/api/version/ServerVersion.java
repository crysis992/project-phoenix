package net.crytec.phoenix.api.version;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * A class to determine the version of the bukkit implementation Supports all
 * versions from 1.13.x to 1.14.x
 * 
 * @author crysis992
 *
 */
public final class ServerVersion {

	public final BukkitVersion version;

	public ServerVersion(JavaPlugin plugin) {

		String version = Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];

		BukkitVersion effectiveVersion = BukkitVersion.UNKNOWN;

		try {
			BukkitVersion v = BukkitVersion.valueOf(version);
			effectiveVersion = v;
		} catch (Exception ex) {
			plugin.getLogger().severe("Failed to parse your server version. You might run an incompatible build!");
		}

		this.version = effectiveVersion;
	}

	public BukkitVersion getVersion() {
		return this.version;
	}

	public enum BukkitVersion {

		UNKNOWN("Unknown Server Implementation", "unknown"), 
		v1_13_R1("1.13", "v13_0"), 
		v1_13_R2("1.13.x", "v13_0"), 
		v1_14_R1("1.14.x", "v14_0");

		private String displayName;
		private String packageName;

		private BukkitVersion(String displayname, String packageName) {
			this.displayName = displayname;
			this.packageName = packageName;
		}

		public String getVersionImplementation() {
			return new StringBuilder("This Server is now running PhoenixAPI implementation for version ").append(this.displayName).toString();
		}

		public String getDisplayName() {
			return displayName;
		}

		public String getPackageName() {
			return packageName;
		}

	}

}
