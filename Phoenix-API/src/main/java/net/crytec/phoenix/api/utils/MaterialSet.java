package net.crytec.phoenix.api.utils;

import java.util.Collection;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.Tag;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.data.BlockData;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class MaterialSet implements Tag<Material> {

	private final NamespacedKey key;
	private final Set<Material> materials;

	public MaterialSet(NamespacedKey key, Predicate<Material> filter) {
		this(key, Stream.of(Material.values()).filter(filter).collect(Collectors.toList()));
	}

	public MaterialSet(NamespacedKey key, Material... materials) {
		this(key, Lists.newArrayList(materials));
	}

	public MaterialSet(NamespacedKey key, Collection<Material> materials) {
		this.key = key;
		this.materials = Sets.newEnumSet(materials, Material.class);
	}

	@Override
	public NamespacedKey getKey() {
		return key;
	}

	@SuppressWarnings("unchecked")
	public MaterialSet add(Tag<Material>... tags) {
		for (Tag<Material> tag : tags) {
			add(tag.getValues());
		}
		return this;
	}

	public MaterialSet add(MaterialSet... tags) {
		for (Tag<Material> tag : tags) {
			add(tag.getValues());
		}
		return this;
	}

	public MaterialSet add(Material... material) {
		this.materials.addAll(Lists.newArrayList(material));
		return this;
	}

	public MaterialSet add(Collection<Material> materials) {
		this.materials.addAll(materials);
		return this;
	}

	public MaterialSet contains(String with) {
		return add(mat -> mat.name().contains(with));
	}

	public MaterialSet endsWith(String with) {
		return add(mat -> mat.name().endsWith(with));
	}

	public MaterialSet startsWith(String with) {
		return add(mat -> mat.name().startsWith(with));
	}
	@SuppressWarnings("deprecation")

	public MaterialSet add(Predicate<Material> filter) {
		add(Stream.of(Material.values()).filter(((Predicate<Material>) Material::isLegacy).negate()).filter(filter).collect(Collectors.toList()));
		return this;
	}

	public MaterialSet not(MaterialSet tags) {
		not(tags.getValues());
		return this;
	}

	public MaterialSet not(Material... material) {
		this.materials.removeAll(Lists.newArrayList(material));
		return this;
	}

	public MaterialSet not(Collection<Material> materials) {
		this.materials.removeAll(materials);
		return this;
	}

	@SuppressWarnings("deprecation")

	public MaterialSet not(Predicate<Material> filter) {
		not(Stream.of(Material.values()).filter(((Predicate<Material>) Material::isLegacy).negate()).filter(filter).collect(Collectors.toList()));
		return this;
	}

	public MaterialSet notEndsWith(String with) {
		return not(mat -> mat.name().endsWith(with));
	}

	public MaterialSet notStartsWith(String with) {
		return not(mat -> mat.name().startsWith(with));
	}

	public Set<Material> getValues() {
		return this.materials;
	}

	public boolean isTagged(BlockData block) {
		return isTagged(block.getMaterial());
	}

	public boolean isTagged(BlockState block) {
		return isTagged(block.getType());
	}

	public boolean isTagged(Block block) {
		return isTagged(block.getType());
	}

	public boolean isTagged(ItemStack item) {
		return isTagged(item.getType());
	}

	public boolean isTagged(Material material) {
		return this.materials.contains(material);
	}
}