package net.crytec.phoenix.api.NPCs.customEntitys;

import org.bukkit.entity.Entity;

import com.google.gson.JsonObject;

public interface CustomEntity {
	
	public default void loadData(JsonObject json) {
		this.getDataContainer().laodFrom(json);
	}
	
	public default void saveData(JsonObject json) {
		this.getDataContainer().saveTo(json);
	}
	
	public abstract CustomPersistantTypeContainer getDataContainer();
	public abstract void despawn();
	public abstract void onDeath();
	public abstract Entity getBukkitEntity();
	public abstract void onLoad();
	public abstract void onSave();
	
	public default String getKey() {
		for(String tag : this.getBukkitEntity().getScoreboardTags()) {
			if(tag.startsWith(CustomEntityManager.NAME_TAG)) {
				return tag.substring(CustomEntityManager.NAME_TAG.length() + 3);
			}
		}
		return null;
	}
	
}