package net.crytec.phoenix.api.holograms;

import java.util.Set;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Consumer;

import com.google.common.collect.Sets;

import net.crytec.phoenix.api.holograms.HologramLineType;

public abstract class PhoenixHologramTextLine implements PhoenixHologramLine<String>{
	
	public PhoenixHologramTextLine(Location location, String text, PhoenixHologram hologram) {
		this.lineLocation = location;
		this.hologram = hologram;
		this.clickActions = Sets.newHashSet();
	}
	
	private final Set<Consumer<Player>> clickActions;
	private final PhoenixHologram hologram;
	
	@Override
	public PhoenixHologram getHostingHologram() {
		return this.hologram;
	}
	
	@Override
	public void registerClickAction(Consumer<Player> action) {
		this.clickActions.add(action);
	}
	
	@Override
	public void onClick(Player player) {
		for(Consumer<Player> action : this.clickActions) {
			action.accept(player);
		}
	}
	
	@Override
	public HologramLineType getType() {
		return HologramLineType.TEXT_LINE;
	}
	
	@Override
	public Location getLocation() {
		return this.lineLocation;
	}
	
	@Override
	public String getCurrentValue() {
		return this.text;
	}
	
	private final Location lineLocation;
	protected String text;
	
}
