package net.crytec.phoenix.api.scoreboard;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public interface PhoenixBoardManager {

	public void addPlayer(Player player, String priority);
	public void addPlayer(Player player);
	public void unregisterPlayer(Player player);
	public void setBoard(Player player, ScoreboardHandler handler);
	public PhoenixBoard getBoard(Player player);
	public void resetBoard(Player player);
	public void setPrefix(Player player, String prefix);
	public void setSuffix(Player player, String suffix);
	public void setHandler(JavaPlugin plugin);
	
}
