package net.crytec.phoenix.api.persistentblocks.blocks;

import org.bukkit.Location;

public interface MovableBlock extends PersistentBaseBlock  {
	
	public abstract boolean onMove(Location oldLoc, Location newLoc);
	
}
