package net.crytec.phoenix.api.utils;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import net.crytec.phoenix.api.io.language.FormatLang;
import net.crytec.phoenix.api.io.language.Language;

public class F {
		
	private static String YES;
	private static String NO;
	private static String ON;
	private static String OFF;
	
	public static void init(Language language) {
		F.YES = language.translate(FormatLang.YES.getLocalePath());
		F.NO = language.translate(FormatLang.NO.getLocalePath());
		F.ON = StringUtils.capitalize(language.translate(FormatLang.ON.getLocalePath()).toLowerCase());
		F.OFF = StringUtils.capitalize(language.translate(FormatLang.OFF.getLocalePath()).toLowerCase());
	}
	
	public static void consoleLog(String module, String body) {
		String message = new StringBuilder().append(ChatColor.AQUA).append(module).append("> ").append(ChatColor.GOLD).append(body).toString();
		Bukkit.getConsoleSender().sendMessage(message);
	}

	public static void log(String module, String body) {
		String message = new StringBuilder(module).append("> ").append(body).toString();
		Bukkit.getLogger().info(message);
	}

	public static String main(String module, String body) {
		return new StringBuilder().append(ChatColor.BLUE).append(module).append(" > ").append(ChatColor.GRAY).append(body).toString();
	}

	public static String error(String prefix, String body) {
		return new StringBuilder().append(ChatColor.DARK_RED).append(prefix).append(" > ").append(ChatColor.GRAY).append(body).toString();
	}
	
	public static String error(String body) {
		return error("Error", body);
	}

	/**
	 * General Message
	 * 
	 * @param elem
	 * @return The formatted String
	 */
	public static String elem(String elem) {
		return new StringBuilder().append(ChatColor.LIGHT_PURPLE).append(elem).append(ChatColor.GRAY).toString();
		
	}

	/**
	 * Playernames, Mobnames etc
	 * 
	 * @param elem
	 * @return The formatted String
	 */
	public static String name(String elem) {
		return new StringBuilder().append(ChatColor.YELLOW).append(elem).append(ChatColor.GRAY).toString();
	}
	/**
	 * Returns On or Off
	 * 
	 * @param var
	 * @return The formatted String
	 */
	public static String oo(boolean var) {
		return var ? ON : OFF;
	}
	
	public static String ctf(boolean var, String e, String d) {
		return var ? e : d;
	}

	/**
	 * Returns Yes/No
	 * 
	 * @param var
	 * @return The formatted String
	 */
	public static String tf(boolean var) {
		return var ? YES : NO;
	}

	/**
	 * Format a iterable Stringlist/Map to a readable String, separated with
	 * separator
	 * 
	 * @param objects
	 * @param separator
	 * @param ifEmpty
	 * @return a formated String, like player1, player2, player3, player4..
	 */
	public static String format(Iterable<?> objects, String separator, String ifEmpty) {
		if (!objects.iterator().hasNext()) {
			return ifEmpty;
		}
		StringBuilder sb = new StringBuilder();
		objects.spliterator().forEachRemaining(con -> sb.append(con.toString() + separator));
		return sb.toString().substring(0, (sb.toString().length() - separator.length()));
	}
	
	public static String ProgressBar(double current, double max, int length, String fragment) {
		StringBuilder bar = new StringBuilder();
		String full = (ChatColor.GREEN + fragment);
		String empty = (ChatColor.RED + fragment);
		double percent = (100 / max) * current;
		int fullfrags = (int) Math.round(percent / (100 / length));
		int emptyfrags = length - fullfrags;

		while (fullfrags > 0) {
			bar.append(full);
			fullfrags--;
		}
		while (emptyfrags > 0) {
			bar.append(empty);
			emptyfrags--;
		}
		return bar.toString();
	}
}