package net.crytec.phoenix.api.bukkitchain;

import java.util.LinkedList;

import javax.annotation.Nullable;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.util.Consumer;

import com.google.common.collect.Lists;

public class  BukkitChain<T> {
	
	public static <T> BukkitChainBuilder<T> builder(JavaPlugin plugin) {
		return new BukkitChainBuilder<T>(plugin);
	}
	
	private BukkitChain(JavaPlugin plugin) {
		this.plugin = plugin;
		this.scheduler = Bukkit.getScheduler();
		this.actionOrder = Lists.<SyncedAction<T>>newLinkedList();
	}
	
	private final BukkitScheduler scheduler;
	private final JavaPlugin plugin;
	private final LinkedList<SyncedAction<T>> actionOrder;
	
	public void callOn(T object) {
		actionOrder.peek().run(object);
	}
	
	private void addSync(Consumer<T> action) {
		SyncedAction<T> head = this.actionOrder.peekLast();
		SyncedAction<T> synced = new SyncedAction<T>(SyncOption.SYNC, action, plugin, scheduler);
		if(head != null) {
			head.child = synced;
		}
		this.actionOrder.add(synced);
	}
	
	private void addAsync(Consumer<T> action) {
		SyncedAction<T> head = this.actionOrder.peekLast();
		SyncedAction<T> synced = new SyncedAction<T>(SyncOption.ASYNC, action, plugin, scheduler);
		if(head != null) {
			head.child = synced;
		}
		this.actionOrder.add(synced);
	}
	
	public static class BukkitChainBuilder<T> {
		
		public BukkitChainBuilder(JavaPlugin plugin) {
			this.chain = new BukkitChain<T>(plugin);
		}
		
		private final BukkitChain<T> chain;
		
		public BukkitChain<T> build() {
			return this.chain;
		}
		
		public BukkitChainBuilder<T> nextSync(Consumer<T> action){
			this.chain.addSync(action);
			return this;
		}
		
		public BukkitChainBuilder<T> nextAsync(Consumer<T> action){
			this.chain.addAsync(action);
			return this;
		}
		
	}
	
	private static class SyncedAction<T> {
		
		public SyncedAction(SyncOption type, Consumer<T> consumer, JavaPlugin plugin, BukkitScheduler scheduler) {
			this.consumer = consumer;
			this.type = type;
			this.scheduler = scheduler;
			this.plugin = plugin;
		}
		
		private final BukkitScheduler scheduler;
		private final JavaPlugin plugin;
		private final Consumer<T> consumer;
		private final SyncOption type;
		@Nullable
		private SyncedAction<T> child = null;
		
		public void run(T object) {
			if(this.type.isSync) {
				scheduler.runTask(plugin, () -> {
					consumer.accept(object);
					if(child != null) {
						child.run(object);
					}
				});
			}else {
				scheduler.runTaskAsynchronously(plugin, () -> {
					consumer.accept(object);
					if(child != null) {
						child.run(object);
					}
				});
			}
		}
		
	}
	
	private static enum SyncOption {
		
		SYNC(true),
		ASYNC(false);
		
		private SyncOption(boolean sync) {
			this.isSync = sync;
		}
		
		private final boolean isSync;
		
	}
	
}
