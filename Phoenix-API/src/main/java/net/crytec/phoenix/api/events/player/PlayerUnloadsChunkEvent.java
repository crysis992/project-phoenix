package net.crytec.phoenix.api.events.player;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PlayerUnloadsChunkEvent extends Event {

	private static final HandlerList handlers = new HandlerList();

	private final long chunkKey;
	private final Player player;

	public PlayerUnloadsChunkEvent(Player who, long chunkKey) {
		this.chunkKey = chunkKey;
		this.player = who;
	}

	public long getChunkKey() {
		return chunkKey;
	}

	public Player getPlayer() {
		return player;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	public HandlerList getHandlers() {
		return handlers;
	}
	
}