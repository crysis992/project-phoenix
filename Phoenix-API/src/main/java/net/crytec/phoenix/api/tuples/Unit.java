package net.crytec.phoenix.api.tuples;

public class Unit <K>{
	
	public static <K> Unit<K> of(K unit){
		return new Unit<K>(unit);
	}
	
	private Unit(K unit) {
		this.unit = unit;
	}
	
	private K unit;
	
	public K getUnit() {
		return unit;
	}
	
	public void setUnit(K unit) {
		this.unit = unit;
	}
	
}
