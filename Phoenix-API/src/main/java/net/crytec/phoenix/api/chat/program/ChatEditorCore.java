package net.crytec.phoenix.api.chat.program;

import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.Maps;

public class ChatEditorCore implements Listener {

	private static ChatEditorCore instance;
	private final Map<UUID, CanvasLineComponent> components;

	public ChatEditorCore(JavaPlugin plugin) {
		instance = this;
		Bukkit.getPluginManager().registerEvents(this, plugin);
		this.components = Maps.newHashMap();
	}

	public CanvasLineComponent componentOf(UUID id) {
		return this.components.get(id);
	}

	public void registerCanvasComponent(CanvasLineComponent component) {
		this.components.put(component.getComponentID(), component);
	}

	public void unregisterCanvasComponent(UUID componentID) {
		this.components.remove(componentID);
	}

	public void unregisterCanvasComponent(CanvasLineComponent component) {
		this.unregisterCanvasComponent(component.getComponentID());
	}

	public static ChatEditorCore getInstance() {
		return instance;
	}

	@EventHandler(ignoreCancelled = false, priority = EventPriority.MONITOR)
	public void parse(PlayerCommandPreprocessEvent event) {
		if (!event.getMessage().isEmpty() && !event.getMessage().startsWith("/ccline"))
			return;
		event.setCancelled(true);

		UUID canvasID = UUID.fromString(event.getMessage().split(" ")[1]);
		if (canvasID == null)
			return;
		CanvasLineComponent component = this.componentOf(canvasID);
		if (component == null)
			return;
		component.getConsumer().accept(event.getPlayer());
	}
}
