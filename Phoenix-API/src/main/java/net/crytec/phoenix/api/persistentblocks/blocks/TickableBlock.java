package net.crytec.phoenix.api.persistentblocks.blocks;

public interface TickableBlock extends PersistentBaseBlock  {
	
	public void onTick();
	public int getTicks();
	
}
