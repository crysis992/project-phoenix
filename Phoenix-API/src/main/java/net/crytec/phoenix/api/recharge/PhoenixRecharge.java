package net.crytec.phoenix.api.recharge;

import org.bukkit.entity.Player;

public interface PhoenixRecharge {

	/**
	 * Use a ability
	 * 
	 * @param player
	 *            - The player
	 * @param ability
	 *            - The ability name
	 * @param recharge
	 *            - Recharge time in milliseconds.
	 * @param inform
	 *            - Inform the player when the ability is recharged.
	 * @param attachItem
	 *            - Attach the current item
	 * @return
	 */
	public boolean use(Player player, String ability, long recharge, boolean inform, boolean attachItem);

	/**
	 * Use a ability
	 * 
	 * @param player
	 *            - The player
	 * @param ability
	 *            - The ability name
	 * @param abilityFull
	 *            - The formatted ability name
	 * @param recharge
	 *            - Recharge time in milliseconds.
	 * @param inform
	 *            - Inform the player when the ability is recharged.
	 * @param attachItem
	 *            - Attach the current item
	 * @return
	 */
	public boolean use(Player player, String ability, String abilityFull, long recharge, boolean inform, boolean attachItem);

	/**
	 * Bypass the cooldown check and forces a new cooldown.
	 * 
	 * @param player
	 * @param ability
	 * @param recharge
	 */
	public void useForce(Player player, String ability, long recharge);

	/**
	 * Checks if the given ability is usable.
	 * 
	 * @param player
	 * @param ability
	 * @return
	 */
	public boolean isUsable(Player player, String ability);

	/**
	 * Recharge a ability to be usable again.
	 * 
	 * @param player
	 * @param ability
	 */
	public void recharge(Player player, String ability);

	/**
	 * Reset a player, clearing all ability cooldowns.
	 * 
	 * @param player
	 */
	public void reset(Player player);

}
