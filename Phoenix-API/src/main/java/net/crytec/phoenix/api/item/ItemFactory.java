package net.crytec.phoenix.api.item;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.attribute.Attribute;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

public interface ItemFactory {
	
	public ItemStack build();
	
	public ItemFactory amount(int amount);
	
	public ItemFactory name(String name);
	
	public ItemFactory lore(String string);
	
	public ItemFactory lore(String string, int line);
	
	public ItemFactory lore(List<String> lore);
	
	public ItemFactory setUnbreakable(boolean unbreakable);
	
	public ItemFactory setDurability(int durability);
	
	public ItemFactory enchantment(Enchantment enchantment, int level);
	
	public ItemFactory enchantment(Enchantment enchantment);
	
	public ItemFactory type(Material material);
	
	public ItemFactory clearLore();
	
	public ItemFactory clearEnchantment();
	
	public ItemFactory setSkullOwner(OfflinePlayer player);
	
	public ItemFactory setItemFlag(ItemFlag flag);
	
	public ItemFactory addNBTString(String key, String value);
	
	public ItemFactory addNBTDouble(String key, double value);
	
	public ItemFactory addNBTBoolean(String key, boolean value);
	
	public ItemFactory addNBTInt(String key, int value);
	
	public ItemFactory addNBTLong(String key, long value);
	
	public ItemFactory setAttribute(Attribute attribute, double amount, EquipmentSlot slot);
	
	public ItemFactory removeAttribute(Attribute attribute);
	
	public ItemFactory setModelData(int data);

}
