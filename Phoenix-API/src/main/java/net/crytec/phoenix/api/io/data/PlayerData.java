package net.crytec.phoenix.api.io.data;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public abstract class PlayerData {

	private JavaPlugin plugin;
	private UUID owner;
	private YamlConfiguration config;
	private File file;

	public void writeDefaults(YamlConfiguration config) {

	}

	public final void initialize(JavaPlugin plugin, UUID owner, File file, YamlConfiguration config) {
		this.plugin = plugin;
		this.owner = owner;
		this.config = config;
		this.file = file;

	}

	public abstract void initialize();

	public boolean isOwnerOnline() {
		return Bukkit.getPlayer(this.owner) != null;
	}

	public boolean saveConfig() {
		try {
			this.config.save(this.file);
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	public JavaPlugin getPlugin() {
		return plugin;
	}

	public UUID getOwner() {
		return owner;
	}

	public YamlConfiguration getConfig() {
		return config;
	}

	public File getFile() {
		return file;
	}

	public abstract void saveData(YamlConfiguration config);

	public abstract void loadData(YamlConfiguration config);
}
