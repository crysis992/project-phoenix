package net.crytec.phoenix.api.io.language;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

public interface Language {
	
	public String getEnchantment(Enchantment enchantment);
	
	public String getEnchantment(Enchantment enchantment, int level);
	
	public String getEntityName(Entity entity);

	public String getEntityName(EntityType type);
	
	public String getItemName(ItemStack item);
	
	public void setLanguage(EnumLang language);
	
	public EnumLang getLanguage();
	
	public String translate(String path);
}
