package net.crytec.phoenix.api.chat.program;

import java.util.ArrayList;

import org.bukkit.entity.Player;

import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.google.common.collect.Lists;

import net.crytec.phoenix.api.packets.ChatPacketWrapper;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.ComponentBuilder.FormatRetention;
import net.md_5.bungee.chat.ComponentSerializer;

public class CanvasLine {

	public CanvasLine(int index, ArrayList<CanvasLineComponent> defaultLine) throws IllegalArgumentException {
		if (index < 0 || index > 20)
			throw new IllegalArgumentException("Canvas index can only be between 0 and 20. Both included.");
		this.defaultLine = defaultLine;
		this.line = Lists.newArrayList();
		this.index = index;
	}

	public CanvasLine(int index) {
		this(index, Lists.newArrayList());
	}

	public void sendTo(Player player) {

		ChatPacketWrapper packet = new ChatPacketWrapper();
		packet.getHandle().setMeta("editor", true);

		packet.setMessage(WrappedChatComponent.fromJson(ComponentSerializer.toString(this.getComponentLine(player))));
		packet.sendPacket(player);
	}

	private final ArrayList<CanvasLineComponent> defaultLine;
	private final ArrayList<CanvasLineComponent> line;
	private int index;

	public CanvasLine addComponent(CanvasLineComponent component) {
		this.line.add(component);
		return this;
	}

	public void unregister() {
		this.line.forEach(component -> component.unregister());
	}

	private BaseComponent[] getComponentLine(Player player) {
		ComponentBuilder builder = new ComponentBuilder("");
		ArrayList<CanvasLineComponent> builderLine = line.isEmpty() ? this.defaultLine : this.line;
		builderLine.stream().map(CLC -> CLC.getComponent(player)).forEach(TC -> builder.append(TC, FormatRetention.NONE));
		return builder.create();
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public ArrayList<CanvasLineComponent> getLine() {
		return line;
	}

}
