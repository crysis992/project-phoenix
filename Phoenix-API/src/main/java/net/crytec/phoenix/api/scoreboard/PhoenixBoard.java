package net.crytec.phoenix.api.scoreboard;

import org.bukkit.entity.Player;

public interface PhoenixBoard {

    /**
     * Activate the scoreboard.
     */
	public void activate();

    /**
     * Deactivate the scoreboard.
     */
    public void deactivate();

    /**
     * Determine if the scoreboard has been already activated.
     *
     * @return activated
     */
    public boolean isActivated();

    /**
     * Returns the handler for this scoreboard.
     *
     * @return handler
     */
    public ScoreboardHandler getHandler();

    /**
     * Set the handler for this scoreboard.
     *
     * @param handler handler
     */
    public PhoenixBoard setHandler(ScoreboardHandler handler);
    
    public void updateLine(int line, String text);
    
    public void showLine(int line);
    
    public void hideLine(int line);
    
    public void setTitle(String title);
    
    public void update();
    
    /**
     * Returns the holder of this scoreboard.
     *
     * @return holder
     */
    public Player getHolder();
	
}
