package net.crytec.phoenix.api.events.infobars;

import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.bukkit.entity.Entity;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.google.common.collect.Lists;

import net.crytec.phoenix.api.holograms.infobars.InfoLineSpacing;

public class InfoBarCreateEvent extends Event implements Cancellable{

	private static final HandlerList handlers = new HandlerList();

	public static HandlerList getHandlerList() {
		return handlers;
	}
	
	public InfoBarCreateEvent(Entity entity) {
		this.entity = entity;
		this.lines = Lists.newArrayList();
	}
	
	private final Entity entity;
	private final List<Pair<String, InfoLineSpacing>> lines;
	
	private boolean cancelled = false;
	
	public HandlerList getHandlers() {
		return handlers;
	}

	public Entity getEntity() {
		return entity;
	}

	public List<Pair<String, InfoLineSpacing>> getLines() {
		return lines;
	}

	@Override
	public boolean isCancelled() {
		return this.cancelled;
	}

	@Override
	public void setCancelled(boolean cancel) {
		this.cancelled = cancel;
	}

	
}
