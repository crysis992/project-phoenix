package net.crytec.phoenix.api.events.player;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.comphenix.protocol.wrappers.WrappedChatComponent;

import net.crytec.phoenix.api.packets.WrapperPlayServerScoreboardTeam;

public class PlayerReceiveTeamEvent extends Event {

	public PlayerReceiveTeamEvent(Player receiver, WrappedChatComponent prefix, WrappedChatComponent suffix, ChatColor color, WrapperPlayServerScoreboardTeam wrapper) {
		this.prefix = prefix;
		this.suffix = suffix;
		this.color = color;
		this.receiver = receiver;
		this.wrapper = wrapper;
	}

	private final Player receiver;
	private WrappedChatComponent prefix;
	private WrappedChatComponent suffix;
	private ChatColor color;
	private WrapperPlayServerScoreboardTeam wrapper;

	private static final HandlerList handlers = new HandlerList();

	// public Optional<Player> getTarget() {
	// if (wrapper.getPlayers().isEmpty()) {
	// Optional<Team> t =
	// BoardManager.get().getUsers().values().stream().filter(entry ->
	// entry.getName().equals(this.wrapper.getName())).findFirst();
	// if (t.isPresent()) {
	// Team team = t.get();
	//
	// if (team.getEntries().isEmpty()) return Optional.empty();
	//
	// Player target =
	// Bukkit.getPlayerExact(team.getEntries().iterator().next());
	// return (target == null) ? Optional.empty() : Optional.of(target);
	// } else {
	// return Optional.empty();
	// }
	// }
	//
	// Player target = Bukkit.getPlayerExact(wrapper.getPlayers().get(0));
	// return (target == null) ? Optional.empty() : Optional.of(target);
	// }

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	public WrappedChatComponent getPrefix() {
		return prefix;
	}

	public void setPrefix(WrappedChatComponent prefix) {
		this.prefix = prefix;
	}

	public WrappedChatComponent getSuffix() {
		return suffix;
	}

	public void setSuffix(WrappedChatComponent suffix) {
		this.suffix = suffix;
	}

	public ChatColor getColor() {
		return color;
	}

	public void setColor(ChatColor color) {
		this.color = color;
	}

	public Player getReceiver() {
		return receiver;
	}

	public WrapperPlayServerScoreboardTeam getWrapper() {
		return wrapper;
	}

}
