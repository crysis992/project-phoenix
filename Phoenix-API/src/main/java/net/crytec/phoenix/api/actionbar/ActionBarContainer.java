package net.crytec.phoenix.api.actionbar;

import java.util.PriorityQueue;

import net.crytec.phoenix.api.actionbar.ActionBarSection.StringArrangement;
/**
 * Used to create splittable ActionBars
 * @author Gestankbratwurst
 * @author crysis992
 *
 */
public interface ActionBarContainer {
	
	public void insert(int index, ActionBarSection section);
	
	/**
	 * Creates a new {@link ActionBarSection}
	 * @param length the length of this section
	 * @param arrangement the {@link StringArrangement} of this section
	 * @param priority the show priority
	 * @return a new ActionBarSection
	 */
	public ActionBarSection createSection(int length, StringArrangement arrangement, int priority);
	
	/**
	 * Removes all sections currently loaded in this container.
	 */
	public void cleanUp();
	
	/**
	 * Gets a queue of all sections currently located at a
	 * place in this container.
	 * @param index the sections index
	 * @return 
	 * a {@link PriorityQueue} of {@link ActionBarSection}
	 * for this slot
	 * null if out of bounds.
	 */
	public PriorityQueue<ActionBarSection> getSectionHeap(int index);
	
	/**
	 * Gets a view only String of the complete Actionbar to display.
	 * @return a String representation of this Actionbar
	 */
	public String getFullHighestPriorityBar();
	
	public void removeSection(int index, ActionBarSection section);
}
