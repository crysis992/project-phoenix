package net.crytec.phoenix.api.utils;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import org.apache.commons.lang3.time.DurationFormatUtils;

public class UtilTime {
	
	public static String getElapsedTime(long timestamp) {
		return getElapsedTime(millisToLocalDate(timestamp));
	}
	
	
	public static int getElapsedTimeInSeconds(long start) {
		return (int) Duration.between(millisToInstant(start), Instant.now()).toMillis() / 1000;
	}
	
	//TODO: Translatable
	public static String getElapsedTime(LocalDateTime startdate) {
		
		Duration d = Duration.between(startdate, localDateToInstant(LocalDateTime.now()));
		
		StringBuilder builder = new StringBuilder();
		if (d.toDays() > 0) builder.append(d.toDays() + ((d.toDays() > 1) ? " Tage " : " Tag ") );
		if (d.toHours() % 24 > 0) builder.append(d.toHours() % 24 + ((d.toHours() % 24 > 1) ? " Stunden " : " Stunde ") );
		if (d.toMinutes() % 60 > 0) builder.append(d.toMinutes() % 60 + ((d.toMinutes() % 60 > 1) ? " Minuten " : " Minute ") );
		if (d.getSeconds() % 60 > 0) builder.append(d.getSeconds() % 60 + ((d.getSeconds() % 60 > 1) ? " Sekunden" : " Sekunde") );
		return builder.toString();
	}
	
	public static String between(LocalDateTime startdate, LocalDateTime endDate) {
		
		Duration d = Duration.between(startdate, endDate);
		
		StringBuilder builder = new StringBuilder();
		if (d.toDays() > 0) builder.append(d.toDays() + ((d.toDays() > 1) ? " Tage " : " Tag ") );
		if (d.toHours() % 24 > 0) builder.append(d.toHours() % 24 + ((d.toHours() % 24 > 1) ? " Stunden " : " Stunde ") );
		if (d.toMinutes() % 60 > 0) builder.append(d.toMinutes() % 60 + ((d.toMinutes() % 60 > 1) ? " Minuten " : " Minute ") );
		if (d.getSeconds() % 60 > 0) builder.append(d.getSeconds() % 60 + ((d.getSeconds() % 60 > 1) ? " Sekunden" : " Sekunde") );
		return builder.toString();
	}
	
	public static String getTimeUntil(long timestamp, String expired) {
		return getTimeUntil(millisToLocalDate(timestamp), expired);
	}
	
	public static String getTimeUntil(long timestamp) {
		return getTimeUntil(timestamp, null);
	}
	
	public static String getTimeUntil(LocalDateTime endDate) {
		return getTimeUntil(endDate, null);
	}
	
	//TODO: Translatable
	public static String getTimeUntil(LocalDateTime endDate, String expired) {
		if (isElapsed(endDate)) return expired != null ? expired : "Expired";
		Duration d = Duration.between(LocalDateTime.now(), endDate);

		StringBuilder builder = new StringBuilder();
		if (d.toDays() > 0) builder.append(d.toDays() + ((d.toDays() > 1) ? " Tage " : " Tag ") );
		if (d.toHours() % 24 > 0) builder.append(d.toHours() % 24 + ((d.toHours() % 24 > 1) ? " Stunden " : " Stunde ") );
		if (d.toMinutes() % 60 > 0) builder.append(d.toMinutes() % 60 + ((d.toMinutes() % 60 > 1) ? " Minuten " : " Minute ") );
		if (d.getSeconds() % 60 > 0) builder.append(d.getSeconds() % 60 + ((d.getSeconds() % 60 > 1) ? " Sekunden" : " Sekunde") );
		return builder.toString();
	}
	
	public static boolean isElapsed(LocalDateTime endDate) {
		return LocalDateTime.now().isAfter(endDate);
	}
	
	public static boolean isElapsed(long timestamp) {
		return isElapsed(millisToLocalDate(timestamp));
	}
	
	public static LocalDateTime millisToLocalDate(long timestamp) {
		return Instant.ofEpochMilli(timestamp).atZone(ZoneId.systemDefault()).toLocalDateTime();
	}
	
	public static long LocalDateToMillis(LocalDateTime timestamp) {
		return timestamp.toInstant(OffsetDateTime.now().getOffset()).toEpochMilli();
	}
	
	public static Instant millisToInstant(long timestamp) {
		return Instant.ofEpochMilli(timestamp);
	}
	
	public static Instant localDateToInstant(LocalDateTime timestamp) {
		return timestamp.toInstant(OffsetDateTime.now().getOffset());
	}
	
	public static LocalDateTime instantToLocalDate(Instant instant) {
		return instant.atZone(ZoneId.systemDefault()).toLocalDateTime();
	}
	
	public static String now() {
		return DurationFormatUtils.formatDuration(LocalDateToMillis(LocalDateTime.now()), "HH:mm:ss", false);
	}
	
	public static String when(LocalDateTime destinationDate) {
		return destinationDate.format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss"));
	}
	
	public static String when(long destinationTime) {
		return  millisToLocalDate(destinationTime).format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss"));
	}
	
	public static String date() {
		return LocalDate.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
	}
}