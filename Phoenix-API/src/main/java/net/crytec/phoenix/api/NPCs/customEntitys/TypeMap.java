package net.crytec.phoenix.api.NPCs.customEntitys;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.google.common.collect.Maps;

public class TypeMap<T> implements Iterable<Entry<String, T>>{
	
	protected TypeMap() {
		this.internalMapping = Maps.newHashMap();
	}
	
	private final Map<String, T> internalMapping;
	
	public T get(String key) {
		return this.internalMapping.get(key);
	}
	
	public boolean containsKey(String key) {
		return this.internalMapping.containsKey(key);
	}
	
	public void put(String key, T object) {
		this.internalMapping.put(key, object);
	}

	@Override
	public Iterator<Entry<String, T>> iterator() {
		return this.internalMapping.entrySet().iterator();
	}
	
}
