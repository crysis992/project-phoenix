package net.crytec.phoenix.api.recipes.types;

import org.bukkit.inventory.ItemStack;

public interface CustomFurnaceRecipe extends PhoenixRecipe{
	
	public void setInput(ItemStack input, boolean exact);
	public void setCookingTime(int time);
	public void setExp(float exp);
	
}