package net.crytec.phoenix.api.recipes.types;

import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;

public interface PhoenixRecipe {

	public NamespacedKey getKey();
	
	public RecipeType getRecipeType();
	
	public ItemStack getResult();
	
	public void setResult(ItemStack result);
	
	public boolean isNBTShape();
	
	public Recipe getRecipe();
	
	public void setGroup(String group);
	
	public String getGroup();
	
}
