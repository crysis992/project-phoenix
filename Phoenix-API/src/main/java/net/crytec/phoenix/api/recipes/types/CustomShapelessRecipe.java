package net.crytec.phoenix.api.recipes.types;

import java.util.List;

import org.bukkit.inventory.RecipeChoice;

public interface CustomShapelessRecipe extends PhoenixRecipe{
	
	public List<RecipeChoice> getChoices();
	public void addChoice(RecipeChoice choice);
	
}
