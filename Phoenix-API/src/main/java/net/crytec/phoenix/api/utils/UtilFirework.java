package net.crytec.phoenix.api.utils;

import org.apache.commons.lang.math.RandomUtils;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.util.Vector;


public class UtilFirework {

	public static void playFirework(Location loc, FireworkEffect fe) {
		Firework firework = (Firework) loc.getWorld().spawn(loc, Firework.class);

		FireworkMeta data = (FireworkMeta) firework.getFireworkMeta();
		data.clearEffects();
		data.setPower(1);
		data.addEffect(fe);
		firework.setFireworkMeta(data);
		firework.setTicksLived(1);
	}

	public static Firework launchFirework(Location loc, FireworkEffect fe, Vector dir, int power) {
		Firework fw = (Firework) loc.getWorld().spawn(loc, Firework.class);

		FireworkMeta data = (FireworkMeta) fw.getFireworkMeta();
		data.clearEffects();
		data.setPower(power);
		data.addEffect(fe);
		fw.setFireworkMeta(data);

		if (dir != null)
			fw.setVelocity(dir);

		return fw;
	}

	public static void LaunchRandomFirework(Location location) {
		FireworkEffect.Builder builder = FireworkEffect.builder();

		if (RandomUtils.nextInt(3) == 0) {
			builder.withTrail();
		} else if (RandomUtils.nextInt(2) == 0) {
			builder.withFlicker();
		}
		builder.with(FireworkEffect.Type.values()[RandomUtils.nextInt(FireworkEffect.Type.values().length)]);

		int colorCount = 17;

		builder.withColor(Color.fromRGB(RandomUtils.nextInt(255), RandomUtils.nextInt(255), RandomUtils.nextInt(255)));

		while (RandomUtils.nextInt(colorCount) != 0) {
			builder.withColor(Color.fromRGB(RandomUtils.nextInt(255), RandomUtils.nextInt(255), RandomUtils.nextInt(255)));
			colorCount--;
		}

		Firework firework = (Firework) location.getWorld().spawn(location, Firework.class);
		FireworkMeta data = firework.getFireworkMeta();
		data.addEffect(builder.build());
		data.setPower(RandomUtils.nextInt(3));
		firework.setFireworkMeta(data);
	}

	public void detonateFirework(Firework firework) {
		firework.detonate();
		firework.remove();
	}

	public static Firework launchFirework(Location loc, Type type, Color color, boolean flicker, boolean trail, Vector dir, int power) {
		return launchFirework(loc, FireworkEffect.builder().flicker(flicker).withColor(color).with(type).trail(trail).build(), dir, power);
	}

	public static void playFirework(Location loc, Type type, Color color, boolean flicker, boolean trail) {
		playFirework(loc, FireworkEffect.builder().flicker(flicker).withColor(color).with(type).trail(trail).build());
	}
}
