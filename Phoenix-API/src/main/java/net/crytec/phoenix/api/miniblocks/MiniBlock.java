package net.crytec.phoenix.api.miniblocks;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public interface MiniBlock {

	public Location getLocation();
	public void showTo(Player player);
	public void hideFrom(Player player);
	
}
