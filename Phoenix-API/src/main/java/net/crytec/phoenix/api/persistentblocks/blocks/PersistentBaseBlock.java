package net.crytec.phoenix.api.persistentblocks.blocks;

import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.block.BlockBreakEvent;

public interface PersistentBaseBlock {
	
	public String getBlockType();
	
	public String getBlockData();
	
	public Location getLocation();
	
	public void delete();
	
	public void onRemove();
	public void onBreak(BlockBreakEvent event);
	public void loadData(ConfigurationSection config);
	public void saveData(ConfigurationSection config);
	
	public void postInit();

}
