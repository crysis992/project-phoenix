package net.crytec.phoenix.api.events.player;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;

import org.bukkit.event.player.PlayerEvent;

public class PlayerUnloadsEntityEvent extends PlayerEvent {

	private final int[] entityIDs;

	public PlayerUnloadsEntityEvent(Player who, int[] entityIDs) {
		super(who);
		this.entityIDs = entityIDs;
	}

	public int[] getEntityIDs() {
		return entityIDs;
	}

	private static final HandlerList handlers = new HandlerList();

	public static HandlerList getHandlerList() {
		return handlers;
	}

	public HandlerList getHandlers() {
		return handlers;
	}

}
