package net.crytec.phoenix.api.inventory.content;

import net.crytec.phoenix.api.inventory.ClickableItem;

public interface Pagination {

    public ClickableItem[] getPageItems();

    int getPage();
    public Pagination page(int page);

    boolean isFirst();
    boolean isLast();

    public Pagination first();
    public Pagination previous();
    public Pagination next();
    public Pagination last();

    public Pagination addToIterator(SlotIterator iterator);

    public Pagination setItems(ClickableItem... items);
    public Pagination setItemsPerPage(int itemsPerPage);
}