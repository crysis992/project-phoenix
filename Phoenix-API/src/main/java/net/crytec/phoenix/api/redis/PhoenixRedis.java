package net.crytec.phoenix.api.redis;

public interface PhoenixRedis {
	
	public void registerProvider(RedisPacket provider);
	
	 public void requestServername();
	 
	 public void setServer(String id);
	 
	 public void sendOutgoingMessage(RedisPacket provider, String serverID);
	 
	 public void sendOutgoingMessage(RedisPacket provider);

}
