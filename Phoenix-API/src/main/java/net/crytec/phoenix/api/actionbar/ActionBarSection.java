package net.crytec.phoenix.api.actionbar;

import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

public class ActionBarSection implements Comparable<ActionBarSection> {

	public static final char COLOR_CHAR = '\u00A7';
	private static final Pattern STRIP_COLOR_PATTERN = Pattern.compile("(?i)" + String.valueOf(COLOR_CHAR) + "[0-9A-FK-OR]");

	public ActionBarSection(int length, UUID id) {
		this(length, StringArrangement.LEFT, 0, id);
	}

	public ActionBarSection(int length, StringArrangement arrangement, int priority, UUID id) {
		this.id = id;
		this.length = length;
		this.content = "";
		this.arrangement = arrangement;
		this.priority = priority;
	}

	private final UUID id;
	private int priority;
	private final int length;
	private String content;
	private StringArrangement arrangement;

	public int getContentLenght() {
		return this.getColorlessLength(this.content);
	}

	public int getLengthLeft() {
		return this.length - this.getContentLenght();
	}

	private int getColorlessLength(String string) {
		Matcher matcher = STRIP_COLOR_PATTERN.matcher(string);
		int colors = Stream.iterate(0, i -> i + 1).filter(i -> !matcher.find()).findFirst().get() * 2;
		return this.content.length() - colors;
	}

	public void concat(String line) {

		int lineLength = this.getColorlessLength(line);
		int left = this.getLengthLeft();

		if (left < lineLength)
			return;

		this.content += line;
	}

	public void clear() {
		this.content = "";
	}

	public void setAt(String line, int pos) {
		this.set(this.content.substring(0, pos) + line + this.content.substring(pos + line.length()));
	}

	public void set(String line) {
		this.clear();
		this.concat(line);
	}

	public String getContent() {
		return this.getContent(this.arrangement);
	}

	public String getContent(StringArrangement pos) {
		return pos.getAccording(this.content, this.length);
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public StringArrangement getArrangement() {
		return arrangement;
	}

	public void setArrangement(StringArrangement arrangement) {
		this.arrangement = arrangement;
	}

	public UUID getId() {
		return id;
	}

	public enum StringArrangement {
		LEFT, RIGHT, CENTER, NONE;

		public String getAccording(String line, int desired) {
			switch (this) {
				case CENTER :
					return StringUtils.center(line, desired);
				case LEFT :
					return StringUtils.leftPad(line, desired);
				case NONE :
					return line;
				case RIGHT :
					return StringUtils.rightPad(line, desired);
				default :
					return line;
			}
		}

	}

	@Override
	public int compareTo(ActionBarSection other) {
		return this.priority - other.priority;
	}

}
