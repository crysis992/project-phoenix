package net.crytec.phoenix.api.chat.program;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.entity.Player;

import com.google.common.collect.Lists;

import net.md_5.bungee.api.ChatColor;

public class ChatCanvas {

	public ChatCanvas(CanvasHeader header, CanvasFooter footer) {
		this(header, footer, 20);
	}

	public ChatCanvas(CanvasHeader header, CanvasFooter footer, int heigth) {
		this.lines = Lists.newArrayList();
		for (int index = 0; index < heigth; index++) {
			this.lines.add(new CanvasLine(index));
		}
		this.lines.set(0, header);
		this.lines.set((heigth - 1), footer);
		this.heigth = heigth;
	}

	private final int heigth;

	public ChatCanvas(String title) {
		this(new CanvasHeader(title), new CanvasFooter());
	}

	private final List<CanvasLine> lines;

	public void setLine(int index, CanvasLine line) {
		this.lines.set(index, line);
	}

	public CanvasLine getCanvasLine(int index) {
		return this.lines.get(index);
	}

	public void sendTo(Player player) {
		for (int index = 0; index < this.heigth; index++) {
			this.lines.get(index).sendTo(player);
		}
	}

	public void unregister() {
		Iterator<CanvasLine> line = this.lines.iterator();
		while (line.hasNext()) {
			line.next().unregister();
		}
	}

	public static class CanvasHeader extends CanvasLine {

		private String title;

		public CanvasHeader(String title, String splitter) {
			super(0);
			this.title = title;
			String[] head = StringUtils.center(title, 50, splitter).split(title);
			super.getLine().add(new CanvasLineComponent(ChatColor.WHITE + head[0]));
			super.getLine().add(new CanvasLineComponent(title));
			super.getLine().add(new CanvasLineComponent(ChatColor.WHITE + head[1]));
		}

		public CanvasHeader(String title) {
			this(title, "-");
		}

		public CanvasHeader(CanvasLineComponent component) {
			super(0);
			super.getLine().add(component);
		}

		public String getTitle() {
			return title;
		}

		public void setTitle(String title) {
			this.title = title;
		}

	}

	public static class CanvasFooter extends CanvasLine {

		public CanvasFooter(String splitter) {
			super(19);
			super.getLine().add(new CanvasLineComponent(StringUtils.repeat(splitter, 50)));
		}

		public CanvasFooter() {
			this("-");
		}

	}

}
