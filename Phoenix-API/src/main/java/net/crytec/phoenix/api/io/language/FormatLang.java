package net.crytec.phoenix.api.io.language;

public enum FormatLang {

	YES("gui.yes"),
	NO("gui.no"),
	ALL("gui.all"),
	OK("gui.ok"),
	UP("gui.up"),
	DOWN("gui.down"),
	
	BACK("gui.back"),
	DONE("gui.done"),
	CANCEL("gui.cancel"),
	
	ON("options.on"),
	OFF("options.off"),
	
	MIN("options.min"),
	MAX("options.max"),
	
	
	DESTROY_ITEM("inventory.binSlot"),
	
	KEY_SPACE("key.keyboard.space"),
	KEY_MOUSE_LEFT("key.mouse.left"),
	KEY_MOUSE_MIDDLE("key.mouse.middle"),
	KEY_MOUSE_RIGHT("key.mouse.right"),
	
	SPRINT("key.sprint"),
	SNEAK("key.sneak");
	
	private String localePath;
	
	private FormatLang(String path) {
		this.localePath = path;
	}
	
	public String getLocalePath() {
		return this.localePath;
	}
}
