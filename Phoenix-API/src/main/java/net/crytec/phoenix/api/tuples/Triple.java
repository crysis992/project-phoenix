package net.crytec.phoenix.api.tuples;

public class Triple <K, E, V>{
	
	public static <K, E, V> Triple<K, E, V> of(K left, E middle, V right) {
		return new Triple<K, E, V>(left, middle, right);
	}
	
	private Triple(K left, E middle, V right) {
		this.left = left;
		this.middle = middle;
		this.right = right;
	}
	
	private K left;
	private E middle;
	private V right;
	
	public K getLeft() {
		return left;
	}
	
	public E getMiddle() {
		return this.middle;
	}
	
	public V getRight() {
		return this.right;
	}
	
	public void setLeft(K left) {
		this.left = left;
	}
	
	public void setMiddle(E middle) {
		this.middle = middle;
	}
	
	public void setRight(V right) {
		this.right = right;
	}
	
}
