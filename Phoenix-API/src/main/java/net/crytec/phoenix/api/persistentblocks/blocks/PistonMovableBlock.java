package net.crytec.phoenix.api.persistentblocks.blocks;

import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;

public interface PistonMovableBlock extends PersistentBaseBlock  {
	
	public abstract void onPistonMove(BlockPistonExtendEvent event);
	public abstract void onPistonMove(BlockPistonRetractEvent event);
	
}
