package net.crytec.phoenix.api.holograms;

public enum HologramLineType {
	
	ITEM_LINE(),
	TEXT_LINE();
	
}
