package net.crytec.phoenix.api.holograms;

import java.util.Iterator;
import java.util.Set;

import org.bukkit.entity.Player;

import com.google.common.collect.Sets;

public class HologramView implements Iterable<PhoenixHologram>{
    
    public HologramView(Player player) {
        this.player = player;
        this.viewingHolograms = Sets.newHashSet();
    }
    
    private final Player player;
    private final Set<PhoenixHologram> viewingHolograms;
    
    public boolean isViewing(PhoenixHologram hologram) {
        return this.viewingHolograms.contains(hologram);
    }
    
    public void addHologram(PhoenixHologram hologram) {
        this.viewingHolograms.add(hologram);
        hologram.showTo(player);
    }
    
    public void removeHologram(PhoenixHologram hologram) {
        this.viewingHolograms.remove(hologram);
        hologram.hideFrom(player);
    }

    @Override
    public Iterator<PhoenixHologram> iterator() {
        return this.viewingHolograms.iterator();
    }
    
}