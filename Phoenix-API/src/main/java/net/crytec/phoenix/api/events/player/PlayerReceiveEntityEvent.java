package net.crytec.phoenix.api.events.player;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class PlayerReceiveEntityEvent extends PlayerEvent {

	private final int entityID;

	public PlayerReceiveEntityEvent(Player who, int entityID) {
		super(who);
		this.entityID = entityID;
	}

	public int getEntityID() {
		return entityID;
	}

	private static final HandlerList handlers = new HandlerList();

	public static HandlerList getHandlerList() {
		return handlers;
	}

	public HandlerList getHandlers() {
		return handlers;
	}
}