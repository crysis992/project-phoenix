package net.crytec.phoenix.api.utils;

import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.ChunkSnapshot;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import net.crytec.phoenix.api.events.player.PlayerReceiveChunkEvent;
import net.crytec.phoenix.api.events.player.PlayerUnloadsChunkEvent;


public class UtilChunk implements Listener {
	
	private static UtilChunk instance;
	private static boolean initialized = false;
	
	public static void init(JavaPlugin host) {
		Preconditions.checkArgument(!initialized, "UtilChunk is already initialized!");
		instance = new UtilChunk();
		Bukkit.getPluginManager().registerEvents(instance, host);
		Bukkit.getOnlinePlayers().forEach(player -> chunkViews.put(player, Sets.newHashSet())); // Handle reloads
		initialized = true;
	}
	
	private UtilChunk() { }
	
	private static final Map<Player, Set<Long>> chunkViews = Maps.newHashMap();
	
	@EventHandler
	public void onChunkReceive(PlayerReceiveChunkEvent event) {
		UtilChunk.chunkViews.get(event.getPlayer()).add(event.getChunkKey());
	}
	
	@EventHandler
	public void onChunkReceive(PlayerUnloadsChunkEvent event) {
		UtilChunk.chunkViews.get(event.getPlayer()).remove(event.getChunkKey());
	}
	
	@EventHandler(priority = EventPriority.LOW)
	public void onJoin(PlayerJoinEvent event) {
		UtilChunk.chunkViews.put(event.getPlayer(), Sets.newHashSet());
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		UtilChunk.chunkViews.remove(event.getPlayer());
	}
	
	public static int[] getChunkCoords(long chunkKey) {
		int x = ((int)chunkKey) >> 32;
		int z = (int)(chunkKey >> 32) >> 32;
		return new int[] {x, z};
	}
	
	public static Set<Long> getChunkViews(Player player) {
		return chunkViews.get(player);
	}
	
	public static boolean isChunkInView(Player player, Chunk chunk) {
		return chunkViews.get(player).contains(getChunkKey(chunk));
	}
	
	public static long getChunkKey(int x, int z) {
		return (long) x & 0xffffffffL | ((long) z & 0xffffffffL) << 32;
	}

	public static long getChunkKey(Chunk chunk) {
		return (long) chunk.getX() & 0xffffffffL | ((long) chunk.getZ() & 0xffffffffL) << 32;
	}

	public static Chunk keyToChunk(World world, long chunkID) {
		Validate.notNull(world, "World cannot be null");
		return world.getChunkAt((int) chunkID, (int) (chunkID >> 32));
	}

	public static boolean isChunkLoaded(Location loc) {
		int chunkX = loc.getBlockX() >> 4;
		int chunkZ = loc.getBlockZ() >> 4;
		return loc.getWorld().isChunkLoaded(chunkX, chunkZ);
	}

	public static long getChunkKey(Location loc) {
		return getChunkKey(loc.getBlockX() >> 4, loc.getBlockZ() >> 4);
	}
	
	public static long getChunkKey(ChunkSnapshot chunk) {
		return (long) chunk.getX() & 0xffffffffL | ((long) chunk.getZ() & 0xffffffffL) << 32;
	}
	
	public static Set<Long> getChunkViewOf(Player player){
		return UtilChunk.chunkViews.get(player);
	}
	
}
