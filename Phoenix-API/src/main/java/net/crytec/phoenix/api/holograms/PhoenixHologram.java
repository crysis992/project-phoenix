package net.crytec.phoenix.api.holograms;

import java.util.ArrayList;
import java.util.Set;
import java.util.function.Predicate;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

public abstract class PhoenixHologram {
	
	private static final double MAX_MOVE_DIST = 8 * 8;
	
	public PhoenixHologram(Location baseLocation, Predicate<Player> playerFilter, PhoenixHologramManager manager) {
		this.lines = Lists.newArrayList();
		this.manager = manager;
		this.baseLocation = baseLocation;
		this.playerFilter = playerFilter;
		this.clickable = false;
	}
	
	protected final PhoenixHologramManager manager;
	private final Location baseLocation;
	protected final ArrayList<PhoenixHologramLine<?>> lines;
	private Predicate<Player> playerFilter;
	protected boolean clickable;
	
	protected void registerClickableEntities() {
		this.manager.setClickableIdentifier(this.getClickableEntityIds(), this);
	}
	
	public void delete() {
		this.manager.removeHologram(this);
	}
	
	public Set<Player> getViewers() {
		return this.manager.getViewing(this);
	}
	
	public void move(Vector direction) {
		Preconditions.checkArgument(direction.lengthSquared() < MAX_MOVE_DIST, "Move distance can be 8 at most.");
		this.moveHologram(direction);
	}
	
	public int getSize() {
		return this.lines.size();
	}
	
	public void setPlayerFilter(Predicate<Player> filter) {
		this.playerFilter = filter;
	}
	
	public boolean isViableViewer(Player player) {
		return this.playerFilter.test(player);
	}
	
	protected void appendLine(PhoenixHologramLine<?> line) {
		this.lines.add(line);
		for(Player viewer : this.getViewers()) {
			line.showTo(viewer);
		}
	}
	
	public PhoenixHologramLine<?> getHologramLine(int index){
		return lines.get(index);
	}
	
	public void showTo(Player player) {
		for(PhoenixHologramLine<?> line : lines) {
			line.showTo(player);
			this.showClickableEntities(player);
		}
	}
	
	public void hideFrom(Player player) {
		for(PhoenixHologramLine<?> line : lines) {
			line.hideFrom(player);
			this.hideClickableEntities(player);
		}
	}
	
	public Location getBaseLocation() {
		return this.baseLocation;
	}
	
	public abstract void setClickable();
	protected abstract void showClickableEntities(Player player);
	protected abstract void hideClickableEntities(Player player);
	protected abstract Set<Integer> getClickableEntityIds();
	protected abstract void moveHologram(Vector direction);
	public abstract void appendTextLine(String text);
	public abstract void appendItemLine(ItemStack item);
	
}