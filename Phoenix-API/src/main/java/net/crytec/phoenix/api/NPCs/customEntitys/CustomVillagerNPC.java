package net.crytec.phoenix.api.NPCs.customEntitys;

import org.bukkit.entity.Villager.Profession;

public interface CustomVillagerNPC extends CustomEntity {
	
	public void setVillagerProfession(Profession profession);
	public void setVillagerExperience(int value);
}