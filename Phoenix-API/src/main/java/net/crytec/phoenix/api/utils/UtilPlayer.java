package net.crytec.phoenix.api.utils;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;

import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;
import net.crytec.phoenix.api.events.player.PlayerReceiveEntityEvent;
import net.crytec.phoenix.api.events.player.PlayerUnloadsEntityEvent;

public class UtilPlayer implements Listener {
	
	private static UtilPlayer instance;
	private static boolean initialized = false;
	private static final Vector UP_VEC = new Vector(0, 1, 0);
	private static final Vector DOWN_VEC = new Vector(0, -1, 0);
	private final Map<Player, IntSet> playerViews;
	
	private UtilPlayer() {
		this.playerViews = Maps.newHashMap();
		Bukkit.getOnlinePlayers().forEach( player -> playerViews.put(player, new IntOpenHashSet())); //  fix for reloads
	}
	
	public static void init(JavaPlugin host) {
		Preconditions.checkArgument(!initialized, "UtilPlayer is already initialized!");
		instance = new UtilPlayer();
		Bukkit.getPluginManager().registerEvents(instance, host);
	}
	
	/**
	 * Add Dashes to a UUID without dashes.
	 * 
	 * @param idNoDashes
	 * @return -
	 */
	public static String addUUIDDashes(String idNoDashes) {
		StringBuffer idBuff = new StringBuffer(idNoDashes);
		idBuff.insert(20, '-');
		idBuff.insert(16, '-');
		idBuff.insert(12, '-');
		idBuff.insert(8, '-');
		return idBuff.toString();
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		this.playerViews.put(event.getPlayer(), new IntOpenHashSet());
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		this.playerViews.remove(event.getPlayer());
	}
	
	@EventHandler
	public void onEntityShowing(PlayerReceiveEntityEvent event) {
		this.playerViews.get(event.getPlayer()).add(event.getEntityID());
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onEntityHiding(PlayerUnloadsEntityEvent event) {
		IntSet ints = this.playerViews.get(event.getPlayer());
		for(int id : event.getEntityIDs()) {
			ints.remove(id);
		}
	}
	
	public static IntSet getEntityViewOf(Player player) {
		return instance.playerViews.get(player);
	}
	
	public static void playSound(Player player, Sound sound, SoundCategory category) {
		player.playSound(player.getLocation(), sound, category, 1, 1);
	}
	
	public static BlockFace getExactFacing(Player player) {
		Vector direction = player.getEyeLocation().getDirection();
		if(direction.angle(UP_VEC) <= (Math.PI / 4)) {
			return BlockFace.UP;
		}else if(direction.angle(DOWN_VEC) <= (Math.PI / 4)) {
			return BlockFace.DOWN;
		}
		return player.getFacing();
	}
	
	public static void playSound(Player player, Sound sound) {
		playSound(player, sound, SoundCategory.MASTER, 1, 1);
	}

	public static void playSound(Player player, Sound sound, float pitch) {
		playSound(player, sound, SoundCategory.MASTER, 1, pitch);
	}

	public static void playSound(Player player, Sound sound, float volume, float pitch) {
		player.playSound(player.getLocation(), sound, SoundCategory.MASTER, volume, pitch);
	}

	public static void playSound(Player player, Sound sound, SoundCategory category, float volume, float pitch) {
		player.playSound(player.getLocation(), sound, category, volume, pitch);
	}

	public static LinkedList<Player> getNearby(Location loc, double maxDist) {
		LinkedList<Player> nearbyMap = new LinkedList<Player>();

		for (Player cur : loc.getWorld().getPlayers()) {
			if (cur.getGameMode() != GameMode.CREATIVE) {

				if (!cur.isDead()) {

					double dist = loc.toVector().subtract(cur.getLocation().toVector()).length();

					if (dist <= maxDist) {

						for (int i = 0; i < nearbyMap.size(); i++) {
							if (dist < loc.toVector().subtract(nearbyMap.get(i).getLocation().toVector()).length()) {
								nearbyMap.add(i, cur);
								break;
							}
						}

						if (!nearbyMap.contains(cur))
							nearbyMap.addLast(cur);
					}
				}
			}
		}
		return nearbyMap;
	}

	public static Player getClosest(Location loc, Collection<Player> ignore) {
		Player best = null;
		double bestDist = 0.0D;

		for (Player cur : loc.getWorld().getPlayers()) {
			if (cur.getGameMode() != GameMode.CREATIVE) {

				if (!cur.isDead()) {

					if ((ignore == null) || (!ignore.contains(cur))) {

						double dist = UtilMath.offset(cur.getLocation(), loc);

						if ((best == null) || (dist < bestDist)) {
							best = cur;
							bestDist = dist;
						}
					}
				}
			}
		}
		return best;
	}

	public static Player getClosest(Location loc, Entity ignore) {
		Player best = null;
		double bestDist = 0.0D;

		for (Player cur : loc.getWorld().getPlayers()) {
			if (cur.getGameMode() != GameMode.CREATIVE && cur.getGameMode() != GameMode.SPECTATOR) {

				if (!cur.isDead()) {

					if ((ignore == null) || (!ignore.equals(cur))) {

						double dist = UtilMath.offset(cur.getLocation(), loc);

						if ((best == null) || (dist < bestDist)) {
							best = cur;
							bestDist = dist;
						}
					}
				}
			}
		}
		return best;
	}

	public static HashMap<Player, Double> getInRadius(Location loc, double dR) {
		HashMap<Player, Double> players = new HashMap<Player, Double>();

		for (Player cur : loc.getWorld().getPlayers()) {
			if (cur.getGameMode() != GameMode.CREATIVE && cur.getGameMode() != GameMode.SPECTATOR) {

				double offset = UtilMath.offset(loc, cur.getLocation());

				if (offset < dR)
					players.put(cur, Double.valueOf(1.0D - offset / dR));
			}
		}
		return players;
	}

	public static void hunger(Player player, int mod) {
		if (player.isDead()) {
			return;
		}
		int hunger = player.getFoodLevel() + mod;

		if (hunger < 0) {
			hunger = 0;
		}
		if (hunger > 20) {
			hunger = 20;
		}
		player.setFoodLevel(hunger);
	}

	public static String safeNameLength(String name) {
		if (name.length() > 16) {
			name = name.substring(0, 16);
		}
		return name;
	}
	
	public static Set<ItemStack> getPlayerEquipment(Player player) {
		PlayerInventory inv = player.getInventory();
		Set<ItemStack> equipment = Stream.of(inv.getArmorContents()).filter(item -> item != null).collect(Collectors.toSet());

		if (inv.getItemInMainHand() != null)
			equipment.add(inv.getItemInMainHand());

		if (inv.getItemInOffHand() != null)
			equipment.add(inv.getItemInOffHand());
		return equipment;
	}
	
	public static void giveItems(Player player, ItemStack item, int amount, boolean dropExcess) {
		
		ItemStack singleItem = item.clone();
		singleItem.setAmount(1);
		
		int stackAmount = item.getAmount();
		int stackSize = singleItem.getMaxStackSize();
		int stacks = (amount * stackAmount) / stackSize;
		int left = amount % stackSize;
		
		if(stacks != 0) {
			singleItem.setAmount(stackSize);
			IntStream.range(0, stacks).forEach(i ->{
				UtilInv.insert(player, singleItem.clone(), dropExcess);
			});
		}
		
		if(left != 0) {
			singleItem.setAmount(left);
			UtilInv.insert(player, singleItem.clone(), dropExcess);
		}
		
	}
	
	public static void message(Entity target, String message) {
		target.sendMessage(message);
	}
}