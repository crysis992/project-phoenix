package net.crytec.phoenix.api.actionbar;

public class ActionSpreadBar {

	public ActionSpreadBar() {
		this("");
	}

	public ActionSpreadBar(String actionBar) {
		this.actionBar = actionBar;
		this.showing = false;
	}

	private boolean showing;
	private String actionBar;

	public boolean toggle() {
		this.showing = !this.showing;
		return this.showing;
	}

	public boolean isShowing() {
		return showing;
	}

	public void setShowing(boolean showing) {
		this.showing = showing;
	}

	public String getActionBar() {
		return actionBar;
	}

	public void setActionBar(String actionBar) {
		this.actionBar = actionBar;
	}

}
