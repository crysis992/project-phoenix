package net.crytec.phoenix.api.scoreboard.common.animate;

public interface AnimatableString {

	String current();

	String next();

	String previous();

}
