package net.crytec.phoenix.api.tuples;

public class Pair <K, V>{
	
	public static <K, V> Pair<K,V> of(K key, V value) {
		return new Pair<K, V>(key, value);
	}
	
	private Pair(K key, V value) {
		this.key = key;
		this.value = value;
	}
	
	private K key;
	private V value;
	
	public K getKey() {
		return this.key;
	}
	
	public V getValue() {
		return this.value;
	}
	
	public void setKey(K key) {
		this.key = key;
	}
	
	public void setValue(V value) {
		this.value = value;
	}
	
}
