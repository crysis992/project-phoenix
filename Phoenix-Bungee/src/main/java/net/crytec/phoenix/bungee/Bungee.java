package net.crytec.phoenix.bungee;

import net.crytec.phoenix.api.redis.PhoenixRedis;
import net.md_5.bungee.api.plugin.Plugin;

public class Bungee extends Plugin {

	private static Bungee instance;
	private PhoenixRedis redis;

	@Override
	public void onLoad() {
		Bungee.instance = this;
	}

	@Override
	public void onEnable() {

		this.redis = new RedisManager(this);

		this.redis.registerProvider(new ServernameAnswerPacket(this.redis));
		this.redis.setServer("BungeeCord");

	}

	@Override
	public void onDisable() {
		if (this.redis != null) {
			((RedisManager) redis).close();
		}
	}

	public static Bungee getInstance() {
		return instance;
	}

	public PhoenixRedis getRedis() {
		return redis;
	}

}
