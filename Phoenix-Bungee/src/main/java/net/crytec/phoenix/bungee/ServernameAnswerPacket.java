package net.crytec.phoenix.bungee;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

import com.google.gson.JsonObject;

import net.crytec.phoenix.api.redis.PhoenixRedis;
import net.crytec.phoenix.api.redis.RedisPacket;
import net.crytec.phoenix.api.redis.RedisServerType;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;

public class ServernameAnswerPacket extends RedisPacket {

	private static PhoenixRedis manager;

	public ServernameAnswerPacket(PhoenixRedis manager) {
		super("servername", RedisServerType.SPIGOT);
		ServernameAnswerPacket.manager = manager;
		this.server = "";
		this.port = 0;
	}

	public ServernameAnswerPacket(String server, int port) {
		super("servername", RedisServerType.SPIGOT);
		this.server = server;
		this.port = port;
	}

	private final String server;
	private final int port;

	@Override
	public void onPacketSent(JsonObject data) {
		data.addProperty("servername", this.server);
		data.addProperty("port", this.port);

	}

	@Override
	public void onPacketReceive(JsonObject data) {
		int port = data.get("port").getAsInt();
		Optional<ServerInfo> server = ProxyServer.getInstance().getServers().values().stream().filter(info -> (info.getAddress().getPort() == port)).findFirst();

		if (server.isPresent()) {
			ServerInfo si = server.get();
			ProxyServer.getInstance().getLogger().info("Server " + si.getName() + " is requesting ID..sending");
			ProxyServer.getInstance().getScheduler().schedule(Bungee.getInstance(), () -> {
				manager.sendOutgoingMessage(new ServernameAnswerPacket(si.getName(), si.getAddress().getPort()));
			}, 2, TimeUnit.SECONDS);
		}

	}
}
