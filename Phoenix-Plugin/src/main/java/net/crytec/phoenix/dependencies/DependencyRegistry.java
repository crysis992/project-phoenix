package net.crytec.phoenix.dependencies;

public class DependencyRegistry {

	private static boolean classExists(String className) {
		try {
			Class.forName(className);
			return true;
		} catch (ClassNotFoundException e) {
			return false;
		}
	}

	public static boolean slf4jPresent() {
		return classExists("org.slf4j.Logger") && classExists("org.slf4j.LoggerFactory");
	}

	public static boolean shouldAutoLoad(Dependency dependency) {
		switch (dependency) {
			// all used within 'isolated' classloaders, and are therefore not
			// relocated.
			case ASM :
			case ASM_COMMONS :
			case JAR_RELOCATOR :
			case H2_DRIVER :
			case SQLITE_DRIVER :
				return false;
			default :
				return true;
		}
	}

}