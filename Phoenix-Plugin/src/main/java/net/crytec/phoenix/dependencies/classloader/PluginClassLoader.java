package net.crytec.phoenix.dependencies.classloader;

import java.nio.file.Path;

/**
 * Represents the plugins classloader
 */
public interface PluginClassLoader {

	void loadJar(Path file);

}