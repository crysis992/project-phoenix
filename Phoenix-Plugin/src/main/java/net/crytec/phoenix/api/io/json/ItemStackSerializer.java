package net.crytec.phoenix.api.io.json;

import java.io.IOException;

import org.bukkit.inventory.ItemStack;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import net.crytec.phoenix.api.PhoenixAPI;

public class ItemStackSerializer extends StdSerializer<ItemStack> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4786252376655086476L;

	public ItemStackSerializer() {
		this(null);
	}

	protected ItemStackSerializer(Class<ItemStack> t) {
		super(t);
	}

	@Override
	public void serialize(ItemStack item, JsonGenerator gen, SerializerProvider serializer) throws IOException {
		gen.writeStartObject();
		String serialized = PhoenixAPI.get().serializeItemStack(item);
		gen.writeStringField("itemdata", serialized);
		gen.writeEndObject();

	}

}
