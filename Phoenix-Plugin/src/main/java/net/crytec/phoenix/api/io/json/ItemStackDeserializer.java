package net.crytec.phoenix.api.io.json;

import java.io.IOException;

import org.bukkit.inventory.ItemStack;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import net.crytec.phoenix.api.PhoenixAPI;

public class ItemStackDeserializer extends StdDeserializer<ItemStack> {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8791905698322202334L;

	public ItemStackDeserializer() {
		this(null);
	}
	

	protected ItemStackDeserializer(Class<?> vc) {
		super(vc);
	}

	@Override
	public ItemStack deserialize(JsonParser parser, DeserializationContext deserializer) throws IOException, JsonProcessingException {

		ObjectCodec codec = parser.getCodec();
		JsonNode node = codec.readTree(parser);
		
		JsonNode itemString = node.get("itemdata");
		ItemStack item = PhoenixAPI.get().deserializeItemStack(itemString.asText());
		
		return item;
	
	}
	
	
	
}
