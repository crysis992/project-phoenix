package net.crytec.phoenix.api.redis;

import java.util.HashMap;
import java.util.UUID;

import org.apache.commons.lang3.Validate;
import org.bukkit.Bukkit;

import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import net.crytec.phoenix.api.PhoenixAPI;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisPubSub;

public class RedisManager implements PhoenixRedis {

	private static final String CHANNEL = "phoenix:update";
	private String server;

	private JedisPool jedisPool;
	private Subscription sub;
	private final Gson gson = new Gson();

	private final PhoenixAPI plugin;
	
	private HashMap<String, RedisPacket> registeredProviders = Maps.newHashMap();

	public RedisManager(PhoenixAPI api) {
		this.registerProvider(new ServernameRequestPacket(this));
		this.plugin = api;
		this.setServer(UUID.randomUUID().toString().replace("-", "").substring(0, 8));
		this.init("localhost", "");
	}

	public void setServer(String id) {
		this.server = id;
	}

	public String getServer() {
		return this.server;
	}

	public void requestServername() {
		this.sendOutgoingMessage(new ServernameRequestPacket());
	}

	public void registerProvider(RedisPacket packet) {
		Validate.isTrue(!this.registeredProviders.containsKey(packet.getID()), "Provider " + packet.getID() + " is already registered!");
		this.registeredProviders.put(packet.getID(), packet);
	}

	private void init(String address, String password) {
		String[] addressSplit = address.split(":");
		String host = addressSplit[0];
		int port = addressSplit.length > 1 ? Integer.parseInt(addressSplit[1]) : 6379;

		if (password.equals("")) {
			this.jedisPool = new JedisPool(new JedisPoolConfig(), host, port);
		} else {
			this.jedisPool = new JedisPool(new JedisPoolConfig(), host, port, 0, password);
		}

		Bukkit.getScheduler().runTaskAsynchronously((this.plugin).getPlugin(), () -> {
			this.sub = new Subscription(this);
			try (Jedis jedis = this.jedisPool.getResource()) {
				jedis.subscribe(this.sub, CHANNEL);
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}

	private void handleIncomingMessage(String msg) {
		JsonObject data = gson.fromJson(msg, JsonElement.class).getAsJsonObject();

		String server = data.get("phserver").getAsString();
		String key = data.get("phkey").getAsString();
		RedisServerType type = RedisServerType.valueOf(data.get("phservertype").getAsString());
		
		if (!this.registeredProviders.containsKey(key)) {
			return;
		}
		
		if (type == RedisServerType.BUNGEE || server.equals(this.server)) return;

		registeredProviders.get(key).onPacketReceive(data);
	}

	public void sendOutgoingMessage(RedisPacket provider) {
		this.sendOutgoingMessage(provider, this.server);
	}

	public void sendOutgoingMessage(RedisPacket packet, String serverID) {
		try (Jedis jedis = this.jedisPool.getResource()) {

			JsonObject data = new JsonObject();
			data.addProperty("phkey", packet.getID());
			data.addProperty("phserver", serverID);
			data.addProperty("phservertype", packet.getReceiverType().toString());

			packet.onPacketSent(data);

			jedis.publish(CHANNEL, data.toString());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void close() {
		this.sub.unsubscribe();
		this.jedisPool.destroy();
	}

	private static class Subscription extends JedisPubSub {
		private final RedisManager parent;

		private Subscription(RedisManager parent) {
			this.parent = parent;
		}

		@Override
		public void onMessage(String channel, String msg) {
			if (!channel.equals(CHANNEL)) {
				return;
			}
			this.parent.handleIncomingMessage(msg);
		}
	}
}
