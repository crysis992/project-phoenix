package net.crytec.phoenix.api.recharge;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.UUID;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.google.common.collect.Maps;

import net.crytec.phoenix.api.PhoenixAPI;
import net.crytec.phoenix.api.utils.F;
import net.crytec.phoenix.api.utils.UtilTime;

public class RechargeHandler implements PhoenixRecharge {
		
	public HashMap<UUID, HashMap<String, RechargeData>> _recharge = Maps.newHashMap();

	public RechargeHandler(PhoenixAPI api) {
		Bukkit.getScheduler().runTaskTimerAsynchronously(api.getPlugin(), () -> recharge() , 1, 1);
	}
	
	public HashMap<String, RechargeData> get(Player player) {
		if (this._recharge.containsKey(player.getUniqueId())) {
			return _recharge.get(player.getUniqueId());
		} else {
			_recharge.put(player.getUniqueId(), Maps.newHashMap());
			return get(player);
		}
	}


	@Override
	public boolean use(Player player, String ability, long recharge, boolean inform, boolean attachItem) {
		return use(player, ability, ability, recharge, inform, attachItem);
	}


	@Override
	public boolean use(Player player, String ability, String abilityFull, long recharge, boolean inform, boolean attachItem) {
		if (recharge == 0L) {
			return true;
		}

		recharge();

		if (get(player).containsKey(ability)) {
			RechargeData data = get(player).get(ability);
			String timeFormatted = F.name(UtilTime.getTimeUntil(data.getRemaining()));
			
			if (inform) {
				player.sendMessage(F.main("Cooldown", "Du kannst " + F.name(abilityFull) + " erst wieder in " + timeFormatted + " nutzen."));
			}
			return false;
		}

		useRecharge(player, ability, recharge, attachItem, inform);
		return true;
	}


	@Override
	public void useForce(Player player, String ability, long recharge) {
		useRecharge(player, ability, recharge, false, true);
	}
	

	@Override
	public boolean isUsable(Player player, String ability) {
		if (!get(player).containsKey(ability)) {
			return true;
		}
		return this.get(player).get(ability).getRemaining() < 0;
	}

	@Override
	public void recharge(Player player, String ability) {
		get(player).remove(ability);
	}

	@Override
	public void reset(Player player) {
		this._recharge.put(player.getUniqueId(), Maps.newHashMap());
	}
	
	
	private void useRecharge(Player player, String ability, long recharge, boolean attachItem, boolean inform) {
		if (attachItem) {
			get(player).put(ability, new RechargeData(player, ability, player.getInventory().getItemInMainHand(), recharge, inform));
		} else {
			get(player).put(ability, new RechargeData(System.currentTimeMillis() + recharge));
		}
	}
	
	private void recharge() {
		Bukkit.getOnlinePlayers().forEach(cur -> {
			LinkedList<String> rechargeList = get(cur).keySet().stream()
					.filter(ability -> get(cur).get(ability).update())
					.collect(Collectors.toCollection(LinkedList::new));
			
			
			Iterator<String> abilityIter = rechargeList.iterator();
			
			while (abilityIter.hasNext()) {
				String ability = abilityIter.next();
				RechargeData data  = get(cur).remove(ability);
				if (data.isInform()) {
					cur.sendMessage(F.main("Cooldown", F.name(ability) + " kann nun wieder verwendet werden."));
				}
			}
		});
	}
}