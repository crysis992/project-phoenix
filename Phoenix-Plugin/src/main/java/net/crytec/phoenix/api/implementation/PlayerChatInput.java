package net.crytec.phoenix.api.implementation;

import java.util.HashMap;
import java.util.UUID;
import java.util.function.Consumer;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.Maps;

import net.crytec.phoenix.api.player.PlayerInput;
import net.crytec.phoenix.plugin.Core;

public class PlayerChatInput implements PlayerInput, Listener {
	
	private final HashMap<UUID, Consumer<String>> players;
	private final JavaPlugin api;
	
	public PlayerChatInput(Core api) {
		this.api = api;
		this.players = Maps.newHashMap();
		Bukkit.getPluginManager().registerEvents(this, api);
		
	}
	

	public void getPlayerChatInput(Player player, Consumer<String> result) {
		this.players.put(player.getUniqueId(), result);
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onChat(AsyncPlayerChatEvent event) {
		if (players.containsKey(event.getPlayer().getUniqueId())) {
			Consumer<String> result = players.get(event.getPlayer().getUniqueId());
			players.remove(event.getPlayer().getUniqueId());
			Bukkit.getScheduler().runTask(this.api, () -> result.accept(event.getMessage())); //Possible fix for 1.14.2
			event.setCancelled(true);
			return;
		}
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		this.players.remove(e.getPlayer().getUniqueId());
	}

}
