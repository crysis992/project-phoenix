package net.crytec.phoenix.api.persistentblocks;

import org.bukkit.block.BlockState;
import org.bukkit.configuration.ConfigurationSection;

import net.crytec.phoenix.api.persistentblocks.blocks.InventoryBlock;
import net.crytec.phoenix.api.persistentblocks.blocks.PersistentMultiBlock;
import net.crytec.phoenix.api.utils.UtilLoc;

public abstract class MultiBlock extends PersistentBlock implements PersistentMultiBlock {
	
	
	private MultiBlock() { }

	@Override
	protected void onLoad(ConfigurationSection config) {
		
		this.getBlockGrid().forEach((loc, data) ->{
			BlockState state = loc.getBlock().getState();
			if(!state.getBlockData().equals(data)) {
				state.setBlockData(data);
				state.update(true);
			}
		});
		
		this.blockType = config.getString("BlockType");
		this.loadData(config);
		if(this instanceof InventoryBlock) ((InventoryBlock)this).loadInventory(config);
	}
	
	@Override
	protected void onUnload(ConfigurationSection config) {
		config.set("Location", UtilLoc.toString(this.location));
		config.set("BlockType", blockType);
		config.set("MultiBlock", true);
		this.saveData(config);
		if(this instanceof InventoryBlock) ((InventoryBlock)this).saveInventory(config);
	}
}