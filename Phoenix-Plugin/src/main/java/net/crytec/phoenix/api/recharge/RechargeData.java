package net.crytec.phoenix.api.recharge;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import net.crytec.phoenix.api.PhoenixAPI;
import net.crytec.phoenix.api.utils.UtilTime;

public class RechargeData {
	private long time;
	private long recharge;
	private UUID uuid;
	private String name;
	private ItemStack item;
	private boolean inform;

	public RechargeData(long time) {
		this.time = time;
	}

	public RechargeData(Player player, String name, ItemStack stack, long rechargeTime, boolean inform) {

		this.inform = inform;
		this.uuid = player.getUniqueId();
		this.name = name;
		this.item = player.getInventory().getItemInMainHand();
		this.recharge = System.currentTimeMillis() + rechargeTime;
	}

	public boolean update() {
		Player player = Bukkit.getPlayer(uuid);

		if ((this.item != null) && (this.name != null) && (player != null)) {
			if (player.getInventory().getItemInMainHand().getType() == this.item.getType()) {
				if (!UtilTime.isElapsed(this.recharge)) {
					if (inform) {
						String line = "§c§l" + this.name + ChatColor.RESET + " - " + "§e§l" + UtilTime.getTimeUntil(recharge);
						PhoenixAPI.get().getHandler().sendActionbar(player, line);
					}
				} else {
					if (this.recharge > 4000L) {
						player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 0.4F, 3.0F);
					}
				}
			} else {
				PhoenixAPI.get().getHandler().sendActionbar(player, "");
			}
		}
		return UtilTime.isElapsed(this.recharge);
	}

	public long getRemaining() {
		return this.recharge - (System.currentTimeMillis() - this.time);
	}

	public boolean isInform() {
		return inform;
	}
}