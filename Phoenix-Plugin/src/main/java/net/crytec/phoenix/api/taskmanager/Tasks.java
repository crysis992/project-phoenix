package net.crytec.phoenix.api.taskmanager;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import com.google.common.base.Preconditions;

import net.crytec.phoenix.plugin.Core;

public class Tasks {
	
	private static JavaPlugin plugin;
	
	static {
		plugin = Core.getInstance();
	}
	
	private Tasks() { /* Util Class */ }
	
	public static BukkitTask runTask(Runnable runnable) {
		return runTask(runnable, false);
	}
	
	public static BukkitTask runTask(Runnable runnable, boolean async) {
		Preconditions.checkNotNull(runnable, "Runnable cannot be null");
		return async ? Bukkit.getScheduler().runTaskAsynchronously(plugin, runnable) : Bukkit.getScheduler().runTask(Core.getInstance(), runnable);
	}

	public static BukkitTask schedule(final Runnable runnable, final int delay, final int interval, final int repeatitions) {
		Preconditions.checkNotNull(runnable, "Runnable cannot be null");
		return new RepeatingTask(runnable, repeatitions).runTaskTimer(plugin, delay, interval);
	}
	
	public static BukkitTask runTaskLater(Runnable runnable, long delay) {
		Preconditions.checkNotNull(runnable, "Runnable cannot be null");
		return runTaskLater(runnable, false, delay);
	}
	
	public static BukkitTask runTaskLater(Runnable runnable, boolean async, long delay) {
		Preconditions.checkNotNull(runnable, "Runnable cannot be null");
		return async ? Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, runnable, delay) : Bukkit.getScheduler().runTaskLater(Core.getInstance(), runnable, delay);
	}
	
	public static BukkitTask scheduleAsync(final Runnable runnable, final int delay, final int interval, final int repeatitions) {
		Preconditions.checkNotNull(runnable, "Runnable cannot be null");
		return new RepeatingTask(runnable, repeatitions).runTaskTimerAsynchronously(plugin, delay, interval);
	}
	
	public static BukkitTask schedule(final ConditionalRunnable runnable, final int delay, final int interval, final int repeatitions) {
		Preconditions.checkNotNull(runnable, "ConditionalRunnable cannot be null");
		return new RepeatTask(runnable, repeatitions).runTaskTimer(plugin, delay, interval);
	}
	
	public static BukkitTask scheduleAsync(final ConditionalRunnable runnable, final int delay, final int interval, final int repeatitions) {
		Preconditions.checkNotNull(runnable, "ConditionalRunnable cannot be null");
		return new RepeatTask(runnable, repeatitions).runTaskTimerAsynchronously(plugin, delay, interval);
	}

	private static class RepeatTask extends BukkitRunnable {
		
		private final ConditionalRunnable runnable;
		private int repeat;

		public RepeatTask(final ConditionalRunnable runnable, final int repeat) {
			this.runnable = runnable;
			this.repeat = repeat;
		}
		
		@Override
		public void run() {
			
			if (runnable.canCancel()) {
				this.cancel();
				runnable.onCancel();
				return;
			}
			
			runnable.run();
			
			if (repeat == -1) return;
			else repeat--;
			
			if (repeat <= 0) {
				cancel();
				runnable.onCancel();
				runnable.onRunout();
			}
		}
	}
	
	private static class RepeatingTask extends BukkitRunnable {
		
		private final Runnable runnable;
		private int repeat;

		public RepeatingTask(final Runnable runnable, final int repeat) {
			this.runnable = runnable;
			this.repeat = repeat;
		}
		
		@Override
		public void run() {
			runnable.run();
			
			if (repeat == -1) return;
			else repeat--;
			
			if (repeat <= 0) {
				cancel();
			}
		}
	}
}