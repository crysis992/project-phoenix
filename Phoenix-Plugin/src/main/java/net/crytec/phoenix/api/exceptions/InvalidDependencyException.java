package net.crytec.phoenix.api.exceptions;

public class InvalidDependencyException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -268381294744612027L;
	
	
    public InvalidDependencyException(String dependency) {
        super("There is a missing dependency: " + dependency);
    }

}
