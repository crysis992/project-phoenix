package net.crytec.phoenix.api.scoreboard;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.WrappedChatComponent;

import net.crytec.phoenix.api.events.player.PlayerReceiveTeamEvent;
import net.crytec.phoenix.api.packets.WrapperPlayServerScoreboardTeam;

public class TeamPacketListener extends PacketAdapter {

	private final JavaPlugin plugin;
	
	public TeamPacketListener(BoardManager manager) {
		super(manager.getHandler(), ListenerPriority.HIGHEST, PacketType.Play.Server.SCOREBOARD_TEAM);
		this.plugin = manager.getHandler();
	}
	
	@Override
	public void onPacketSending(PacketEvent event) {
		if(!event.getPacketType().equals(PacketType.Play.Server.SCOREBOARD_TEAM)) return;
		PacketContainer packet = event.getPacket();
		WrapperPlayServerScoreboardTeam wrapper = new WrapperPlayServerScoreboardTeam(packet);
		
		PlayerReceiveTeamEvent teamEvent = new PlayerReceiveTeamEvent(event.getPlayer(), wrapper.getPrefix(), wrapper.getSuffix(), wrapper.getColor(), wrapper);
		
		Bukkit.getScheduler().runTask(this.plugin, () -> Bukkit.getPluginManager().callEvent(teamEvent));
		
		wrapper.setColor(teamEvent.getColor());
		wrapper.setPrefix(WrappedChatComponent.fromJson(teamEvent.getPrefix().getJson()));
		wrapper.setSuffix(teamEvent.getSuffix());
	}
}