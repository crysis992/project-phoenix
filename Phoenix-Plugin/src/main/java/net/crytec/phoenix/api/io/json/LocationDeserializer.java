package net.crytec.phoenix.api.io.json;

import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

public class LocationDeserializer extends StdDeserializer<Location> {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6362337267613029630L;

	public LocationDeserializer() {
		this(null);
	}
	

	protected LocationDeserializer(Class<?> vc) {
		super(vc);
	}

	@Override
	public Location deserialize(JsonParser parser, DeserializationContext deserializer) throws IOException, JsonProcessingException {

		ObjectCodec codec = parser.getCodec();
		JsonNode node = codec.readTree(parser);
		
		World world = Bukkit.getWorld(node.get("world").asText());
		
		if (world == null) {
			return this.getNullValue(deserializer);
		}
		
		return new Location(world, node.get("x").asInt() , node.get("y").asInt(), node.get("z").asInt(), (float) node.get("yaw").asDouble(0), (float) node.get("pitch").asDouble(0));
	}
}
