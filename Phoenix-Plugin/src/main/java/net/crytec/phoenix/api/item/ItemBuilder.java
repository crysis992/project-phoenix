package net.crytec.phoenix.api.item;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.attribute.Attribute;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import net.crytec.phoenix.api.PhoenixAPI;

public class ItemBuilder implements ItemFactory {
	
	private final ItemFactory WRAPPER; 
	
	public ItemBuilder(Material material) {
		WRAPPER =  PhoenixAPI.get().getHandler().getItemFactory(material);
	}
	
	public ItemBuilder(ItemStack item) {
		WRAPPER = PhoenixAPI.get().getHandler().getItemFactory(item);
	}
	
	@Override
	public ItemStack build() {
		return WRAPPER.build();
	}

	@Override
	public ItemFactory amount(int amount) {
		return WRAPPER.amount(amount);
	}

	@Override
	public ItemFactory name(String name) {
		return WRAPPER.name(name);
	}

	@Override
	public ItemFactory lore(String string) {
		return WRAPPER.lore(string);
	}

	@Override
	public ItemFactory lore(String string, int line) {
		return WRAPPER.lore(string, line);
	}

	@Override
	public ItemFactory lore(List<String> lore) {
		return WRAPPER.lore(lore);
	}

	@Override
	public ItemFactory setUnbreakable(boolean unbreakable) {
		return WRAPPER.setUnbreakable(unbreakable);
	}

	@Override
	public ItemFactory setDurability(int durability) {
		return WRAPPER.setDurability(durability);
	}

	@Override
	public ItemFactory enchantment(Enchantment enchantment, int level) {
		return WRAPPER.enchantment(enchantment, level);
	}

	@Override
	public ItemFactory enchantment(Enchantment enchantment) {
		return WRAPPER.enchantment(enchantment);
	}

	@Override
	public ItemFactory type(Material material) {
		return WRAPPER.type(material);
	}

	@Override
	public ItemFactory clearLore() {
		return WRAPPER.clearLore();
	}

	@Override
	public ItemFactory clearEnchantment() {
		return WRAPPER.clearEnchantment();
	}

	@Override
	public ItemFactory setSkullOwner(OfflinePlayer player) {
		return WRAPPER.setSkullOwner(player);
	}

	@Override
	public ItemFactory setItemFlag(ItemFlag flag) {
		return WRAPPER.setItemFlag(flag);
	}

	@Override
	public ItemFactory addNBTString(String key, String value) {
		return WRAPPER.addNBTString(key, value);
	}

	@Override
	public ItemFactory addNBTDouble(String key, double value) {
		return WRAPPER.addNBTDouble(key, value);
	}

	@Override
	public ItemFactory addNBTBoolean(String key, boolean value) {
		return WRAPPER.addNBTBoolean(key, value);
	}

	@Override
	public ItemFactory addNBTInt(String key, int value) {
		return WRAPPER.addNBTInt(key, value);
	}

	@Override
	public ItemFactory addNBTLong(String key, long value) {
		return WRAPPER.addNBTLong(key, value);
	}

	@Override
	public ItemFactory setAttribute(Attribute attribute, double amount, EquipmentSlot slot) {
		return WRAPPER.setAttribute(attribute, amount, slot);
	}

	@Override
	public ItemFactory removeAttribute(Attribute attribute) {
		return WRAPPER.removeAttribute(attribute);
	}

	@Override
	public ItemFactory setModelData(int data) {
		return WRAPPER.setModelData(data);
	}
}