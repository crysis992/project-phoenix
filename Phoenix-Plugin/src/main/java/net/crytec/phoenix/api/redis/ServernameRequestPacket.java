package net.crytec.phoenix.api.redis;

import org.bukkit.Bukkit;

import com.google.gson.JsonObject;

import net.crytec.phoenix.api.PhoenixAPI;

public class ServernameRequestPacket extends RedisPacket {

	private static RedisManager manager;

	public ServernameRequestPacket(RedisManager manager) {
		super("servername", RedisServerType.BUNGEE);
		ServernameRequestPacket.manager = manager;
	}

	public ServernameRequestPacket() {
		super("servername", RedisServerType.BUNGEE);
	}

	@Override
	public void onPacketSent(JsonObject data) {
		data.addProperty("servername", "requested");
		data.addProperty("port", Bukkit.getServer().getPort());
	}

	@Override
	public void onPacketReceive(JsonObject data) {
		String servername = data.get("servername").getAsString();
		int port = data.get("port").getAsInt();

		if (Bukkit.getServer().getPort() == port) {
			PhoenixAPI.get().getPlugin().getLogger().info("Successfully received servername from Bungeecord. This server is now locally registered as " + servername);
			manager.setServer(servername);
		}
	}
}