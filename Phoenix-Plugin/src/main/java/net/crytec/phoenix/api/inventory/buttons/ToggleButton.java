package net.crytec.phoenix.api.inventory.buttons;

import java.util.List;
import java.util.function.Consumer;

import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import net.crytec.phoenix.api.inventory.ClickableItem;
import net.crytec.phoenix.api.item.ItemBuilder;

public class ToggleButton extends ClickableItem {
	
	public static final Material off = Material.GRAY_DYE;
	public static final Material on = Material.LIME_DYE;

	public ToggleButton(ItemStack item, Consumer<InventoryClickEvent> consumer) {
		super(item, consumer);
	}
	
	public ToggleButton(String name, List<String> desc, boolean current, Consumer<Boolean> mode) {
		this(new ItemBuilder( current ? on : off ).name(name).lore(desc).build(), e -> {
			mode.accept(!current);
		});
	}
	
	public ToggleButton(String name, boolean current, Consumer<Boolean> mode) {
		this(new ItemBuilder( current ? on : off ).name(name).build(), e -> {
			mode.accept(!current);
		});
	}
}