package net.crytec.phoenix.api.actionbar;

import java.util.UUID;
import java.util.concurrent.ConcurrentMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import com.google.common.collect.Maps;

import net.crytec.phoenix.api.PhoenixAPI;
import net.crytec.phoenix.api.actionbar.ActionBarSection.StringArrangement;

/**
 * 
 * @author Gestankbratwurst
 * @author crysis992
 *
 */
public class ActionBarHandler implements Listener, PhoenixActionBar {
	
	private final PhoenixAPI api;
	
	public ActionBarHandler(PhoenixAPI api) {
		
		this.api = api;
		
		Bukkit.getPluginManager().registerEvents(this, api.getPlugin() );
		
		
		Bukkit.getScheduler().runTaskTimerAsynchronously(api.getPlugin(), () -> {

			container.forEach((p, c) -> {
				api.getHandler().sendActionbar(p, c.getFullHighestPriorityBar());
			});

		}, 45, 10);

	}
	
	private final ConcurrentMap<Player, ActionBarContainer> container = Maps.newConcurrentMap();
	protected final ConcurrentMap<UUID, ActionBarSection> sections = Maps.newConcurrentMap();
	
	@Override
	public ActionBarSection getSection(UUID id) {
		return sections.get(id);
	}
	
	@Override
	public void removeSection(ActionBarSection section, Player player, int index) {
		this.removeSection(section.getId(), player, index);
	}
	
	@Override
	public void removeSection(UUID id, Player player, int index) {
		this.container.get(player).removeSection(index, this.sections.get(id));
		this.sections.remove(id);
	}
	
	@Override
	public ActionBarSection createSection(int length, StringArrangement arrangement, int priority) {
		UUID id = UUID.randomUUID();
		ActionBarSection section = new ActionBarSection(length, arrangement, priority, id);
		this.sections.put(id, section);
		return section;
	}
	
	@Override
	public void insertSection(Player player, int index, ActionBarSection section) {
		this.container.get(player).insert(index, section);
	}
	
	@Override
	public void updateFor(Player player) {
		this.api.getHandler().sendActionbar(player, this.container.get(player).getFullHighestPriorityBar());
	}
	
	@Override
	public ActionBarContainer getActionBarContainer(Player player) {
		return container.get(player);
	}
	
	@Override
	public ActionBarContainer createActionBarContainer(int sectionCount, int preferredLength, String sectionBorder) {
		return new ActionBarContainerManager(this, sectionCount, preferredLength, sectionBorder);
	}
	
	@Override
	public void addPlayer(Player player, ActionBarContainer container) {
		this.container.put(player, container);
	}
	
	
	@EventHandler
	public void cleanOnQuit(PlayerQuitEvent event) {
		ActionBarContainer container = this.container.remove(event.getPlayer());
		if (container != null) container.cleanUp();
	}
}