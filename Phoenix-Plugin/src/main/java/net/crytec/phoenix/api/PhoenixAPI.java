package net.crytec.phoenix.api;

import java.util.List;
import java.util.function.Consumer;

import org.apache.commons.lang.Validate;
import org.apache.commons.lang3.EnumUtils;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import net.crytec.phoenix.api.NPCs.PhoenixNPCManager;
import net.crytec.phoenix.api.actionbar.ActionBarHandler;
import net.crytec.phoenix.api.actionbar.PhoenixActionBar;
import net.crytec.phoenix.api.chat.ChatQueueManager;
import net.crytec.phoenix.api.chat.chatpause.PhoenixChatQueue;
import net.crytec.phoenix.api.chat.program.ChatEditorCore;
import net.crytec.phoenix.api.holograms.PhoenixHologramManager;
import net.crytec.phoenix.api.holograms.infobars.InfoBarManager;
import net.crytec.phoenix.api.implementation.AnvilGUI;
import net.crytec.phoenix.api.implementation.PlayerChatInput;
import net.crytec.phoenix.api.inventory.InventoryManager;
import net.crytec.phoenix.api.io.PlayerDataManager;
import net.crytec.phoenix.api.io.language.EnumLang;
import net.crytec.phoenix.api.io.language.Language;
import net.crytec.phoenix.api.itemactions.ItemActions;
import net.crytec.phoenix.api.miniblocks.MiniBlockManager;
import net.crytec.phoenix.api.nbt.MinecraftVersion;
import net.crytec.phoenix.api.persistentblocks.PersistentBlockListener;
import net.crytec.phoenix.api.persistentblocks.PersistentBlockManager;
import net.crytec.phoenix.api.persistentblocks.PhoenixPersistentBlocks;
import net.crytec.phoenix.api.recharge.PhoenixRecharge;
import net.crytec.phoenix.api.recharge.RechargeHandler;
import net.crytec.phoenix.api.recipes.RecipeManager;
import net.crytec.phoenix.api.redis.PhoenixRedis;
import net.crytec.phoenix.api.redis.RedisManager;
import net.crytec.phoenix.api.scoreboard.BoardManager;
import net.crytec.phoenix.api.scoreboard.PhoenixBoardManager;
import net.crytec.phoenix.api.utils.F;
import net.crytec.phoenix.api.version.ImplementationHandler;
import net.crytec.phoenix.listener.GlobalListener;
import net.crytec.phoenix.listener.WorldGuardRegions;
import net.crytec.phoenix.plugin.ConfigOptions;
import net.crytec.phoenix.plugin.Core;

public class PhoenixAPI {
	
	private static final String nmsPackage = "net.crytec.phoenix.api.version.";
	private static final String className = ".NMSManager";
	private static PhoenixAPI instance;
	
	private final PlayerChatInput playerChatInput;
	private final Core plugin;
	
	private ImplementationHandler handler;
	
	private BoardManager scoreboardManager;
	
	private final PersistentBlockManager blockManager;
	
	private ActionBarHandler actionBarHandler;
	
	private ChatQueueManager chatQueueManager;
	
	private final PhoenixRecharge rechargeManager;
	
	private PhoenixRedis redisManager;
	
	private final InfoBarManager infoBarManager;
	
	public PhoenixAPI(Core plugin) {
		PhoenixAPI.instance = this;
		this.plugin = plugin;
		
		this.playerChatInput = new PlayerChatInput(this.plugin);
		this.setupServerImplementation();
		this.handler.startup(this.plugin);
		
		// Setup NBT Items
		MinecraftVersion.getVersion();
		
		// Setup Anvil GUI
		AnvilGUI.initialize(this);
		
		// Setup Smart Inventories
		new InventoryManager(this.plugin);
		
		// Setup Worldguard Region Events
		if (Bukkit.getPluginManager().getPlugin("WorldGuard") != null) {
			WorldGuardRegions.initialize(this);
		}
		
		// Setup ActionBars
		this.actionBarHandler = new ActionBarHandler(this);
		
		//Setup Recharge
		this.rechargeManager = new RechargeHandler(this);
		
		//Setup ItemActions
		new ItemActions(plugin);
		
		//Setup Entity Info Bars
		this.infoBarManager = this.handler.getInfoBarManager();
		
		//Initialize ChatEditor
		new ChatEditorCore(this.getPlugin());
		
		// Check if ProtocolLib is installed, some modules require it.
		if (Bukkit.getPluginManager().getPlugin("ProtocolLib") != null) {
		
			// Setup ChatQueue Manager
			this.chatQueueManager = new ChatQueueManager(this);

			// Setup Scoreboard Manager
			if (ConfigOptions.USE_SCOREBOARD.asBoolean()) {
				this.scoreboardManager = new BoardManager(this);
				this.plugin.getLogger().info("Using custom ScoreboardAPI!");
				this.plugin.getLogger().info("This will most likely overwrite all other scoreboard plugins!");
			}
		
		}
		
		new PlayerDataManager(this.plugin);
		
		Bukkit.getPluginManager().registerEvents(new GlobalListener(), this.plugin);
		
		if (ConfigOptions.USE_REDIS.asBoolean()) {
			this.redisManager = new RedisManager(this);
			this.redisManager.requestServername();
		}
		
		
		this.blockManager = new PersistentBlockManager(this.plugin);
		Bukkit.getPluginManager().registerEvents(new PersistentBlockListener(this.blockManager), plugin);
		
		
		// Load language file
		
		if (EnumUtils.isValidEnum(EnumLang.class, ConfigOptions.LANGUAGE.asString())) {
			this.getLanguageHelper().setLanguage(EnumLang.valueOf(ConfigOptions.LANGUAGE.asString()));
		} else {
			this.getPlugin().getLogger().severe("Unable to load language file! The language key " + ConfigOptions.LANGUAGE.asString() + " is invalid.");
			this.getPlugin().getLogger().severe("Loading default " + EnumLang.EN_US.getLocale());
			this.getLanguageHelper().setLanguage(EnumLang.EN_US);
		}
		
		// Initialize Format class
		F.init(this.getLanguageHelper());
		
	}

	public static PhoenixAPI get() {
		return PhoenixAPI.instance;
	}

	public void getPlayerChatInput(Player player, Consumer<String> result) {
		this.playerChatInput.getPlayerChatInput(player, result);
	}
	
	public Language getLanguageHelper() {
		return this.handler.getLanguageHelper();
	}
	
	public PhoenixBoardManager getScoreboardManager() {
		return this.scoreboardManager;
	}
	
	public PhoenixActionBar getActionBarManager() {
		return this.actionBarHandler;
	}
	
	public PhoenixChatQueue getChatQueue() {
		return this.chatQueueManager;
	}
	
	public PhoenixRecharge getRecharge() {
		return this.rechargeManager;
	}
	
	public PhoenixRedis getRedis() {
		return this.redisManager;
	}
	
	public PhoenixPersistentBlocks getPersistentBlockManager() {
		return this.getInternalPersistentBlockManager();
	}
	
	public boolean requireAPIVersion(JavaPlugin plugin, int requiredVersion) {
		String version = this.getPlugin().getDescription().getVersion();
		
		version = version.replaceAll("\\.", "");
		int api_version = Integer.parseInt(version);
		
		if (api_version < requiredVersion) {
			this.getPlugin().getLogger().info(plugin.getName() + " requires API Version " + requiredVersion + ". You have " + api_version);
			String ver = String.valueOf(requiredVersion);
			log("===================================");
			log("§c Unable to load " + plugin.getDescription().getName());
			log("§c You need at least version: §e" + ver.substring(0, 1) + "." + ver.substring(1, 2) + "."
					+ ver.substring(2, 3) + "§c but you have: §6" + this.getPlugin().getDescription().getVersion());
			log("§c Please update PhoenixAPI to the latest version if you wish to use this plugin!");
			log("");
			log("§6 Download the latest version here: https://www.spigotmc.org/resources/24842/");
			log("===================================");
			return false;
		}
		return true;
	}
	
	
	public ImplementationHandler getHandler() {
		return this.handler;
	}
	
	public JavaPlugin getPlugin() {
		return this.plugin;
	}
	
	public PersistentBlockManager getInternalPersistentBlockManager() {
		return this.blockManager;
	}
	
	public void shutdown() {
		
		// Shutdown Persistent Block Manager
		this.blockManager.onDisable();
		
		if (this.redisManager != null) ((RedisManager) this.redisManager).close();
	}
	
	
	private void setupServerImplementation() {
		try {
			final Class<?> clazz = Class.forName(nmsPackage + this.plugin.getVersion().getVersion().getPackageName() + className);
			this.handler = (ImplementationHandler) clazz.newInstance();
		} catch (final Exception e) {
			e.printStackTrace();
			return;
		}
	}
	
	private void log(String message) {
		ConsoleCommandSender s = Bukkit.getConsoleSender();
		s.sendMessage(message);
	}

	public PhoenixHologramManager getHologramManager() {
		return this.getHandler().getHologramManager();
	}

	public PhoenixNPCManager getNPCManager() {
		return this.getHandler().getNPCManager();
	}

	public RecipeManager getRecipeManager() {
		return this.getHandler().getRecipeManager();
	}

	public MiniBlockManager getMiniBlockManager() {
		return this.getHandler().getMiniBlockManager();
	}

	public String serializeItemStack(ItemStack item) {
		Validate.notNull(item, "ItemStack cannot be null!");
		return this.handler.serializeItemStack(item);
	}

	public ItemStack deserializeItemStack(String json) {
		Validate.notNull(json, "String cannot be null!");
		return this.handler.deserializeItemStack(json);
	}

	public InfoBarManager getEntityInfoBarManager() {
		return infoBarManager;
	}

	public List<ItemStack> breakBlock(Player player, Block block, ItemStack tool) {
		return this.handler.breakBlock(player, block, tool);
	}

}