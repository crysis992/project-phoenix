package net.crytec.phoenix.api.persistentblocks;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.BlockState;
import org.bukkit.block.data.BlockData;
import org.bukkit.configuration.ConfigurationSection;

import net.crytec.phoenix.api.PhoenixAPI;
import net.crytec.phoenix.api.persistentblocks.blocks.InventoryBlock;
import net.crytec.phoenix.api.persistentblocks.blocks.PersistentBaseBlock;
import net.crytec.phoenix.api.utils.UtilChunk;
import net.crytec.phoenix.api.utils.UtilLoc;

public abstract class PersistentBlock implements PersistentBaseBlock {
	
	private static PersistentBlockManager persistentBlockManager;
	
	static {
		PersistentBlock.persistentBlockManager = PhoenixAPI.get().getInternalPersistentBlockManager();
	}
	
	protected PersistentBlock() {
		this.loadData = true;
		this.timesTicked = 0;
		this.holoTicked = 0;
	}
	
	protected boolean loadData;
	protected Location location;
	protected String blockData;
	protected String blockType;
	protected long chunkID;
	protected long timesTicked;
	protected long holoTicked;
	protected boolean stateLinked = false;
	
	@Override
	public void delete() {
		PersistentBlock.persistentBlockManager.removePersistentBlock(this);
	}
	
	protected void onLoad(ConfigurationSection config) {
		this.stateLinked = config.getBoolean("StateLinked");
		this.blockData = config.getString("BlockData");
		this.chunkID = UtilChunk.getChunkKey(location);
		this.blockType = config.getString("BlockType");
		BlockState state = this.location.getBlock().getState();
		BlockData data = Bukkit.createBlockData(this.blockData);
		if(state.getBlockData() != data) {
			if(this.stateLinked) {
				PersistentBlockManager.instance.removePersistentBlock(this);
				return;
			}else {
				state.setBlockData(data);
				state.update(true, false);
			}
		}
		
		this.loadData(config);
		if(this instanceof InventoryBlock) ((InventoryBlock)this).loadInventory(config);
	}
	
	protected void onUnload(ConfigurationSection config) {
		config.set("StateLinked", this.stateLinked);
		config.set("BlockData", this.blockData);
		config.set("Location", UtilLoc.toString(this.location));
		config.set("BlockType", blockType);
		config.set("MultiBlock", false);
		this.saveData(config);
		if(this instanceof InventoryBlock) ((InventoryBlock)this).saveInventory(config);
	}

	public boolean isLoadData() {
		return loadData;
	}

	public void setLoadData(boolean loadData) {
		this.loadData = loadData;
	}

	public String getBlockType() {
		return blockType;
	}

	public void setBlockType(String blockType) {
		this.blockType = blockType;
	}

	public boolean isStateLinked() {
		return stateLinked;
	}

	public void setStateLinked(boolean stateLinked) {
		this.stateLinked = stateLinked;
	}

	public Location getLocation() {
		return location;
	}
	
	
}