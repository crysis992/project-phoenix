package net.crytec.phoenix.api.scoreboard;

import java.util.HashMap;
import java.util.UUID;

import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.server.ServerLoadEvent;
import org.bukkit.event.server.ServerLoadEvent.LoadType;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import com.comphenix.protocol.ProtocolLibrary;
import com.google.common.collect.Maps;

import net.crytec.phoenix.api.PhoenixAPI;

public class BoardManager implements Listener, PhoenixBoardManager, Runnable {

	private HashMap<UUID, Team> users = Maps.newHashMap();
	private HashMap<UUID, PlayerBoard> boards = Maps.newHashMap();
	
	private final Scoreboard titleboard;
	private boolean customHandler = false;
	
	private JavaPlugin handler;
	
	public BoardManager(PhoenixAPI api) {
		Validate.notNull(Bukkit.getPluginManager().getPlugin("ProtocolLib"), "The Scoreboard API requires ProtocolLib.");
		
		this.handler = api.getPlugin();
		
		ProtocolLibrary.getProtocolManager().addPacketListener(new TeamPacketListener(this));
		
		this.titleboard = Bukkit.getScoreboardManager().getNewScoreboard();
		Bukkit.getPluginManager().registerEvents(this, api.getPlugin());
		
		Objective obj = this.titleboard.registerNewObjective("sidebar", "dummy", " ");
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);
		
		Bukkit.getScheduler().runTaskTimerAsynchronously(api.getPlugin(), this, 20, 5);
	}
	
	@Override
	public void addPlayer(Player player, String priority) {
		if (this.users.containsKey(player.getUniqueId())) {
			this.unregisterPlayer(player);
		}
		
		String name = player.getName();
		
		if (name.length() > 14) {
			name = name.substring(0, 14);
		}
		
		if (priority.length() > 2) {
			priority = priority.substring(0, 2);
		}
		
		Team team = this.titleboard.registerNewTeam(priority + name);
		
		team.setPrefix("");
		team.setSuffix("");
		team.setColor(ChatColor.WHITE);
		team.addEntry(player.getName());
		this.users.put(player.getUniqueId(), team);
		
		player.setScoreboard(this.titleboard);
		this.boards.put(player.getUniqueId(), new PlayerBoard(player));
	}
	
	@Override
	public void addPlayer(Player player) {
		this.addPlayer(player, "0");
	}

	@Override
	public void unregisterPlayer(Player player) {
		if (!this.users.containsKey(player.getUniqueId())) return;
		this.titleboard.getTeam(this.users.get(player.getUniqueId()).getName()).unregister();
		player.setScoreboard(Bukkit.getScoreboardManager().getMainScoreboard());
		users.remove(player.getUniqueId());
		boards.get(player.getUniqueId()).deactivate();
		boards.remove(player.getUniqueId());
	}
	
	@Override
	public void setBoard(Player player, ScoreboardHandler handler) {
		PlayerBoard board = this.getBoard(player);
		if (board.isActivated()) {
			board.deactivate();
		}
		board.setHandler(handler);
		board.activate();
	}
	
	@Override
	public PlayerBoard getBoard(Player player) {
		return this.boards.get(player.getUniqueId());
	}
	
	@Override
	public void resetBoard(Player player) {
		this.getBoard(player).deactivate();
	}
	
	@Override
	public void setPrefix(Player player, String prefix) {
		this.users.get(player.getUniqueId()).setPrefix(prefix);
	}
	
	@Override
	public void setSuffix(Player player, String suffix) {
		this.users.get(player.getUniqueId()).setSuffix(suffix);
	}
	
	public HashMap<UUID, Team> getUsers() {
		return users;
	}

	public Scoreboard getTitleboard() {
		return titleboard;
	}

	public JavaPlugin getHandler() {
		return handler;
	}

	@EventHandler
	public void handleReload(ServerLoadEvent event) {
		if (event.getType() == LoadType.RELOAD) {
			if (this.customHandler) return;
			Bukkit.getOnlinePlayers().forEach( cur -> this.addPlayer(cur));
		}
	}
	
	@Override
	public void setHandler(JavaPlugin plugin) {
		this.handler = plugin;
		this.customHandler = true;
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		if (this.customHandler) return;
		this.addPlayer(event.getPlayer());
	}
	
	@EventHandler
	public void onJoin(PlayerQuitEvent event) {
		if (this.customHandler) return;
		this.unregisterPlayer(event.getPlayer());
	}

	@Override
	public void run() {
		for (PlayerBoard board : boards.values()) {
			if (!board.isActivated()) continue;
			board.update();
		}
		
	}
}