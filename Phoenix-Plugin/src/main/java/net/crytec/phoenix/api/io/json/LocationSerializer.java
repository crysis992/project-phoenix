package net.crytec.phoenix.api.io.json;

import java.io.IOException;

import org.bukkit.Location;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class LocationSerializer extends StdSerializer<Location> {


	/**
	 * 
	 */
	private static final long serialVersionUID = -7571450115629747295L;

	public LocationSerializer() {
		this(null);
	}

	protected LocationSerializer(Class<Location> t) {
		super(t);
	}

	@Override
	public void serialize(Location location, JsonGenerator jsonGenerator, SerializerProvider serializer) throws IOException {

		
		jsonGenerator.writeStartObject();

		Location center = location.clone().add(0.5, 0.5, 0.5);
		
		jsonGenerator.writeNumberField("x" , center.getBlockX());
		jsonGenerator.writeNumberField("y" , center.getBlockY());
		jsonGenerator.writeNumberField("z" , center.getBlockZ());
		
		jsonGenerator.writeNumberField("pitch" , center.getPitch());
		jsonGenerator.writeNumberField("yaw" , center.getYaw());
		
		jsonGenerator.writeStringField("world", center.getWorld().getName());

		jsonGenerator.writeEndObject();

	}

}
