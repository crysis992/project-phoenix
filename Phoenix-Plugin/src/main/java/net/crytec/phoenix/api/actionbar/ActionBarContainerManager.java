package net.crytec.phoenix.api.actionbar;

import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.Lists;
import com.google.common.collect.Queues;
import com.google.common.collect.Sets;

import net.crytec.phoenix.api.actionbar.ActionBarSection.StringArrangement;
/**
 * Used to create splittable ActionBars
 * @author Gestankbratwurst
 * @author crysis992
 *
 */
public class ActionBarContainerManager implements ActionBarContainer {
	
	/**
	 * Main constructor
	 * @param sectionCount the amount of sections the actionbar should have.
	 * @param preferredLength the length of a single section.
	 * @param sectionBorder the seperator between sections.
	 */
	public ActionBarContainerManager(ActionBarHandler handler, int sectionCount, int preferredLength, String sectionBorder) {
		
		this.handler = handler;
		
		this.sectionCount = sectionCount;
		this.sectionBorder = sectionBorder;
		this.staticBar = new ActionSpreadBar();
		this.sections = Lists.newArrayList();
		this.sectionIdentifier = Sets.newHashSet();
		this.nullString = StringUtils.repeat(" ", preferredLength);
		for(int index = 0; index < this.sectionCount; index++) {
			sections.add(Queues.newPriorityQueue());
		}
	}
	
	private final ActionBarHandler handler;
	
	private final String nullString;
	private final int sectionCount;
	private String sectionBorder;
	private final ArrayList<PriorityQueue<ActionBarSection>> sections;
	private final ActionSpreadBar staticBar;
	
	private final Set<UUID> sectionIdentifier;
	
	/**
	 * Inserts a section in one of the places specified with sectionCount.
	 * @param index the sections index (left to right)
	 * @param section the section to insert.
	 * @throws IllegalArgumentException if the section index is out of bounds.
	 */
	public void insert(int index, ActionBarSection section) throws IllegalArgumentException{
		if(index >= this.sectionCount) throw new IllegalArgumentException("Index " + index + " is out of bounds.");
		this.sections.get(index).add(section);
	}
	
	/**
	 * Creates a new {@link ActionBarSection}
	 * @param length the length of this section
	 * @param arrangement the {@link StringArrangement} of this section
	 * @param priority the show priority
	 * @return a new ActionBarSection
	 */
	public ActionBarSection createSection(int length, StringArrangement arrangement, int priority) {
		return this.handler.createSection(length, arrangement, priority);
	}
	
	/**
	 * Removes all sections currently loaded in this container.
	 */
	public void cleanUp() {
		this.sectionIdentifier.forEach(section -> this.handler.sections.remove(section));
	}
	
	/**
	 * Gets a queue of all sections currently located at a
	 * place in this container.
	 * @param index the sections index
	 * @return 
	 * a {@link PriorityQueue} of {@link ActionBarSection}
	 * for this slot
	 * null if out of bounds.
	 */
	public PriorityQueue<ActionBarSection> getSectionHeap(int index){
		if(index >= sections.size()) return null;
		return this.sections.get(index);
	}
	
	
	/**
	 * The {@link ActionSpreadBar}
	 */
	public ActionSpreadBar getStaticBar() {
		return staticBar;
	}

	/**
	 * Gets a view only String of the complete Actionbar to display.
	 * @return a String representation of this Actionbar
	 */
	public String getFullHighestPriorityBar() {
		
		if(this.staticBar.isShowing()) return this.staticBar.getActionBar();
		
		String line = "";
		
		for(int index = 0; index < this.sectionCount; index++) {
			line += this.sectionBorder;
			if(sections.get(index).isEmpty()) {
				line += this.nullString;
			}else {
				line += sections.get(index).peek().getContent();
			}
		}
		
		line += this.sectionBorder;
		
		return line;
	}

	@Override
	public void removeSection(int index, ActionBarSection section) {
		this.sections.get(index).remove(section);
	}
	
}
