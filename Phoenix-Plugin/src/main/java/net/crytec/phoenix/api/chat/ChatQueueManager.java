package net.crytec.phoenix.api.chat;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.stream.Stream;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketContainer;
import com.google.common.collect.Maps;

import net.crytec.phoenix.api.PhoenixAPI;
import net.crytec.phoenix.api.chat.chatpause.PacketJam;
import net.crytec.phoenix.api.chat.chatpause.PhoenixChatQueue;

public class ChatQueueManager implements Listener, PhoenixChatQueue {
	
	public ChatQueueManager(PhoenixAPI api) {
		this.playerToListenFor = Maps.newConcurrentMap();
		this.protocolManager = ProtocolLibrary.getProtocolManager();
		this.protocolManager.addPacketListener(new ChatQueueListener(api.getPlugin(), this));
		Bukkit.getPluginManager().registerEvents(this, api.getPlugin());
	}
	
	private final ProtocolManager protocolManager;
	private final Map<Player, PacketJam<PacketContainer>> playerToListenFor;
	
	@Override
	public void addPacket(Player player, PacketContainer packet) throws IllegalStateException{
		if(!this.playerToListenFor.containsKey(player)) throw new IllegalStateException("Player " + player.getUniqueId() + " is not registered.");
		this.playerToListenFor.get(player).append(packet);
	}
	@Override
	public void registerPlayer(Player player) {
		this.playerToListenFor.put(player, new PacketJam<PacketContainer>(30));
	}
	
	@Override
	public boolean isRegistered(Player player) {
		return this.playerToListenFor.containsKey(player);
	}
	
	@Override
	public void unregisterPlayer(Player player) {
		this.unregisterPlayer(player, true);
	}
	
	@Override
	public void unregisterPlayer(Player player, boolean sendInterceptedPackets) throws IllegalStateException{
		if(!this.playerToListenFor.containsKey(player)) throw new IllegalStateException("Player " + player.getUniqueId() + " is not registered.");
		Stream<PacketContainer> stream = this.playerToListenFor.get(player).release();
		this.removePlayer(player);
		
		//Remove player & drop packets when offline
		if (!player.isOnline()) {
			this.playerToListenFor.remove(player);
			return;
		}
		
		if (!sendInterceptedPackets) return;
			
		for(int i = 0; i < 80; i++) {
			player.sendMessage("");
		}
		
		stream.forEach(packet -> {
			try {
				this.protocolManager.sendServerPacket(player, packet);
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		});
	}
	
	private void removePlayer(Player player) {
		this.playerToListenFor.remove(player);
	}
	
	@EventHandler
	public void cleanOnQuit(PlayerQuitEvent event) {
		if (this.isRegistered(event.getPlayer())) {
			this.playerToListenFor.remove(event.getPlayer());
		}
	}
}