package net.crytec.phoenix.api.chat;

import org.bukkit.plugin.Plugin;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;

import net.crytec.phoenix.api.chat.chatpause.PhoenixChatQueue;

public class ChatQueueListener extends PacketAdapter {

	public ChatQueueListener(Plugin plugin, PhoenixChatQueue queueManager) {
		super(plugin, PacketType.Play.Server.CHAT);
		this.queueManager = queueManager;
	}

	private final PhoenixChatQueue queueManager;

	@Override
	public void onPacketSending(PacketEvent event) {

		if (!this.queueManager.isRegistered(event.getPlayer()))
			return;
		if (!event.getPacketType().equals(PacketType.Play.Server.CHAT))
			return;
		if (event.getPacket().getMeta("editor").isPresent())
			return;

		queueManager.addPacket(event.getPlayer(), event.getPacket());
		event.setCancelled(true);
	}
}