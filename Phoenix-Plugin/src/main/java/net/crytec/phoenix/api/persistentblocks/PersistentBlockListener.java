package net.crytec.phoenix.api.persistentblocks;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.event.world.WorldLoadEvent;
import org.bukkit.event.world.WorldUnloadEvent;

public class PersistentBlockListener implements Listener {
	
	public PersistentBlockListener(PersistentBlockManager blockManager) {
		this.blockManager = blockManager;
	}
	
	private final PersistentBlockManager blockManager;
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onExplode(BlockExplodeEvent event) {
		if(event.isCancelled()) return;
		this.blockManager.handleExplode(event);
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onExplode(EntityExplodeEvent event) {
		if(event.isCancelled()) return;
		this.blockManager.handleExplode(event);
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onInteract(PlayerInteractEvent event) {
		if (event.useInteractedBlock() == Result.DENY || event.useItemInHand() == Result.DENY) return;
		this.blockManager.handleInteract(event);
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onBlockBreak(BlockBreakEvent event) {
		if(event.isCancelled()) return;
		this.blockManager.handleBlockBreak(event);
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onBurn(BlockBurnEvent event) {
		if(event.isCancelled()) return;
		this.blockManager.handleBlockBurn(event);
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onExtend(BlockPistonExtendEvent event) {
		if(event.isCancelled()) return;
		event.getBlocks().forEach(block ->{
			if(this.blockManager.isPersistent(block.getLocation())) {
				this.blockManager.handlePistonExtend(event, block);
			}
		});
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void onRetract(BlockPistonRetractEvent event) {
		if(event.isCancelled()) return;
		event.getBlocks().forEach(block ->{
			if(this.blockManager.isPersistent(block.getLocation())) {
				this.blockManager.handlePistonRetract(event, block);
			}
		});
	}
		
	/*
	 *  Saving / Loading of Blocks
	 */
	
	@EventHandler
	public void onChunkLoad(ChunkLoadEvent event) {
		Chunk chunk = event.getChunk();
		if (event.isNewChunk()) return;
		this.blockManager.loadFilesFromSystem(chunk);
	}
	
	@EventHandler
	public void onChunkUnload(ChunkUnloadEvent event) {
		Chunk chunk = event.getChunk();
		
		File persistentfolder = new File(Bukkit.getWorldContainer(), event.getWorld().getName() + File.separator + "persistentblocks");
		if (!persistentfolder.exists()) persistentfolder.mkdir();
		
		this.blockManager.saveFilesToSystem(chunk);
	}
	
	@EventHandler
	public void onLoadWorld(WorldLoadEvent event) {
		World world = event.getWorld();
		File persistentfolder = new File(Bukkit.getWorldContainer(), event.getWorld().getName() + File.separator + "persistentblocks");
		
		if (!persistentfolder.exists()) persistentfolder.mkdir();
		
		for (Chunk chunk : world.getLoadedChunks()) {
			this.blockManager.loadFilesFromSystem(chunk);
		}
	}
	
	@EventHandler
	public void onUnloadWorld(WorldUnloadEvent event) {

		for (Chunk chunk : event.getWorld().getLoadedChunks()) {
			this.blockManager.saveFilesToSystem(chunk);
		}
	}
	
}