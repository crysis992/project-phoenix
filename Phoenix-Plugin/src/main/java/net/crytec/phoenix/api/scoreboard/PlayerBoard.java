package net.crytec.phoenix.api.scoreboard;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.comphenix.protocol.wrappers.EnumWrappers.ScoreboardAction;
import com.comphenix.protocol.wrappers.WrappedChatComponent;

import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.ints.IntOpenHashSet;
import it.unimi.dsi.fastutil.ints.IntSet;
import net.crytec.phoenix.api.packets.WrapperPlayServerScoreboardDisplayObjective;
import net.crytec.phoenix.api.packets.WrapperPlayServerScoreboardObjective;
import net.crytec.phoenix.api.packets.WrapperPlayServerScoreboardObjective.HealthDisplay;
import net.crytec.phoenix.api.packets.WrapperPlayServerScoreboardScore;
import net.crytec.phoenix.api.packets.WrapperPlayServerScoreboardTeam;

public class PlayerBoard implements PhoenixBoard {

	private static final List<String> teams = IntStream.rangeClosed(0, 15).mapToObj(i -> ChatColor.values()[i].toString()).collect(Collectors.toList());
	private final Player player;
	
	private final Int2ObjectOpenHashMap<String> cache;
	private final IntSet cache2 = new IntOpenHashSet();
	
	private boolean activated;
	private ScoreboardHandler handler;
	private boolean hidden = false;
	
	private String title;
	
	public PlayerBoard(Player player) {
		this.player = player;
		this.cache = new Int2ObjectOpenHashMap<String>(15);
	}
	
	@Override
	public void activate() {
		if (activated)
			return;
		if (handler == null)
			throw new IllegalStateException("Scoreboard handler not set");

		this.activated = true;
		
		this.cache.clear();
		
		// Register Teams
		for (int i = 0; i < 15; i++) {
			WrapperPlayServerScoreboardTeam line = new WrapperPlayServerScoreboardTeam();
			line.setName(teams.get(i));
			line.setPlayers(Arrays.asList(teams.get(i)));
			line.setMode(0);
			line.sendPacket(this.player);
		}
		
		if (hidden) {
			WrapperPlayServerScoreboardObjective display = new WrapperPlayServerScoreboardObjective();
			display.setName("sidebar");
			display.setDisplayName(WrappedChatComponent.fromText(this.title));
			display.setMode(0);
			display.sendPacket(player);
			
			WrapperPlayServerScoreboardDisplayObjective obj = new WrapperPlayServerScoreboardDisplayObjective();
			obj.setPosition(1);
			obj.setScoreName("sidebar");
			obj.sendPacket(this.player);
		}
	}

	@Override
	public void deactivate() {
		if (!activated)
			return;

		activated = false;
		if (!this.player.isOnline()) return;
		
		for (int i = 0; i < 15; i++) {
			WrapperPlayServerScoreboardTeam line = new WrapperPlayServerScoreboardTeam();
			line.setName(teams.get(i));
			line.setPlayers(Arrays.asList(teams.get(i)));
			line.setMode(1);
			line.sendPacket(this.player);
		}
		
		WrapperPlayServerScoreboardObjective display = new WrapperPlayServerScoreboardObjective();
		display.setName("sidebar");
		display.setMode(1);
		display.sendPacket(player);
		this.hidden = true;
		
	}
	
	@Override
	public void updateLine(int line, String text) {
		if (!cache.containsKey(line)) {
			this.showLine(line);
		}
		WrapperPlayServerScoreboardTeam update = new WrapperPlayServerScoreboardTeam();
		update.setName(teams.get(line));
		update.setMode(2);
		update.setSuffix(WrappedChatComponent.fromText(text));
		update.sendPacket(player);
		this.cache.put(line, text);
	}
	
	@Override
	public void showLine(int line) {
		WrapperPlayServerScoreboardScore score = new WrapperPlayServerScoreboardScore();
		score.setObjectiveName("sidebar");
		score.setValue(line);
		score.setScoreboardAction(ScoreboardAction.CHANGE);
		score.setScoreName(teams.get(line));
		score.sendPacket(player);
	}

	@Override
	public void hideLine(int line) {
		WrapperPlayServerScoreboardScore score = new WrapperPlayServerScoreboardScore();
		score.setObjectiveName("sidebar");
		score.setValue(line);
		score.setScoreboardAction(ScoreboardAction.REMOVE);
		score.setScoreName(teams.get(line));
		score.sendPacket(player);
	}
	
	@Override
	public void setTitle(String title) {
		WrapperPlayServerScoreboardObjective obj = new WrapperPlayServerScoreboardObjective();
		obj.setName("sidebar");
		obj.setMode(2);
		obj.setHealthDisplay(HealthDisplay.INTEGER);
		obj.setDisplayName(WrappedChatComponent.fromText(title));
		obj.sendPacket(this.player);
	}
	
	@Override
	public void update() {
		
		if (!player.isOnline()) {
			this.deactivate();
			return;
		}
		
		this.setTitle(handler.getTitle(player));
		
		for (Entry entry : handler.getEntries(player)) {
			String key = entry.getName();
			int score = entry.getPosition();
			
			if (key == null) continue;
			
			this.cache2.add(score);
			if (cache.containsKey(score) && cache.get(score).equals(key)) continue;
			this.updateLine(score, key);
		}
		
		for (int line : cache.keySet()) {
			if (this.cache2.contains(line)) continue;
			
			this.hideLine(line);
			cache.remove(line);
		}
		
		this.cache2.clear();
	}

	@Override
	public boolean isActivated() {
		return this.activated;
	}

	@Override
	public ScoreboardHandler getHandler() {
		return this.handler;
	}

	@Override
	public PhoenixBoard setHandler(ScoreboardHandler handler) {
		this.handler = handler;
		this.title = handler.getTitle(player);
		return this;
	}
	
	@Override
	public Player getHolder() {
		return this.player;
	}
}
