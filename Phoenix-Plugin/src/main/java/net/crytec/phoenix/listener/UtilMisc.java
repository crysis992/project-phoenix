package net.crytec.phoenix.listener;

import net.crytec.phoenix.api.PhoenixAPI;
import net.crytec.phoenix.api.nbt.NBTItem;
import org.bukkit.Location;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

public class UtilMisc {

	static final String DENY_ITEM_PICKUP = "denypickup";
	static final String DENY_HOPPER_PICKUP = "denyhopperpickup";
	static final String DENY_BREAK = "denyblockbreak";
	static final String BLOCK_MERGE = "denyItemmerge";
	static final String BLOCK_PLACE = "antibuild";

	/**
	 * Deny a block from being mined at the given location.
	 * 
	 * @param location
	 */
	public static void denyBlockBreak(final Location location) {
		location.getBlock().setMetadata(DENY_BREAK, new FixedMetadataValue( PhoenixAPI.get().getPlugin() , true));
	}

	/**
	 * Removes the lock from denyBlockBreak
	 * 
	 * @param location
	 */
	public static void allowBlockBreak(final Location location) {
		location.getBlock().removeMetadata(DENY_BREAK, PhoenixAPI.get().getPlugin() );
	}

	/**
	 * Makes an {@link Item} not being able to be picked up by entities/players
	 * or hoppers
	 * 
	 * @param item
	 */
	public static void disableItemPickup(final Item item) {
		item.addScoreboardTag(DENY_HOPPER_PICKUP);
		item.setPickupDelay(Integer.MAX_VALUE);
	}

	/**
	 * Prevents an {@link Item} from being merged
	 * 
	 * @param item
	 */
	public static void disableItemMerge(final Item item) {
		item.addScoreboardTag(BLOCK_MERGE);
	}

	/**
	 * Make an item not being placable by players
	 * 
	 * @param item
	 * @return returns the itemstack with the specific NBT tag
	 */
	public static ItemStack makeUnplacable(final ItemStack item) {
		final NBTItem nbt = new NBTItem(item);
		nbt.setByte(BLOCK_PLACE, (byte) 0);
		return nbt.getItem();
	}
}