package net.crytec.phoenix.plugin;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

public enum ConfigOptions {
	
	LANGUAGE("language", "EN_US"),
	USE_REDIS("redis", false),
	USE_SCOREBOARD("scoreboard", false),
	
	;

	
	private String path;
	private Object def;
	
    private static YamlConfiguration CONFIG;
	
	private ConfigOptions(String path, Object object) {
		this.path = path;
		this.def = object;
	}
	
	public boolean asBoolean() {
		return CONFIG.getBoolean(this.path);
	}
	
	public double asDouble() {
		return CONFIG.getDouble(this.path);
	}
	
	public int asInt() {
		return CONFIG.getInt(this.path);
	}
	
	public float asFloat() {
		return  (float) CONFIG.getDouble(this.path);
	}
	
	public String asString() {
		return CONFIG.getString(this.path);
	}
	
	public String asString(boolean translateColor) {
		return ChatColor.translateAlternateColorCodes('&', CONFIG.getString(this.path));
	}
	
	public List<String> asStringList() {
		return CONFIG.getStringList(this.path);
	}
	
	
	
	
    /**
    * Set the {@code YamlConfiguration} to use.
    * @param config The config to set.
    */
    public static void setFile(YamlConfiguration config) {
    	CONFIG = config;
    }
    
    public static YamlConfiguration getFile() {
    	return CONFIG;
    }
    
    /**
    * Get the default value of the path.
    * @return The default value of the path.
    */
    public Object getDefault() {
        return this.def;
    }
 
    /**
    * Get the path to the string.
    * @return The path to the string.
    */
    public String getPath() {
        return this.path;
    }
	
}
