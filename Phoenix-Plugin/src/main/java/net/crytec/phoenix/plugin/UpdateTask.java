package net.crytec.phoenix.plugin;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import net.crytec.phoenix.api.updater.UpdateChecker;
import net.crytec.phoenix.api.updater.UpdateChecker.UpdateReason;

public class UpdateTask implements Runnable {

	private final UpdateChecker updater;
	private final JavaPlugin plugin;
	
	protected UpdateTask(JavaPlugin plugin) {
		this.plugin = plugin;
		this.updater = UpdateChecker.init(plugin, 69554);
	}

	@Override
	public void run() {
		this.plugin.getLogger().info("Checking for new versions...");
		this.updater.requestUpdateCheck().whenComplete( (result, exception) -> {
			UpdateReason reason = result.getReason();
			
			switch (reason) {
				case NEW_UPDATE : {
					Bukkit.getConsoleSender().sendMessage("§eVersion " + result.getNewestVersion() + " from PhoenixAPI is available. You are still running version " + this.plugin.getDescription().getVersion());
					this.plugin.getLogger().info("It is recommended to update as soon as possible. You can download the latest version from here:");
					this.plugin.getLogger().info("https://www.spigotmc.org/resources/phoenix-api.69554/");
					break;
				}
				case UNRELEASED_VERSION : {
					this.plugin.getLogger().info(String.format("Your version of PhoeninxAPI (%s) is more recent than the one publicly (%u) available. Are you on a development build?", this.plugin.getDescription().getVersion(), result.getNewestVersion()));
					break;
				}
				case UP_TO_DATE: {
					this.plugin.getLogger().info("Your version is up to date.");
					break;
				}
				default : {
					this.plugin.getLogger().warning("Failed to check for updates: " + reason);
					break;
				}
			}
		});		
	}
}