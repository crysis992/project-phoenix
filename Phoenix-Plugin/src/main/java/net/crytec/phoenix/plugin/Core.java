package net.crytec.phoenix.plugin;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.Sets;

import net.crytec.phoenix.api.PhoenixAPI;
import net.crytec.phoenix.api.utils.UtilChunk;
import net.crytec.phoenix.api.utils.UtilPlayer;
import net.crytec.phoenix.api.version.ServerVersion;
import net.crytec.phoenix.api.version.ServerVersion.BukkitVersion;
import net.crytec.phoenix.dependencies.Dependency;
import net.crytec.phoenix.dependencies.DependencyManager;
import net.crytec.phoenix.dependencies.classloader.PluginClassLoader;
import net.crytec.phoenix.dependencies.classloader.ReflectionClassLoader;

public class Core extends JavaPlugin {
	
	private static Core instance;
	private ServerVersion version;
	private PhoenixAPI api;
	
	private PluginClassLoader classLoader;
	
	@Override
	public void onLoad() {

	}
	
	@Override
	public void onEnable() {
		
		HashSet<Dependency> dependencies = Sets.newHashSet(Dependency.FASTUTILS);
		
		this.classLoader = new ReflectionClassLoader(this);
		
		DependencyManager dependencyManager = new DependencyManager(this);
		
		dependencyManager.loadDependencies(dependencies);
		
		Core.instance = this;
		Bukkit.getConsoleSender().sendMessage("Loading " + this.getDescription().getName() + " v." + this.getDescription().getVersion());
		this.setupConfig();
		this.version = new ServerVersion(this);
		
		if (version.getVersion() == BukkitVersion.UNKNOWN) {
			Bukkit.getPluginManager().disablePlugin(this);
			
			Bukkit.getLogger().severe("=================== -ERROR- ========================");
			Bukkit.getLogger().severe(" ");
			Bukkit.getLogger().severe("This plugin version is not compatible with the selected server version.");
			Bukkit.getLogger().severe(" ");
			Bukkit.getLogger().severe("=================== " + this.getDescription().getName() + "[" + this.getDescription().getVersion() +  "] ========================");
			
			return;
		}
		
		api = new PhoenixAPI(this);
		
		UtilChunk.init(this);  // Init UtilChunk (ChunkTracking) //TODO Better implementation
		UtilPlayer.init(this); //Init UtilPlayer (EntityTracking) //TODO Better implementation
		
		new Metrics(this);
		// Check 3 Seconds after successful start for updates.
		Bukkit.getScheduler().runTaskLaterAsynchronously(this, new UpdateTask(this), 60L);
		
	}
	
	@Override
	public void onDisable() {
		this.getLogger().info("Shutting down...");
		this.api.shutdown();
	}
	
    public PluginClassLoader getPluginClassLoader() {
        return this.classLoader;
    }
	
	private void setupConfig() {
		try {
			File lang = new File(this.getDataFolder(), "config.yml");
			if (!lang.exists()) {
				this.getDataFolder().mkdir();
				lang.createNewFile();
				if (lang != null) {
					YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(lang);
					defConfig.save(lang);
					ConfigOptions.setFile(defConfig);
				}
			}

			YamlConfiguration conf = YamlConfiguration.loadConfiguration(lang);
			ConfigOptions.setFile(conf);
			int updated = 0;
			for (ConfigOptions option : ConfigOptions.values()) {
				if (!conf.isSet(option.getPath())) {
					conf.set(option.getPath(), option.getDefault());
					updated++;
				}
			}

			if (updated > 0) {
				conf.save(lang);
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	
	public ServerVersion getVersion() {
		return version;
	}

	public static Core getInstance() {
		return instance;
	}
}